## Kuka YOUBOT
--------------

This Workspace contains the code relating to the Kuka Youbot


Set correct ROS env parameters on youbot and laptop:
	export ROS_MASTER_URI=http://<ip>:11311 && export ROS_HOSTNAME=<ip>
	e.g. laptop: export ROS_MASTER_URI=http://129.215.90.92:11311 && export ROS_HOSTNAME=129.215.90.92
	e.g. youbot: export ROS_MASTER_URI=http://129.215.90.92:11311 && export ROS_HOSTNAME=129.215.91.26

Launch roscore on laptop:
	roscore

Launch youbot wrapper on the robot:
	roslaunch youbot_driver_ros_interface youbot_complete.launch

Launch other nodes on the laptop:
	rosrun map_server map_server src/youbot_andrew/floorplans/floor1.yaml # map
	rosrun youbot_andrew laser_scan_normalization # laser base_scan wrapper for move_base and amcl nodes
	rosrun youbot_andrew pose_transform # Wrapper for EKF and visualization

	roslaunch youbot_andrew robot_pose_ekf.launch # EKF filter, can be configured to use amcl and/or fabmap nodes
	
	roslaunch youbot_andrew run_base_move.launch # navigation planner
	roslaunch youbot_andrew run_amcl.launch # particle filter
	
	roslaunch youbot_andrew image_cache.launch  # fabmap db
	roslaunch fabmap_andrew run.launch # fabmap	

Launch other nodes like rviz/etc...
Modify robot_pose_ekf to either use fabmap and/or amcl, alternatively modify amcl node to publish tf transform.
Navigation goals are published to /move_base_simple/goal topic.

To run "delivery bot" demo use:
	rosrun youbot_andrew delivery_planner 

and follow on-screen instructions. Note, robot will start moving as soon as goals are specified.




Other useful commands:
	Individual arm joints can be controlled with:
		rostopic pub /arm_1/arm_controller/position_command brics_actuator/JointPositions -1 '[None, None, 0.5]' '[[now, arm_joint_1, rad, 2.94], [now, arm_joint_2, rad, 0.2], [now, arm_joint_3, rad, -1.6], [now, arm_joint_4, rad, 3.1], [now, arm_joint_5, rad, 2.93]]'
	
	Static transforms can be published with:
		rosrun tf static_transform_publisher 0 0 0 0 0 0 /map /odom 100
