#ifndef __RUN_NODE__
#define __RUN_NODE__

#include <ros/ros.h>
#include <opencv2/opencv.hpp>
#include <opencv2/nonfree/features2d.hpp>

#include <tf/tf.h>
#include <tf/transform_listener.h>
#include <tf/transform_broadcaster.h>

#include <std_srvs/Empty.h>
#include "nav_msgs/Odometry.h"
#include "geometry_msgs/Twist.h"
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/PoseWithCovarianceStamped.h"
#include "sensor_msgs/Image.h"

//typedef boost::shared_ptr<sensor_msgs::ImageConstPtr const> ImgConstPtr;

class RunNode
{
public:
  RunNode();
  virtual ~RunNode();

private:
  bool load_codebooks();
  bool load_map();
  void img_callback(const sensor_msgs::ImageConstPtr& img);
  bool bows_service(std_srvs::Empty::Request& request, std_srvs::Empty::Response& response);

  ros::NodeHandle node_;

  ros::Publisher matches_pub_;
  ros::Subscriber img_sub_;
  ros::ServiceServer bows_srv_;

  std::string vocab_file_, cltree_file_, trainbows_file_, bows_feature_file_, bows_index_file_;
  bool visualise_;
  int min_descriptor_count_, max_matches_;
  double bow_cluster_size_, min_match_value_;
  int hessian_threshold_, num_octaves_, num_octave_layers_;
  bool extended_, add_queries_, map_building_, preload_map_, absolute_index_;

  //of2::FabMap* fabmap_;
  of2::FabMap* fabmap_;
  of2::BOWMSCTrainer bow_trainer_;
  cv::FeatureDetector* detector_;
  cv::DescriptorExtractor*  extractor_;
  cv::DescriptorMatcher* matcher_;
  cv::BOWImgDescriptorExtractor* bide_; // Seems bide can only be initialized once

  cv::Mat vocab_, cltree_, trained_bows_, map_bows_, map_indices_;


  std::vector<unsigned int> id_to_img_seq;
}; // class

#endif
