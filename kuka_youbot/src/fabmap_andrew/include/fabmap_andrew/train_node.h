#ifndef __TRAINING_NODE__
#define __TRAINING_NODE__

#include <ros/ros.h>
#include <opencv2/opencv.hpp>
#include <opencv2/nonfree/features2d.hpp>

#include <tf/tf.h>
#include <tf/transform_listener.h>
#include <tf/transform_broadcaster.h>

#include <std_srvs/Empty.h>
#include "nav_msgs/Odometry.h"
#include "geometry_msgs/Twist.h"
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/PoseWithCovarianceStamped.h"
#include "sensor_msgs/Image.h"

//typedef boost::shared_ptr<sensor_msgs::ImageConstPtr const> ImgConstPtr;

class TrainingNode
{
public:
  TrainingNode();
  virtual ~TrainingNode();

private:
  void img_callback(const sensor_msgs::ImageConstPtr& img);
  bool codebook_service(std_srvs::Empty::Request& request, std_srvs::Empty::Response& response);

  ros::NodeHandle node_;

  //ros::Publisher pose_pub_, particle_pub_;
  ros::Subscriber img_sub_;
  ros::ServiceServer codebook_srv_;

  std::string vocab_file_, cltree_file_, trainbows_file_;
  bool visualise_;
  int min_descriptor_count_;
  double bow_cluster_size_;
  int hessian_threshold_, num_octaves_, num_octave_layers_;
  bool extended_;

  of2::BOWMSCTrainer bow_trainer_;
  cv::FeatureDetector* detector_;
  cv::DescriptorExtractor*  extractor_;
  cv::DescriptorMatcher* matcher_;
  cv::BOWImgDescriptorExtractor* bide_; // Seems bide can only be initialized once


  std::vector<cv_bridge::CvImagePtr> frames_trained_;
}; // class

#endif
