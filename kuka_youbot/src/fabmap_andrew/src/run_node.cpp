// http://ftp.isr.ist.utl.pt/pub/roswiki/cv_bridge%282f%29Tutorials%282f%29UsingCvBridgeCppDiamondback.html
// https://github.com/Itseez/opencv/blob/master/samples/cpp/fabmap_sample.cpp
// http://stackoverflow.com/questions/6832933/difference-between-feature-detection-and-descriptor-extraction

#include <ros/ros.h>
#include <vector>
#include <opencv2/opencv.hpp>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <fabmap_andrew/openfabmap.h>
#include <fabmap_andrew/run_node.h>
#include <fabmap_andrew/Match.h>
#include <std_srvs/Empty.h>



RunNode::RunNode()
{
	ROS_INFO("Starting FABMAP live node...");

	ros::NodeHandle priv_nh("~");
	ros::NodeHandle nh;

	priv_nh.param<std::string>("vocab", vocab_file_, "vocabulary.yml");
	priv_nh.param<std::string>("cltree", cltree_file_, "cltree.yml");
	priv_nh.param<std::string>("trainbows", trainbows_file_, "trained_bows.yml");

	if (load_codebooks()) {
		ROS_INFO("Codebooks loaded succesfully!");	

		// Get params
		priv_nh.param<double>("bowClusterSize", bow_cluster_size_, 0.4);
		priv_nh.param<int>("surfHessianThreshold", hessian_threshold_, 1000);
		priv_nh.param<int>("surfNumOctaves", num_octaves_, 4);
		priv_nh.param<int>("surfNumOctaveLayers", num_octave_layers_, 2);
		priv_nh.param<bool>("surfExtended", extended_, false);
		priv_nh.param<bool>("fabmapAddQueries", add_queries_, true);
		priv_nh.param<bool>("fabmapMapBuilding", map_building_, false);
		priv_nh.param<bool>("fabmapPreloadMap", preload_map_, false);
		priv_nh.param<bool>("fabmapAbsoluteIndex", absolute_index_, true);
		priv_nh.param<double>("fabmapMinMatchValue", min_match_value_, 0.99);
		priv_nh.param<int>("fabmapMaxMatches", max_matches_, 1);
		priv_nh.param<std::string>("mapBows", bows_feature_file_, "map_bows.yml");
		priv_nh.param<std::string>("mapIndices", bows_index_file_, "map_indices.yml");


		if (map_building_) {
			ROS_WARN("Running in a map building mode!");
		}
		
		//priv_nh.param<bool>("visualise", visualise_, false);
		priv_nh.param<int>("minDescriptorCount", min_descriptor_count_, 50);

		// Setup Fabmap specifics
		bow_trainer_ = of2::BOWMSCTrainer(bow_cluster_size_);

		detector_ = new cv::SURF(hessian_threshold_, num_octaves_, num_octave_layers_, extended_);
		extractor_ = new cv::SURF(hessian_threshold_, num_octaves_, num_octave_layers_, extended_);
		matcher_ = new cv::FlannBasedMatcher();
		bide_ = new cv::BOWImgDescriptorExtractor(extractor_, matcher_);
		
		ROS_INFO("Initializing FabMAP...");
		
		bide_->setVocabulary(vocab_);

		// pzge/pzgne based on sample_fabamp.cpp
		int options = of2::FabMap::SAMPLED | of2::FabMap::CHOW_LIU; // of2::FabMap::MOTION_MODEL
		double pzge = 0.39, pzgne = 0;

		//fabmap_ = new of2::FabMap2(cltree_, pzge, pzgne, options);
		fabmap_ = new of2::FabMap2(cltree_, 0.39, 0, of2::FabMap::SAMPLED | of2::FabMap::CHOW_LIU);
		fabmap_->addTraining(trained_bows_);

		if (preload_map_ && !map_building_) {
			ROS_INFO("Loading map BoWs...");
			if (load_map()) {
				fabmap_->add(map_bows_);

				for (int i = 0; i < map_indices_.rows; i++) {
					const int* Mi = map_indices_.ptr<int>(i);
					id_to_img_seq.push_back(Mi[0]);
				}
			} else {
				ROS_ERROR("Failed to load map BoW!");
				ros::shutdown();
			}
		}

		ROS_INFO("FabMAP initialized!");

		if (map_building_) {
			// We care about all images on the topic
			img_sub_ = nh.subscribe("/fabmap_live", -1, &RunNode::img_callback, this);
			
			// Expose codebook saving service
			bows_srv_ = nh.advertiseService("fabmap_save_bows", &RunNode::bows_service, this);
		} else {
			// We care only about the most recent image
			img_sub_ = nh.subscribe("/fabmap_live", 1, &RunNode::img_callback, this);

			// Advertise fabmap matches topic
			matches_pub_ = nh.advertise<fabmap_andrew::Match>("/fabmap_matches", 10);

		}
	} else {
		ROS_ERROR("Failed to load codebooks!");
		ros::shutdown();
	}
}

RunNode::~RunNode()
{
}

std::string type2str(int type) {
  std::string r;

  uchar depth = type & CV_MAT_DEPTH_MASK;
  uchar chans = 1 + (type >> CV_CN_SHIFT);

  switch ( depth ) {
    case CV_8U:  r = "8U"; break;
    case CV_8S:  r = "8S"; break;
    case CV_16U: r = "16U"; break;
    case CV_16S: r = "16S"; break;
    case CV_32S: r = "32S"; break;
    case CV_32F: r = "32F"; break;
    case CV_64F: r = "64F"; break;
    default:     r = "User"; break;
  }

  r += "C";
  r += (chans+'0');

  return r;
}

bool RunNode::load_codebooks()
{
	ROS_INFO("Loading codebooks from files...");

	cv::FileStorage fin;
		
	if (!fin.open(vocab_file_, cv::FileStorage::READ)) {
		return false;
	}
	fin["Vocabulary"] >> vocab_;
	fin.release();
  	ROS_INFO("Vocabulary loaded with %d words and %d dimensions!", vocab_.rows, vocab_.cols);
		
	if(!fin.open(cltree_file_, cv::FileStorage::READ)) {
		return false;
	}
	fin["clTree"] >> cltree_;
	
	//std::string ty =  type2str( cltree_.type() );
	//printf("Matrix: %s %dx%d \n", ty.c_str(), cltree_.cols, cltree_.rows );

	fin.release();
	ROS_INFO("CL Tree loaded!");

	if (!fin.open(trainbows_file_, cv::FileStorage::READ)) {
		return false;
	}
	fin["BOWImageDescs"] >> trained_bows_;
	fin.release();
	ROS_INFO("Trained BoWs loaded!");

	return true;
}

bool RunNode::load_map()
{
	ROS_INFO("Loading map from files...");

	cv::FileStorage fin;
		
	if(!fin.open(bows_feature_file_, cv::FileStorage::READ)) {
		return false;
	}
	fin["bowsmap"] >> map_bows_;
	fin.release();
	ROS_INFO("Map BoW loaded!");

	if (!fin.open(bows_index_file_, cv::FileStorage::READ)) {
		return false;
	}
	fin["mapindices"] >> map_indices_;
	fin.release();

	assert(map_indices_.cols == 1);
	assert(map_bows_.rows == map_indices_.rows);

	ROS_INFO("Indices loaded!");

	return true;
}

void RunNode::img_callback(const sensor_msgs::ImageConstPtr& img)
{
	//ROS_INFO("New image received with seq: %d", img->header.seq);
		
    cv_bridge::CvImagePtr cv_ptr;
    try {
	    cv_ptr = cv_bridge::toCvCopy(img, sensor_msgs::image_encodings::MONO8);
    } catch (cv_bridge::Exception& e) {
    	ROS_ERROR("cv_bridge exception: %s", e.what());
    	return;
    }

	// Detect e.g. SURF keypoints in the image
	std::vector<cv::KeyPoint> kpts;
	detector_->detect(cv_ptr->image, kpts);
	
	// Do we have required amount of the descriptors?
	if (kpts.size() > min_descriptor_count_) {
		// Extract descriptors for image keypoints
		cv::Mat descriptors;
		bide_->compute(cv_ptr->image, kpts, descriptors);

		// Check if found any descriptors
		if (!descriptors.empty()) {
			// Map internal fabmap id to image seqid
			// TODO: replace with image_query message
			if (add_queries_) {
				id_to_img_seq.push_back(img->header.seq);
			}

			if (map_building_) {
				assert(descriptors.rows == 1);
				map_indices_.push_back(img->header.seq);
				map_bows_.push_back(descriptors);
				ROS_INFO("Total BoWs: %d", map_bows_.rows);
				return;
			}

			// Find matches for the bow descriptor
			std::vector<of2::IMatch> matches;
			fabmap_->compare(descriptors, matches, add_queries_);
			//fabmap_->add(descriptors);
			//ROS_INFO("%ld", id_to_img_seq.size());

			// Sort matches 
			std::sort(matches.begin(), matches.end());
			
			//ROS_INFO("%f %f", matches[matches.size() - 1].likelihood, matches[matches.size() - 1].match);

			// Build message
			fabmap_andrew::Match matched;
			matched.query = img->header.seq;
			
			// IMAGE seq number
			int matchImgSeq;				
			// Prepare message in Decending match likelihood order
			for (int i = matches.size() - 1; i >= 0; i--) {
				// Limit the number of matches published (by 'maxMatches_' OR 'minMatchValue_')
				if (matched.matches.size() == max_matches_ || matches[i].match < min_match_value_) {
					break;
				}
					
				//ROS_INFO_STREAM("QueryIdx " << matchIter->queryIdx << " ImgIdx " << matchIter->imgIdx << " Likelihood " << matchIter->likelihood << " Match " << matchIter->match);

				if (absolute_index_) {
					// Lookup IMAGE seq number from MATCH seq number
					matchImgSeq = matches[i].imgIdx > -1 ? id_to_img_seq[matches[i].imgIdx] : -1;
				} else {
					matchImgSeq = matches[i].imgIdx;
				}

				// Additionally if required, 
				// --do NOT return matches below self matches OR new places ('-1') 
				/*if ((matchImgSeq >= matched.fromImgSeq-self_match_window_ && disable_self_match_) || (matchImgSeq == -1 && disable_unknown_match_))
					{
						break;
					}
				*/	
				// Add the Image seq number and its match likelihood
				matched.matches.push_back(matchImgSeq);
				matched.prob.push_back(matches[i].match);
			}

			// If filtered matches were found
			if (matched.matches.size() > 0) {
			// Publish current matches
				matches_pub_.publish(matched);


			}
		}
	} else {
		ROS_WARN("Not enough image features detected, skipping...");
	}
}

bool RunNode::bows_service(std_srvs::Empty::Request& request, std_srvs::Empty::Response& response) {
	ROS_INFO("Saving map to file...");
	
	cv::FileStorage fout;
		
	ROS_INFO("BoWs...");
	fout.open(bows_feature_file_, cv::FileStorage::WRITE);
	fout << "bowsmap" << map_bows_;
	fout.release();

	ROS_INFO("Indices...");		
	fout.open(bows_index_file_, cv::FileStorage::WRITE);
	fout << "mapindices" << map_indices_;
	fout.release();
	
	ROS_INFO("Map saved!");
	return true;
}

int main(int argc, char **argv)
{
	ros::init(argc, argv, "run_node");

	ros::NodeHandle priv_nh("~");
	ros::NodeHandle nh;

	double sample_rate;
	bool map_building;

	priv_nh.param<double>("samplingRate", sample_rate, 30);
	priv_nh.param<bool>("fabmapMapBuilding", map_building, false);

	RunNode rn;

	// Process images as fast as you can
	if (map_building) {
		ros::spin();
		return 0;
	}

	// Process images as a fixed rate
	ros::Rate rate(sample_rate);
	while (nh.ok()) {
		ros::spinOnce();
		rate.sleep();
	}

	return 0;
}