// http://ftp.isr.ist.utl.pt/pub/roswiki/cv_bridge%282f%29Tutorials%282f%29UsingCvBridgeCppDiamondback.html
// https://github.com/Itseez/opencv/blob/master/samples/cpp/fabmap_sample.cpp
// http://stackoverflow.com/questions/6832933/difference-between-feature-detection-and-descriptor-extraction

#include <ros/ros.h>
#include <vector>
#include <opencv2/opencv.hpp>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <fabmap_andrew/openfabmap.h>
#include <fabmap_andrew/train_node.h>
#include <std_srvs/Empty.h>


TrainingNode::TrainingNode()
{
	ROS_INFO("Starting FABMAP training node...");

	ros::NodeHandle priv_nh("~");
	ros::NodeHandle nh;

	// Get params
	priv_nh.param<std::string>("vocab", vocab_file_, "vocabulary.yml");
	priv_nh.param<std::string>("cltree", cltree_file_, "cltree.yml");
	priv_nh.param<std::string>("trainbows", trainbows_file_, "trained_bows.yml");
	priv_nh.param<double>("bowClusterSize", bow_cluster_size_, 0.4);
	priv_nh.param<int>("surfHessianThreshold", hessian_threshold_, 1000);
	priv_nh.param<int>("surfNumOctaves", num_octaves_, 4);
	priv_nh.param<int>("surfNumOctaveLayers", num_octave_layers_, 2);
	priv_nh.param<bool>("surfExtended", extended_, false);

		
	priv_nh.param<bool>("visualise", visualise_, false);
	priv_nh.param<int>("minDescriptorCount", min_descriptor_count_, 50);
	
	// We want to train on all images published to the topic
	//img_sub_ = nh.subscribe("/fabmap_training", -1, &TrainingNode::img_callback, this);

	// Only on last image
	img_sub_ = nh.subscribe("/fabmap_training", 1, &TrainingNode::img_callback, this);

	// Expose codebook saving as service
	codebook_srv_ = nh.advertiseService("fabmap_save_codebook", &TrainingNode::codebook_service, this);

	// Setup Fabmap specifics
	bow_trainer_ = of2::BOWMSCTrainer(bow_cluster_size_);

	detector_ = new cv::SURF(hessian_threshold_, num_octaves_, num_octave_layers_, extended_);
	extractor_ = new cv::SURF(hessian_threshold_, num_octaves_, num_octave_layers_, extended_);
	matcher_ = new cv::FlannBasedMatcher();
	bide_ = new cv::BOWImgDescriptorExtractor(extractor_, matcher_);

	if (visualise_) {
		cv::namedWindow("samples");
	}
}

TrainingNode::~TrainingNode()
{
	if (visualise_) {
		cv::destroyWindow("samples");
	}
}

void TrainingNode::img_callback(const sensor_msgs::ImageConstPtr& img)
{
	ROS_INFO("New image received with seq: %d", img->header.seq);
		
    cv_bridge::CvImagePtr cv_ptr;
    try {
	    cv_ptr = cv_bridge::toCvCopy(img, sensor_msgs::image_encodings::MONO8);
    } catch (cv_bridge::Exception& e) {
    	ROS_ERROR("cv_bridge exception: %s", e.what());
    	return;
    }

	// Detect e.g. SURF keypoints in the image
	std::vector<cv::KeyPoint> kpts;
	detector_->detect(cv_ptr->image, kpts);
	
	// Do we have required amount of the descriptors?
	if (kpts.size() > min_descriptor_count_) {
		// Extract descriptors for image keypoints
		cv::Mat descriptors;
		extractor_->compute(cv_ptr->image, kpts, descriptors);

		// Check if found any descriptors
		if (!descriptors.empty()) {
			// Push descriptors to the BoW cluster
			bow_trainer_.add(descriptors);

			// Save frame for later processing
			frames_trained_.push_back(cv_ptr);
			ROS_INFO("Added frame to trainer, total: %ld", frames_trained_.size());

			// Show sampled frame for the user
			if (visualise_) {
				cv::Mat img_with_kpts;
    			cv::drawKeypoints(cv_ptr->image, kpts, img_with_kpts);
    			cv::imshow("samples", img_with_kpts);
    			cv::waitKey(100);
			}
		}
	} else {
		ROS_WARN("Not enough image features detected, skipping...");
	}
}

bool TrainingNode::codebook_service(std_srvs::Empty::Request& request, std_srvs::Empty::Response& response) {
	ROS_INFO("Saving codebooks...");
	
	ROS_INFO("Clustering for feature vocabulary required to create BoW representations...");
	cv::Mat vocab;
	vocab = bow_trainer_.cluster();
	ROS_INFO("Vocabulary has %d words with %d dimensions", vocab.rows, vocab.cols);
	
	bide_->setVocabulary(vocab);
		
	ROS_INFO("Generating BoWs for sampled images based on the new vocabulary...");
	cv::Mat tmp_bow, bows;
	std::vector<cv::KeyPoint> kpts;
	//for (std::vector<cv_bridge::CvImagePtr>::iterator frameIter = framesSampled.begin(); frameIter != framesSampled.end(); ++frameIter) {
	for (int i = 0; i < frames_trained_.size(); i++) {
		detector_->detect(frames_trained_[i]->image, kpts);
		bide_->compute(frames_trained_[i]->image, kpts, tmp_bow);
		bows.push_back(tmp_bow);
	}
	ROS_INFO("BoWs generated: %d (of sampled images: %ld)", bows.rows, frames_trained_.size());
	
	ROS_INFO("Making the CL Tree...");
	of2::ChowLiuTree tmp_tree;
	cv::Mat cltree;
	tmp_tree.add(bows);
	cltree = tmp_tree.make(0);
	
	ROS_INFO("Saving to files...");
	
	cv::FileStorage fout;
		
	ROS_INFO("Vocabulary...");
	fout.open(vocab_file_, cv::FileStorage::WRITE);
	fout << "Vocabulary" << vocab;
	fout.release();

	ROS_INFO("CL Tree...");		
	fout.open(cltree_file_, cv::FileStorage::WRITE);
	fout << "clTree" << cltree;
	fout.release();
		
	ROS_INFO("Trained BoWs...");
	fout.open(trainbows_file_, cv::FileStorage::WRITE);
	fout << "BOWImageDescs" << bows;
	fout.release();

	ROS_INFO("Codebooks saved!");
	return true;
}

int main(int argc, char **argv)
{
	ros::init(argc, argv, "train_node");

	TrainingNode tn;
	ros::NodeHandle priv_nh("~");
	
	double sample_rate;
	priv_nh.param<double>("samplingRate", sample_rate, 1/14.); // every 14 seconds
	//printf("Sample rate %fHz", sample_rate);

	// Process images as a fixed rate
	ros::Rate rate(sample_rate);
	while (ros::ok()) {
		ros::spinOnce();
		rate.sleep();
	}

	return 0;
}