// TODO: http://wiki.ros.org/image_transport

#include <ros/ros.h>
#include <std_msgs/UInt32.h>
#include <nav_msgs/Odometry.h>
#include <std_srvs/Empty.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Point.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/Image.h>
#include <youbot_andrew/image_cache_trig.h>
#include <boost/thread.hpp>
#include <youbot_andrew/Match.h>

ros::Subscriber match_sub;
ros::Publisher match_pub;

void match_callback(const youbot_andrew::MatchConstPtr& msg)
{
  youbot_andrew::Match tmp = youbot_andrew::Match(*msg);
  ROS_DEBUG("New match received for %d image", tmp.query);
  
  // Self matches
  // TODO: replace with map
  std::vector<int> to_img_seq;
  std::vector<double> to_img_match;
  
  for (int i = 0; i < tmp.matches.size(); i++) {
    if (tmp.query - tmp.matches[i] > 50) {
      to_img_seq.push_back(tmp.matches[i]);
      to_img_match.push_back(tmp.prob[i]);
    } else {
      ROS_DEBUG("Match between %d and %d rejected due to self-match window", tmp.query, tmp.matches[i]);
    }
  }
  
  tmp.matches = to_img_seq;
  tmp.prob = to_img_match;

  // Publish if any valid matches remain
  if (tmp.matches.size() > 0) {
    match_pub.publish(tmp);
  }
}

int main(int argc, char** argv){
  ros::init(argc, argv, "match_verification");
	
  ros::NodeHandle node;
  match_sub = node.subscribe("/appearance_matches", 100, &match_callback);
  match_pub = node.advertise<youbot_andrew::Match>("/loop_closures", 100);
  
  ros::spin();
  return 0;
}
