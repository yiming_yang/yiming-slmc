// TODO: http://wiki.ros.org/image_transport

#include <ros/ros.h>
#include <std_msgs/UInt32.h>
#include <nav_msgs/Odometry.h>
#include <std_srvs/Empty.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Point.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/Image.h>
#include <youbot_andrew/image_cache_trig.h>
#include <visualization_msgs/Marker.h>
#include <geometry_msgs/Point.h>
#include <boost/thread.hpp>
#include <unordered_map>
#include <youbot_andrew/Match.h>
#include <tf/transform_datatypes.h>
#include <tf/LinearMath/Matrix3x3.h>

ros::Subscriber odom_sub;
ros::Subscriber loop_sub;
ros::Publisher marker_pub;
ros::ServiceClient sample_srv;

bool pose_init = false;
geometry_msgs::Pose robot_pose_last;

struct node {
  //unsigned int img_id;
  bool show;
  visualization_msgs::Marker marker;
  std::vector<unsigned int> matched_nodes;
};

std::vector<node> nodes;
std::unordered_map<unsigned int, std::vector<unsigned int> > gimg_to_node;

void draw_rviz_node(node tmp) {
  marker_pub.publish(tmp.marker);
}

void draw_rviz_graph() {
  visualization_msgs::Marker line_marker;
  
  line_marker.header.frame_id = "/map";
  line_marker.header.stamp = ros::Time::now();
  line_marker.ns = "edges";
  line_marker.type = visualization_msgs::Marker::LINE_STRIP;
  line_marker.action = visualization_msgs::Marker::ADD;
  line_marker.id = 0;
  //line_marker.pose = pose;

  line_marker.scale.x = 0.05;
  line_marker.color.r = 0.5;
  line_marker.color.g = 0.5;
  line_marker.color.b = 0.5;
  line_marker.color.a = 1.0;

  line_marker.lifetime = ros::Duration();
  
  std::vector<geometry_msgs::Point> points;
  for (int i = 0; i < nodes.size(); i++) {
    if (nodes[i].matched_nodes.size() > 0) {
      nodes[i].marker.ns = "nodes_matched";
      nodes[i].marker.color.r = 0.0;
      nodes[i].marker.color.g = 1.0;
      nodes[i].marker.color.b = 0.0;
    }
 
    // Draw only matched nodes
    if (nodes[i].matched_nodes.size() > 0 || nodes[i].show)
      draw_rviz_node(nodes[i]);

    geometry_msgs::Point tmp;
    tmp.x = nodes[i].marker.pose.position.x;
    tmp.y = nodes[i].marker.pose.position.y;
    tmp.z = nodes[i].marker.pose.position.z;

    points.push_back(tmp);
    
    usleep(1000);
  }
  line_marker.points = points;
  
  marker_pub.publish(line_marker);
}

void draw_rviz_graph_thread() {
  while (ros::ok()) {
    draw_rviz_graph();
    sleep(1);
  }
}

visualization_msgs::Marker get_new_node_marker(unsigned int id, geometry_msgs::Pose pose, const float *color) {
  visualization_msgs::Marker marker;
  
  marker.header.frame_id = "/map";
  marker.header.stamp = ros::Time::now();

  marker.ns = "nodes";
  marker.type = visualization_msgs::Marker::SPHERE;
  marker.action = visualization_msgs::Marker::ADD;

  marker.id = id;
  marker.pose = pose;

  marker.scale.x = 0.1;
  marker.scale.y = 0.1;
  marker.scale.z = 0.1;

  marker.color.r = color[0];
  marker.color.g = color[1];
  marker.color.b = color[2];
  marker.color.a = 1.0;

  marker.lifetime = ros::Duration();
  
  return marker;
}

void odom_callback(const nav_msgs::OdometryConstPtr& msg){
  geometry_msgs::Pose robot_pose = msg->pose.pose;
  
  if (!pose_init) {
    robot_pose_last = robot_pose;
    pose_init = true;
  }

  // Measure vector distance
  double dist = sqrt(pow(robot_pose.position.x - robot_pose_last.position.x, 2) + pow(robot_pose.position.y - robot_pose_last.position.y, 2));
  // Measure angle
  double useless_pitch, useless_roll, yaw;
  tf::Matrix3x3(tf::Quaternion(robot_pose.orientation.x, robot_pose.orientation.y, robot_pose.orientation.z, robot_pose.orientation.w)).getEulerYPR(yaw, useless_pitch, useless_roll);

  double useless_pitch_last, useless_roll_last, yaw_last;
  tf::Matrix3x3(tf::Quaternion(robot_pose_last.orientation.x, robot_pose_last.orientation.y, robot_pose_last.orientation.z, robot_pose_last.orientation.w)).getEulerYPR(yaw_last, useless_pitch, useless_roll);

  double rot = fabs(yaw - yaw_last);
  // Sample image
  // TODO: on rotation too!
  if (dist >= 0.01 || rot >= 0.1) {
    youbot_andrew::image_cache_trig srv;
    
    if (sample_srv.call(srv)) {
      ROS_INFO("gID: %ld lID: %d", (long int)srv.response.global_img_id, srv.response.local_img_id);
      node tmp;
      //tmp.img_id = srv.response.id;
      /*float color[3] = {1.0, 0.5, 0.0};
      tmp.marker = get_new_node_marker(nodes.size(), robot_pose, color);
      tmp.show = false;

      nodes.push_back(tmp);
      draw_rviz_node(tmp);
      */
    } else {
      ROS_ERROR("Failed to call service image_cache_trig");
    }
    
    robot_pose_last = robot_pose;
  }
  
  //robot_pose.position.x, robot_pose.position.y;
  //robot_pose.orientation.getAngle();
}

int line_ids = 0;

void loop_callback(const youbot_andrew::MatchConstPtr& msg){
  //ROS_INFO("ROS IS AWESOME: %d", msg->fromImgSeq);
  //a.insert(a.end(), b.begin(), b.end());


  // TODO: check indices
  /*for (int i = 0; i < msg->toImgSeq.size(); i++) {
    nodes[msg->toImgSeq[i]].matched_nodes.push_back(msg->fromImgSeq);
    nodes[msg->toImgSeq[i]].marker.ns = "nodes_matched";
    nodes[msg->toImgSeq[i]].marker.color.r = 0.0;
    nodes[msg->toImgSeq[i]].marker.color.g = 1.0;
    nodes[msg->toImgSeq[i]].marker.color.b = 0.0;
    draw_rviz_node(nodes[msg->toImgSeq[i]]);

    nodes[msg->fromImgSeq].show  = true;
    draw_rviz_node(nodes[msg->fromImgSeq]);

    // SHow line connection
    visualization_msgs::Marker line_marker;
  
    line_marker.header.frame_id = "/map";
    line_marker.header.stamp = ros::Time::now();
    line_marker.ns = "match_edges";
    line_marker.type = visualization_msgs::Marker::LINE_STRIP;
    line_marker.action = visualization_msgs::Marker::ADD;
    line_marker.id = line_ids++;

    line_marker.scale.x = 0.05;
    line_marker.color.r = 0.0;
    line_marker.color.g = 1;
    line_marker.color.b = 0;
    line_marker.color.a = 1.0;
    line_marker.lifetime = ros::Duration();
    
    std::vector<geometry_msgs::Point> points;
    geometry_msgs::Point tmp1, tmp2;
    tmp1.x = nodes[msg->toImgSeq[i]].marker.pose.position.x;
    tmp1.y = nodes[msg->toImgSeq[i]].marker.pose.position.y;
    tmp1.z = nodes[msg->toImgSeq[i]].marker.pose.position.z;

    tmp2.x = nodes[msg->fromImgSeq].marker.pose.position.x;
    tmp2.y = nodes[msg->fromImgSeq].marker.pose.position.y;
    tmp2.z = nodes[msg->fromImgSeq].marker.pose.position.z;

    points.push_back(tmp1);
    points.push_back(tmp2);
    line_marker.points = points;
  
    marker_pub.publish(line_marker);
  }
  */
}

int main(int argc, char** argv){
  ros::init(argc, argv, "slam_backend");
	
  ros::NodeHandle node;
  
  sample_srv = node.serviceClient<youbot_andrew::image_cache_trig>("image_cache_sample");
  //marker_pub = node.advertise<visualization_msgs::Marker>("/slam_graph", 1);
  odom_sub = node.subscribe("/odom", 10, &odom_callback);
  loop_sub = node.subscribe("/loop_closures", 100, &loop_callback);  
  
  // Setup grpah drawing thread
  //boost::thread draw_thread(&draw_rviz_graph_thread);  
  
  //while (ros::ok())
  ros::spin();

  // shutdown the node and join the thread back before exiting
  ros::shutdown();
  //draw_thread.join();
  return 0;
}
