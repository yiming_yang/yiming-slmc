#include <ros/ros.h>
#include <cmath>
#include <limits>


#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <geometry_msgs/PoseStamped.h>
#include <nav_msgs/Odometry.h>


ros::Subscriber pose_sub;
ros::Publisher pose_pub;
ros::Subscriber pose_sub2;
ros::Subscriber pose_sub6;
ros::Publisher pose_pub2;
ros::Publisher pose_pub6;
ros::Publisher pose_pub3, pose_pub4;
ros::Subscriber odom_sub;
ros::Publisher odom_pub;


void pose_callback(const geometry_msgs::PoseWithCovarianceStampedConstPtr& msg) {
  geometry_msgs::PoseStamped tmp;
  tmp.header = msg->header;
  tmp.pose = msg->pose.pose;
  pose_pub.publish(tmp);

  nav_msgs::Odometry tmpo;
  tmpo.header = msg->header;
  tmpo.child_frame_id = "base_footprint";
  tmpo.header.frame_id = "map";
  tmpo.pose = msg->pose;

  float tmpcov[] = {.1, 0.0, 0.0, 0.0, 0.0, 0.0, 
                    0.0, .1, 0.0, 0.0, 0.0, 0.0, 
                    0.0, 0.0, .1, 0.0, 0.0, 0.0, 
                    0.0, 0.0, 0.0, 0.1, 0.0, 0.0, 
                    0.0, 0.0, 0.0, 0.0, 0.1, 0.0, 
                    0.0, 0.0, 0.0, 0.0, 0.0, 0.1};
 for (int i = 0; i < 6; i++) {
    if (tmpo.pose.covariance[i * 7] == 0.0) {
      tmpo.pose.covariance[i * 7] = tmpcov[i * 7];
    } 
    //tmpo.twist.covariance[i] = tmpcov[i];
  }

  //tmpo.twist.twist.linear.x =tmpo.twist.twist.linear.y =tmpo.twist.twist.linear.z = tmpo.twist.twist.angular.x =tmpo.twist.twist.angular.y = tmpo.twist.twist.angular.z = 0.1;    

  pose_pub3.publish(tmpo);
}

void pose_callback2(const geometry_msgs::PoseWithCovarianceStampedConstPtr& msg) {
  geometry_msgs::PoseStamped tmp;

  tmp.header = msg->header;
  tmp.pose = msg->pose.pose;

  pose_pub2.publish(tmp);
}

void odom_callback(const nav_msgs::OdometryConstPtr& msg) {
  nav_msgs::Odometry tmp;
  tmp = *msg;
  //tmp.child_frame_id = "wheelodom";
  //tmp.header.frame_id = "wheelodom";
  float tmpcov[] = {1, 0.0, 0.0, 0.0, 0.0, 0.0, 
                    0.0, 1, 0.0, 0.0, 0.0, 0.0, 
                    0.0, 0.0, 1, 0.0, 0.0, 0.0, 
                    0.0, 0.0, 0.0, 1, 0.0, 0.0, 
                    0.0, 0.0, 0.0, 0.0, 1, 0.0, 
                    0.0, 0.0, 0.0, 0.0, 0.0, 1};
  for (int i = 0; i < 36; i++) {
      tmp.pose.covariance[i] = tmpcov[i];
      tmp.twist.covariance[i] = tmpcov[i];
  }

  odom_pub.publish(tmp);

  geometry_msgs::PoseStamped tmpp;
  tmpp.header = msg->header;
  tmpp.pose = msg->pose.pose;

  pose_pub4.publish(tmpp);
}

void pose6_callback(const geometry_msgs::PoseWithCovarianceStampedConstPtr& msg) {
  geometry_msgs::PoseStamped tmp;

  tmp.header = msg->header;
  tmp.pose = msg->pose.pose;

  pose_pub6.publish(tmp);
}

int main(int argc, char** argv){
  ros::init(argc, argv, "pose_transform");
	
  ros::NodeHandle node;
  
  pose_sub = node.subscribe("/amcl_pose", 10, &pose_callback);
  pose_pub = node.advertise<geometry_msgs::PoseStamped>("/amcl_pose_no_cov", 10);
  pose_pub3 = node.advertise<nav_msgs::Odometry>("/amcl_pose_odom", 10);

  pose_sub6 = node.subscribe("/fabmap_pose", 10, &pose6_callback);
  pose_pub6 = node.advertise<geometry_msgs::PoseStamped>("/fabmap_pose_no_cov", 10);


  pose_sub2 = node.subscribe("/robot_pose_ekf_andrew/odom_combined", 10, &pose_callback2);
  pose_pub2 = node.advertise<geometry_msgs::PoseStamped>("/robot_pose_ekf_andrew/odom_combined_no_cov", 10);
  
  odom_sub = node.subscribe("/odom", 10, &odom_callback);
  odom_pub = node.advertise<nav_msgs::Odometry>("/odom_cov", 10);
  pose_pub4 = node.advertise<geometry_msgs::PoseStamped>("/odom_pose", 10);

  //while (ros::ok())
  ros::spin();

  // shutdown the node and join the thread back before exiting
  ros::shutdown();
  return 0;
}
