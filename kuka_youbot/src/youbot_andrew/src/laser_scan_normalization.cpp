#include <ros/ros.h>
#include <cmath>
#include <limits>


#include <std_msgs/Float32.h>
#include <std_srvs/Empty.h>
#include <sensor_msgs/LaserScan.h>

ros::Subscriber laser_sub;
ros::Publisher laser_pub_loc;
ros::Publisher laser_pub_mov;

void laser_callback(const sensor_msgs::LaserScanConstPtr& msg) {
  //printf("%d\n", );
  sensor_msgs::LaserScan msg_loc = *msg;
  sensor_msgs::LaserScan msg_mov = *msg;
  //msg2.intensities.resize(msg2.ranges.size());
  //_intensities_type new_intensities = new _intensities_type[msg2.ranges.size()];
  //msg2.intensities = new (msg2.ranges.size());

  for (int i = 0; i < msg_loc.ranges.size(); i++) {
    if (msg_loc.ranges[i] != msg_loc.ranges[i]) { // Check if msg->ranges[i] is NaN
      msg_loc.ranges[i] = std::numeric_limits<float>::infinity();
      //printf("nan\n");
    }

    // Check if laser sensed obstacle
    if (msg_loc.ranges[i] > msg_loc.range_max) {
      msg_mov.ranges[i] = msg_loc.range_max - 0.1;
    }

    //msg2.intensities[i] = 254;
  }


  laser_pub_mov.publish(msg_mov);
  laser_pub_loc.publish(msg_loc);
}

int main(int argc, char** argv){
  ros::init(argc, argv, "laser_scan_normalization");
	
  ros::NodeHandle node;
  
  laser_pub_loc = node.advertise<sensor_msgs::LaserScan>("/base_scan_norm_loc", 10);
  laser_pub_mov = node.advertise<sensor_msgs::LaserScan>("/base_scan_norm_mov", 10);
  laser_sub = node.subscribe("/base_scan", 10, &laser_callback);
  
  //while (ros::ok())
  ros::spin();

  // shutdown the node and join the thread back before exiting
  ros::shutdown();
  return 0;
}
