#include <ros/ros.h>
#include <cmath>
#include <limits>

#include <tf/transform_listener.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <geometry_msgs/PoseStamped.h>
#include <nav_msgs/Odometry.h>
#include <std_msgs/Bool.h>
#include <actionlib_msgs/GoalStatusArray.h>
#include <vector>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Pose.h>
#include <brics_actuator/JointPositions.h>

geometry_msgs::PoseStamped object_location_;
geometry_msgs::PoseStamped delivery_location_;
ros::Publisher goal_pub;
ros::Publisher manipulator_pub;
ros::Subscriber move_base_sub;
ros::Subscriber manipulator_sub;
ros::Publisher camera_pub;


enum { 
  START,
  HAVE_OBJECT_LOCATION, 
  HAVE_DELIVERY_LOCATION, 
  ROBOT_LOCALIZED,
  SENT_OBJECT_DESTINATION,
  OBJECT_DESTINATION_REACHED,
  SENT_GRASPING_TASK,
  GRASPING_TASK_DONE,
  SENT_DELIVERY_DESTINATION, 
  DELIVERY_DESTINATION_REACHED
};

int state_ = START;

ros::Subscriber pose_sub;

bool set_camera_pose(){
  double positions[] = {2.925, 0.2, -1.6, 3.4, 2.93};
  int joints_number = sizeof(positions) / sizeof(positions[0]);

  brics_actuator::JointPositions command;
  command.poisonStamp.originator = "origin";
  command.poisonStamp.description = "desc";
  command.poisonStamp.qos = 1.0;

  std::vector <brics_actuator::JointValue> armJointPositions;
  armJointPositions.resize(joints_number);
  
  std::stringstream jointName;
  for (int i = 0; i < joints_number; i++) {
    jointName.str("");
    jointName << "arm_joint_" << (i + 1); // Assamble "arm_joint_1", "arm_joint_2", ...

    armJointPositions[i].joint_uri = jointName.str();
    armJointPositions[i].value = positions[i]; // Here we set the new value.
    armJointPositions[i].unit = "rad"; // Set unit.
    armJointPositions[i].timeStamp = ros::Time::now(); // Set unit.

  };
  command.positions = armJointPositions;

  ROS_INFO("Latching robot camera pose for 2 seconds...");
  int count = 0;
  float freq = 10;

  ros::Rate rate(freq);
  while (count < int(2 / (1 / freq))) {
    camera_pub.publish(command);
    rate.sleep();
    count++;
  }

  return true;
}

void target_callback(const geometry_msgs::PoseStampedConstPtr& msg) {
  //geometry_msgs::PoseStamped tmp;
  //tmp.header = msg->header;
  //tmp.pose = msg->pose.pose;
  if (state_ == START) {
    object_location_.pose = msg->pose;
    object_location_.header = msg->header;

    state_ = HAVE_OBJECT_LOCATION;
    ROS_INFO("Object location received!");
    ROS_INFO("Please specify DELIVERY location on topic /delivery_targets in the map frame");
    return;
  } 

  if (state_ == HAVE_OBJECT_LOCATION) {
    delivery_location_.pose = msg->pose;
    delivery_location_.header = msg->header;

    state_ = HAVE_DELIVERY_LOCATION;
    ROS_INFO("Delivery location received!");
    return;
  } 

  ROS_ERROR("Undefined action sequence!");
}

// TODO: UID
void move_callback(const actionlib_msgs::GoalStatusArrayConstPtr& msg) {
  actionlib_msgs::GoalStatus status = msg->status_list[0];

  if (state_ == SENT_OBJECT_DESTINATION && status.status == 3) {
    state_ = OBJECT_DESTINATION_REACHED;
    return;
  }

  if (state_ == SENT_DELIVERY_DESTINATION && status.status == 3) {
    state_ = DELIVERY_DESTINATION_REACHED;
    return;
  }
}

// Callback when it does grasping
void manipulator_callback(const std_msgs::BoolConstPtr& msg) {
  //GRASPING_TASK_DONE
  if (state_ == SENT_GRASPING_TASK && msg->data) {
    state_ = GRASPING_TASK_DONE;
    return;
  }
}

int main(int argc, char** argv){
  ros::init(argc, argv, "delivery_planner");
  ros::NodeHandle node;
  pose_sub = node.subscribe("/delivery_targets", 10, &target_callback);
  move_base_sub = node.subscribe("/move_base/status", 10, &move_callback);
  manipulator_sub = node.subscribe("/youbot_manipulator/done", 10, &manipulator_callback);

  goal_pub = node.advertise<geometry_msgs::PoseStamped>("/move_base_simple/goal", 10);
  manipulator_pub = node.advertise<std_msgs::Bool>("/youbot_manipulator/start", 10);
  camera_pub = node.advertise<brics_actuator::JointPositions>("/arm_1/arm_controller/position_command", 10);

  set_camera_pose();
  ROS_INFO("Please specify OBJECT location on topic /delivery_targets in the map frame");
  //ros::spin(); 
  tf::TransformListener listener;
  tf::StampedTransform transform;
  ros::Rate loop_rate(1.0); // Hz
  while (ros::ok())
  {
    ros::spinOnce();

    // localization and 1st navigation steps
    if (state_ == HAVE_DELIVERY_LOCATION) {
      ROS_INFO("Waiting for robot localization...");
      
      try {
        listener.waitForTransform("map", "base_footprint", ros::Time(0), ros::Duration(1) );
        listener.lookupTransform("map", "base_footprint", ros::Time(0), transform);
        
        state_ = ROBOT_LOCALIZED;
        ROS_INFO("Got robot location, waiting 2 seconds to settle!");
        ros::Duration(2.0).sleep();
      } catch (tf::TransformException ex) {
        continue;
      }

      continue;
    } 

    if (state_ == ROBOT_LOCALIZED) {
      ROS_INFO("Sending object location...");
      goal_pub.publish(object_location_);

      ROS_INFO("Waiting for robot to reach object destination...");
      state_ = SENT_OBJECT_DESTINATION;
      continue;
    }

    if (state_ == OBJECT_DESTINATION_REACHED) {
      ROS_INFO("Robot reached object destination, handing over to grasping task!");
      std_msgs::Bool tmp;
      tmp.data = true;
      manipulator_pub.publish(tmp);
      state_ = SENT_GRASPING_TASK;
      continue;
    }

    if (state_ == GRASPING_TASK_DONE) {
      ROS_INFO("Grasping task done, moving to delivery destination...");
      set_camera_pose();
      goal_pub.publish(delivery_location_);
      state_ = SENT_DELIVERY_DESTINATION;
      continue;
    }

    if (state_ == DELIVERY_DESTINATION_REACHED) {
      ROS_INFO("DELIVERY DONE, exiting!");
      return 0;
    }

    loop_rate.sleep();
  }

  ros::shutdown();
  return 0;
}
