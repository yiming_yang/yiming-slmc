// TODO: http://wiki.ros.org/image_transport

#include <ros/ros.h>
#include <std_msgs/UInt32.h>
#include <nav_msgs/Odometry.h>
#include <std_srvs/Empty.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Point.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/Image.h>
#include <youbot_andrew/image_cache_trig.h>
#include <boost/thread.hpp>
#include <tf/transform_listener.h>
#include <tf/transform_broadcaster.h>
#include <unordered_map>
#include <geometry_msgs/PoseWithCovariance.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TransformStamped.h>
#include <youbot_andrew/Match.h>
#include <visualization_msgs/Marker.h>

ros::Subscriber camera_sub;
ros::Publisher camera_pub;
ros::ServiceServer camera_trig;
ros::Subscriber odom_sub;
ros::Publisher pose_pub;
ros::Subscriber loop_sub;
ros::Publisher vis_pub;

tf::Transform relative_drift;
bool first_pose_set = false, preload_map_;
std::string in_file_;

// Defines how long a map->odom transform is good for
ros::Duration transform_tolerance_;

boost::mutex last_camera_image_mutex;
sensor_msgs::Image last_camera_image;
//std::vector<sensor_msgs::Image> image_cache;
unsigned int images_cached = 0, total_markers = 0;

std::unordered_map<unsigned int, bool> used_frames;
//std::unordered_map<unsigned int, tf::StampedTransform> frame_id_to_pose_tf;
std::unordered_map<unsigned int, geometry_msgs::Pose> frame_id_to_pose;


bool sample_trig_callback(youbot_andrew::image_cache_trig::Request& req, youbot_andrew::image_cache_trig::Response& res)
{ 
  /*ROS_DEBUG("Camera sample trigerred...");
  static tf::TransformListener tf_listener;
  tf::StampedTransform transform;  

  try {
    tf_listener.waitForTransform("/map", "/base_footprint", ros::Time(0), ros::Duration(2.0) );
    tf_listener.lookupTransform("/map", "/base_footprint", ros::Time(0), transform);
  } catch (tf::TransformException ex) {
    ROS_ERROR("%s",ex.what());
  }


  last_camera_image_mutex.lock();  

  // Save TF
  frame_id_to_pose_tf[images_cached] = transform;

  // Visualize TF pose
  geometry_msgs::PoseStamped pose;
  pose.header.frame_id = "map";
  pose.pose.position.x = transform.getOrigin().x();
  pose.pose.position.y = transform.getOrigin().y();
  pose.pose.position.z = transform.getOrigin().z();
  pose.pose.orientation.x = transform.getRotation().x();
  pose.pose.orientation.y = transform.getRotation().y();
  pose.pose.orientation.z = transform.getRotation().z();
  pose.pose.orientation.w = transform.getRotation().w();
  pose_pub.publish(pose);
  
  // Publish the sampled image  
  camera_pub.publish(last_camera_image);

  used_frames[last_camera_image.header.seq] = true;  

  res.global_img_id = last_camera_image.header.seq;
  res.local_img_id = images_cached; // TODO: implement custom message with ID, DO NOT RELY ON SEQ  
  
  images_cached++;

  last_camera_image_mutex.unlock();

  return true;*/
}

void camera_callback(const sensor_msgs::ImageConstPtr& msg)
{
  /*ROS_DEBUG("New image received...");
  last_camera_image_mutex.lock();

  // Check if already seen, if yes, skip
  if (used_frames.count(msg->header.seq) == 0) {
    last_camera_image = sensor_msgs::Image(*msg);
  } else {
    ROS_INFO("Already used image with seq: %d", msg->header.seq);
  }

  last_camera_image_mutex.unlock();*/
}

void loop_callback(const youbot_andrew::MatchConstPtr& msg) {
  static tf::TransformListener tf_listener;

  for (int i = 0; i < msg->matches.size(); i++) {
  
    // Check if within array bounds
    if (msg->matches[i] >= 0) {
      if (frame_id_to_pose.count(msg->matches[i])) {
        geometry_msgs::PoseWithCovarianceStamped tmp;
        tmp.header = msg->header;
        tmp.header.frame_id = "fabmap";
        tmp.pose.pose = frame_id_to_pose[msg->matches[i]];

        float tmpcov[] = {0.25, 0.0, 0.0, 0.0, 0.0, 0.0, 
                        0.0, 0.25, 0.0, 0.0, 0.0, 0.0, 
                        0.0, 0.0, 0.25, 0.0, 0.0, 0.0, 
                        0.0, 0.0, 0.0, 0.25, 0.0, 0.0, 
                        0.0, 0.0, 0.0, 0.0, 0.25, 0.0, 
                        0.0, 0.0, 0.0, 0.0, 0.0, 0.25};
      
        for (int i = 0; i < 36; i++) {
          tmp.pose.covariance[i] = tmpcov[i];
        //tmp.twist.covariance[i] = tmpcov[i];
        }

        pose_pub.publish(tmp);

        // Add rviz markers
        visualization_msgs::Marker marker;
        marker.header.frame_id = "fabmap";
        marker.header.stamp = ros::Time();
        marker.ns = "fabmap_loop_closure";
        marker.id = total_markers++;
        marker.type = visualization_msgs::Marker::SPHERE;
        marker.action = visualization_msgs::Marker::ADD;
        marker.pose.position = tmp.pose.pose.position;
        marker.pose.orientation = tmp.pose.pose.orientation;
        marker.scale.x = 0.6;
        marker.scale.y = 0.6;
        marker.scale.z = 0.6;
        marker.color.a = 1.0;
        marker.color.r = 0.0;
        marker.color.g = 1.0;
        marker.color.b = 0.0;
        vis_pub.publish( marker );


      } else {
        ROS_WARN("No pose found for frame id: %d", msg->matches[i]);
      }
    }
  }

}

void odom_callback(const nav_msgs::OdometryConstPtr& msg){
}

// dummy callbacks
void connected(const ros::SingleSubscriberPublisher&) {}
void disconnected(const ros::SingleSubscriberPublisher&) {}

int main(int argc, char** argv){
  ros::init(argc, argv, "image_cache");
  ros::NodeHandle node;
  ros::NodeHandle priv_nh("~");

  // Get params
  priv_nh.param<bool>("cachePreloadMap", preload_map_, true);
  priv_nh.param<std::string>("mapInFile", in_file_, "frame_to_pose.csv");


  // TODO: make a param
  transform_tolerance_.fromSec(0.5);

  camera_sub = node.subscribe("/rgb/image_raw", 1, &camera_callback);
  camera_trig = node.advertiseService("image_cache_sample", sample_trig_callback);
  //odom_sub = node.subscribe("/amcl_pose", 10, &odom_callback);
  pose_pub = node.advertise<geometry_msgs::PoseWithCovarianceStamped>("/fabmap_pose", 10);
  loop_sub = node.subscribe("/fabmap_matches", 100, &loop_callback);
  vis_pub = node.advertise<visualization_msgs::Marker>( "/fabmap_markers", 0 );

  // for TF publish trigger
  odom_sub = node.subscribe("/odom", 1, &odom_callback);

  if (preload_map_) {
    ROS_INFO("Loading pose map...");
    FILE *fin = fopen(in_file_.c_str(), "r");

    if (!fin) {
      ROS_ERROR("Failed to open pose map!");
      ros::shutdown();
    }

    unsigned int seq;
    float tx, ty, tz, rx, ry, rz, rw;
    while (fscanf(fin, "%u, %f, %f, %f, %f, %f, %f, %f\n", &seq, &tx, &ty, &tz, &rx, &ry, &rz, &rw) != EOF) {
      geometry_msgs::Pose tmp;
      tmp.position.x = tx;
      tmp.position.y = ty;
      tmp.position.z = tz;

      tmp.orientation.x = rx;
      tmp.orientation.y = ry;
      tmp.orientation.z = rz;
      tmp.orientation.w = rw;

      frame_id_to_pose[seq] = tmp;
      //printf("%d\n", seq);
    }
    ROS_INFO("Pose map loaded!");
  }


  // Keep headers the same
  /*ros::AdvertiseOptions op = ros::AdvertiseOptions::create<sensor_msgs::Image>("/rgb/image_raw_sampled", 10, &connected, &disconnected, ros::VoidPtr(), NULL);
  op.has_header = false;
  camera_pub = node.advertise(op);
  */
  camera_pub = node.advertise<sensor_msgs::Image>("/rgb/image_raw_sampled", 10);
  ros::spin();
  return 0;
}
