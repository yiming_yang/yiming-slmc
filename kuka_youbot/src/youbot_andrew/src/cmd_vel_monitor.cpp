#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Point.h>

ros::Time last_vel_update_;

void vel_callback(const geometry_msgs::TwistConstPtr& msg) {
  last_vel_update_ = ros::Time::now();
}

int main(int argc, char** argv){
  ros::init(argc, argv, "cmd_vel_monitor");
  ros::NodeHandle node;

  last_vel_update_ = ros::Time::now();

  ros::Duration allowed_no_cmd_interval;
  allowed_no_cmd_interval.fromSec(1.0);

  ros::Subscriber sub = node.subscribe("/cmd_vel", 10, &vel_callback);
  ros::Publisher pub = node.advertise<geometry_msgs::Twist>("/cmd_vel", 10);

  ros::Rate loop_rate(10);
  while (ros::ok()) {
    ros::spinOnce();

    if (ros::Time::now() - last_vel_update_ >= allowed_no_cmd_interval) {
      ROS_INFO("cmd_vel timeout...");
      geometry_msgs::Twist tmp;
      pub.publish(tmp);
    }

    loop_rate.sleep();
  }

  return 0;
};
