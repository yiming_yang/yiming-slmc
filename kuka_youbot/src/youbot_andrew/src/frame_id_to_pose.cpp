// TODO: http://wiki.ros.org/image_transport

#include <ros/ros.h>
#include <std_msgs/UInt32.h>
#include <nav_msgs/Odometry.h>
#include <std_srvs/Empty.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Point.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/Image.h>
#include <youbot_andrew/image_cache_trig.h>
#include <boost/thread.hpp>
#include <tf/transform_listener.h>
#include <tf/transform_broadcaster.h>
#include <unordered_map>
#include <geometry_msgs/PoseWithCovariance.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TransformStamped.h>
#include <youbot_andrew/Match.h>

ros::Subscriber camera_sub;
FILE *out;

void camera_callback(const sensor_msgs::ImageConstPtr& msg)
{
  if (msg->header.seq > 15208) {
    return;
  }

  static tf::TransformListener tf_listener;
  tf::StampedTransform transform;  
  ros::Time current_time = ros::Time::now();
  try {// msg->header.stamp
    tf_listener.waitForTransform("/map", "/base_footprint", ros::Time(0), ros::Duration(10) );
    tf_listener.lookupTransform("/map", "/base_footprint", ros::Time(0), transform);
  } catch (tf::TransformException ex) {
    ROS_ERROR("%s",ex.what());
    return;
  }

  /*
  pose.pose.position.x = transform.getOrigin().x();
  pose.pose.position.y = transform.getOrigin().y();
  pose.pose.position.z = transform.getOrigin().z();
  pose.pose.orientation.x = transform.getRotation().x();
  pose.pose.orientation.y = transform.getRotation().y();
  pose.pose.orientation.z = transform.getRotation().z();
  pose.pose.orientation.w = transform.getRotation().w();
  */

  fprintf(out, "%d, %f, %f, %f, %f, %f, %f, %f\n", msg->header.seq, 
    transform.getOrigin().x(), transform.getOrigin().y(), transform.getOrigin().z(), 
    transform.getRotation().x(), transform.getRotation().y(), transform.getRotation().z(), transform.getRotation().w());
  fflush(out);
}

int main(int argc, char** argv){
  ros::init(argc, argv, "image_cache");

  ros::NodeHandle node;
  ros::NodeHandle priv_nh("~");

  camera_sub = node.subscribe("/rgb/image_raw", -1, &camera_callback);

  // Get Params
  std::string out_file_;

  priv_nh.param<std::string>("mapOutFile", out_file_, "/home/ros/workspace/codebooks/evaluation/frame_to_pose.csv");
  out = fopen(out_file_.c_str(), "w");

  ros::spin();
  return 0;
}
