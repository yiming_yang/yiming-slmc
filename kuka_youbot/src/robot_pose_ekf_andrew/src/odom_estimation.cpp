#include <robot_pose_ekf/odom_estimation.h>

using namespace MatrixWrapper;
using namespace BFL;
using namespace tf;
using namespace std;
using namespace ros;


namespace estimation
{
  // constructor
  OdomEstimation::OdomEstimation():
  prior_(NULL),
  filter_(NULL),
  filter_initialized_(false),
  odom_initialized_(false),
  fabmap_initialized_(false),
  amcl_initialized_(false)
  {
    // create SYSTEM MODEL
    ColumnVector sysNoise_Mu(6);  sysNoise_Mu = 0;
    SymmetricMatrix sysNoise_Cov(6); sysNoise_Cov = 0;
    for (unsigned int i=1; i<=6; i++) {
      sysNoise_Cov(i,i) = pow(1000.0,2);
    }
    Gaussian system_Uncertainty(sysNoise_Mu, sysNoise_Cov);
    sys_pdf_   = new NonLinearAnalyticConditionalGaussianOdo(system_Uncertainty);
    sys_model_ = new AnalyticSystemModelGaussianUncertainty(sys_pdf_);

    // create MEASUREMENT MODEL ODOM
    ColumnVector measNoiseOdom_Mu(6);  measNoiseOdom_Mu = 0;
    SymmetricMatrix measNoiseOdom_Cov(6);  measNoiseOdom_Cov = 0;
    for (unsigned int i=1; i<=6; i++) {
      measNoiseOdom_Cov(i,i) = 1;
    }
    Gaussian measurement_Uncertainty_Odom(measNoiseOdom_Mu, measNoiseOdom_Cov);
    Matrix Hodom(6,6);  Hodom = 0;
    Hodom(1,1) = 1;    Hodom(2,2) = 1;    Hodom(6,6) = 1;
    odom_meas_pdf_   = new LinearAnalyticConditionalGaussian(Hodom, measurement_Uncertainty_Odom);
    odom_meas_model_ = new LinearAnalyticMeasurementModelGaussianUncertainty(odom_meas_pdf_);

    // create MEASUREMENT MODEL AMCL
    ColumnVector measNoiseAmcl_Mu(6);  measNoiseAmcl_Mu = 0;
    SymmetricMatrix measNoiseAmcl_Cov(6);  measNoiseAmcl_Cov = 0;
    for (unsigned int i=1; i<=6; i++) {
      measNoiseAmcl_Cov(i,i) = 1;
    }
    Gaussian measurement_Uncertainty_AMCL(measNoiseAmcl_Mu, measNoiseAmcl_Cov);
    Matrix Hamcl(6,6);  Hamcl = 0;
    Hamcl(1,1) = 1;    Hamcl(2,2) = 1;    Hamcl(6,6) = 1;    
    amcl_meas_pdf_   = new LinearAnalyticConditionalGaussian(Hamcl, measurement_Uncertainty_AMCL);
    amcl_meas_model_ = new LinearAnalyticMeasurementModelGaussianUncertainty(amcl_meas_pdf_);

    // create MEASUREMENT MODEL FABMAP
    ColumnVector measNoiseFabmap_Mu(6);  measNoiseFabmap_Mu = 0;
    SymmetricMatrix measNoiseFabmap_Cov(6);  measNoiseFabmap_Cov = 0;
    for (unsigned int i=1; i<=6; i++) {
      measNoiseFabmap_Cov(i,i) = 1;
    }
    Gaussian measurement_Uncertainty_FABMAP(measNoiseFabmap_Mu, measNoiseFabmap_Cov);
    Matrix Hfabmap(6,6);  Hfabmap = 0;
    Hfabmap(1,1) = 1;    Hfabmap(2,2) = 1;    Hfabmap(6,6) = 1;    
    fabmap_meas_pdf_   = new LinearAnalyticConditionalGaussian(Hfabmap, measurement_Uncertainty_FABMAP);
    fabmap_meas_model_ = new LinearAnalyticMeasurementModelGaussianUncertainty(fabmap_meas_pdf_);
  };



  // destructor
  OdomEstimation::~OdomEstimation(){
    if (filter_) delete filter_;
    if (prior_)  delete prior_;
    delete odom_meas_model_;
    delete odom_meas_pdf_;
    delete fabmap_meas_model_;
    delete fabmap_meas_pdf_;
    delete amcl_meas_model_;
    delete amcl_meas_pdf_;
    delete sys_pdf_;
    delete sys_model_;
  };


  // initialize prior density of filter 
  void OdomEstimation::initialize(const Transform& prior, const Time& time)
  {
    // set prior of filter
    ColumnVector prior_Mu(6); 
    decomposeTransform(prior, prior_Mu(1), prior_Mu(2), prior_Mu(3), prior_Mu(4), prior_Mu(5), prior_Mu(6));
    SymmetricMatrix prior_Cov(6); 
    for (unsigned int i=1; i<=6; i++) {
      for (unsigned int j=1; j<=6; j++){
        if (i==j) {
          prior_Cov(i,j) = pow(0.001,2);
        } else {
          prior_Cov(i,j) = 0;
        }
      }
    }
    prior_  = new Gaussian(prior_Mu, prior_Cov);
    filter_ = new ExtendedKalmanFilter(prior_);

    // remember prior
    addMeasurement(StampedTransform(prior, time, "odom_combined", "base_footprint"));
    filter_estimate_old_vec_ = prior_Mu;
    filter_estimate_old_ = prior;
    filter_time_old_     = time;

    // filter initialized
    filter_initialized_ = true;
  }





  // update filter
  bool OdomEstimation::update(bool odom_active, bool fabmap_active, bool amcl_active, const Time&  filter_time)
  {
    // only update filter when it is initialized
    if (!filter_initialized_){
      ROS_INFO("Cannot update filter when filter was not initialized first.");
      return false;
    }

    // only update filter for time later than current filter time
    double dt = (filter_time - filter_time_old_).toSec();
    if (dt == 0) return false;
    if (dt <  0){
      ROS_INFO("Will not update robot pose with time %f sec in the past.", dt);
      return false;
    }

    // system update filter
    ColumnVector vel_desi(2); vel_desi = 0;
    filter_->Update(sys_model_, vel_desi);
    
    if (odom_active){
      if (!transformer_.canTransform("base_footprint","wheelodom", filter_time)){
        ROS_ERROR("filter time older than odom message buffer");
        return false;
      }
      transformer_.lookupTransform("wheelodom", "base_footprint", filter_time, odom_meas_);
      if (odom_initialized_){
        // convert absolute odom measurements to relative odom measurements in horizontal plane
        Transform odom_rel_frame =  Transform(tf::createQuaternionFromYaw(filter_estimate_old_vec_(6)), filter_estimate_old_.getOrigin()) * odom_meas_old_.inverse() * odom_meas_;
        ColumnVector odom_rel(6); 
        decomposeTransform(odom_rel_frame, odom_rel(1), odom_rel(2), odom_rel(3), odom_rel(4), odom_rel(5), odom_rel(6));
        angleOverflowCorrect(odom_rel(6), filter_estimate_old_vec_(6));
        
        // update filter
        odom_meas_pdf_->AdditiveNoiseSigmaSet(odom_covariance_ * pow(dt,2));
        filter_->Update(odom_meas_model_, odom_rel);
      }
      else{
        odom_initialized_ = true;
      }
      odom_meas_old_ = odom_meas_;
    }
    // sensor not active
    else odom_initialized_ = false;
    
    
    if (fabmap_active) {
      if (!transformer_.canTransform("base_footprint", "fabmap", filter_time)){
        ROS_ERROR("filter time older than fabmap message buffer");
        return false;
      }
      transformer_.lookupTransform("fabmap", "base_footprint", filter_time, fabmap_meas_);
      if (fabmap_initialized_){
        fabmap_meas_pdf_->AdditiveNoiseSigmaSet(fabmap_covariance_ * pow(dt,2));
        ColumnVector fabmap_vec(6);
        //Take fabmap as an absolute measurement, do not convert to relative measurement
        decomposeTransform(fabmap_meas_, fabmap_vec(1), fabmap_vec(2), fabmap_vec(3), fabmap_vec(4), fabmap_vec(5), fabmap_vec(6));
        angleOverflowCorrect(fabmap_vec(6), filter_estimate_old_vec_(6));
        filter_->Update(fabmap_meas_model_,  fabmap_vec);
      }
      else  {
        fabmap_initialized_ = true;
        fabmap_meas_old_ = fabmap_meas_;
      }
    }
    // sensor not active
    else fabmap_initialized_ = false;


    if (amcl_active){
      if (!transformer_.canTransform("base_footprint", "amcl", filter_time)){
        ROS_ERROR("filter time older than amcl message buffer");
        return false;
      }
      transformer_.lookupTransform("amcl", "base_footprint", filter_time, amcl_meas_);
      if (amcl_initialized_){
        amcl_meas_pdf_->AdditiveNoiseSigmaSet(amcl_covariance_ * pow(dt,2));
        ColumnVector amcl_vec(6);
        //Take amcl as an absolute measurement, do not convert to relative measurement
        decomposeTransform(amcl_meas_, amcl_vec(1), amcl_vec(2), amcl_vec(3), amcl_vec(4), amcl_vec(5), amcl_vec(6));
        angleOverflowCorrect(amcl_vec(6), filter_estimate_old_vec_(6));
        filter_->Update(amcl_meas_model_,  amcl_vec);
      }
      else {
        amcl_initialized_ = true;
        amcl_meas_old_ = amcl_meas_;
      }
    }
    // sensor not active
    else amcl_initialized_ = false;


    
    // remember last estimate
    filter_estimate_old_vec_ = filter_->PostGet()->ExpectedValueGet();
    tf::Quaternion q;
    q.setRPY(filter_estimate_old_vec_(4), filter_estimate_old_vec_(5), filter_estimate_old_vec_(6));
    filter_estimate_old_ = Transform(q, Vector3(filter_estimate_old_vec_(1), filter_estimate_old_vec_(2), filter_estimate_old_vec_(3)));
    filter_time_old_ = filter_time;
    addMeasurement(StampedTransform(filter_estimate_old_, filter_time, "odom_combined", "base_footprint"));

    return true;
  };

  void OdomEstimation::addMeasurement(const StampedTransform& meas)
  {
    /*ROS_INFO("AddMeasurement from %s to %s:  (%f, %f, %f)  (%f, %f, %f, %f)",
      meas.frame_id_.c_str(), meas.child_frame_id_.c_str(),
      meas.getOrigin().x(), meas.getOrigin().y(), meas.getOrigin().z(),
      meas.getRotation().x(),  meas.getRotation().y(), 
      meas.getRotation().z(), meas.getRotation().w());*/
    transformer_.setTransform( meas );
  }

  void OdomEstimation::addMeasurement(const StampedTransform& meas, const MatrixWrapper::SymmetricMatrix& covar)
  {
    // check covariance
    for (unsigned int i=0; i<covar.rows(); i++){
      if (covar(i+1, i+1) == 0){
        ROS_ERROR("Covariance specified for measurement on topic %s is zero", meas.child_frame_id_.c_str());
        return;
      }
    }
    // add measurements
    addMeasurement(meas);
    if (meas.child_frame_id_ == "wheelodom") odom_covariance_ = covar;
    else if (meas.child_frame_id_ == "fabmap") fabmap_covariance_   = covar;
    else if (meas.child_frame_id_ == "amcl")  amcl_covariance_  = covar;
    else ROS_ERROR("Adding a measurement for an unknown sensor %s", meas.child_frame_id_.c_str());
  };


  // get latest filter posterior as vector
  void OdomEstimation::getEstimate(MatrixWrapper::ColumnVector& estimate)
  {
    estimate = filter_estimate_old_vec_;
  };

  // get filter posterior at time 'time' as Transform
  void OdomEstimation::getEstimate(Time time, Transform& estimate)
  {
    StampedTransform tmp;
    if (!transformer_.canTransform("base_footprint","odom_combined", time)){
      ROS_ERROR("Cannot get transform at time %f", time.toSec());
      return;
    }
    transformer_.lookupTransform("odom_combined", "base_footprint", time, tmp);
    estimate = tmp;
  };

  // get filter posterior at time 'time' as Stamped Transform
  void OdomEstimation::getEstimate(Time time, StampedTransform& estimate)
  {
    if (!transformer_.canTransform("odom_combined", "base_footprint", time)){
      ROS_ERROR("Cannot get transform at time %f", time.toSec());
      return;
    }
    transformer_.lookupTransform("odom_combined", "base_footprint", time, estimate);
  };

  // get most recent filter posterior as PoseWithCovarianceStamped
  void OdomEstimation::getEstimate(geometry_msgs::PoseWithCovarianceStamped& estimate)
  {
    // pose
    StampedTransform tmp;
    if (!transformer_.canTransform("odom_combined", "base_footprint", ros::Time())){
      ROS_ERROR("Cannot get transform at time %f", 0.0);
      return;
    }
    transformer_.lookupTransform("odom_combined", "base_footprint", ros::Time(), tmp);
    poseTFToMsg(tmp, estimate.pose.pose);

    // header
    estimate.header.stamp = tmp.stamp_;
    estimate.header.frame_id = "odom";

    // covariance
    SymmetricMatrix covar =  filter_->PostGet()->CovarianceGet();
    for (unsigned int i=0; i<6; i++)
      for (unsigned int j=0; j<6; j++)
        estimate.pose.covariance[6*i+j] = covar(i+1,j+1);
    };

  // correct for angle overflow
    void OdomEstimation::angleOverflowCorrect(double& a, double ref)
    {
      while ((a-ref) >  M_PI) a -= 2*M_PI;
      while ((a-ref) < -M_PI) a += 2*M_PI;
    };

  // decompose Transform into x,y,z,Rx,Ry,Rz
    void OdomEstimation::decomposeTransform(const StampedTransform& trans, 
     double& x, double& y, double&z, double&Rx, double& Ry, double& Rz){
      x = trans.getOrigin().x();   
      y = trans.getOrigin().y(); 
      z = trans.getOrigin().z(); 
      trans.getBasis().getEulerYPR(Rz, Ry, Rx);
    };

  // decompose Transform into x,y,z,Rx,Ry,Rz
    void OdomEstimation::decomposeTransform(const Transform& trans, 
     double& x, double& y, double&z, double&Rx, double& Ry, double& Rz){
      x = trans.getOrigin().x();   
      y = trans.getOrigin().y(); 
      z = trans.getOrigin().z(); 
      trans.getBasis().getEulerYPR(Rz, Ry, Rx);
    };

}; // namespace
