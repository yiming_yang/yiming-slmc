#include <robot_pose_ekf/odom_estimation_node.h>


using namespace MatrixWrapper;
using namespace std;
using namespace ros;
using namespace tf;


static const double EPS = 1e-5;

ros::Duration transform_tolerance_;

namespace estimation
{
  // constructor
  OdomEstimationNode::OdomEstimationNode():
  odom_active_(false),
  fabmap_active_(false),
  amcl_active_(false),
  odom_initializing_(false),
  fabmap_initializing_(false),
  amcl_initializing_(false),
  odom_covariance_(6),
  fabmap_covariance_(6),
  amcl_covariance_(6)
  {
    ros::NodeHandle nh_private("~");
    ros::NodeHandle nh;

    nh_private.param("fabmap_used",   fabmap_used_, true);
    nh_private.param("amcl_used",   amcl_used_, false);
    double freq;
    nh_private.param("freq", freq, 15.0);

    output_frame_ = "/odom_combined";
    timer_ = nh_private.createTimer(ros::Duration(1.0/max(freq,1.0)), &OdomEstimationNode::spin, this);
    pose_pub_ = nh_private.advertise<geometry_msgs::PoseWithCovarianceStamped>(output_frame_, 10);
    particle_pub_ = nh_private.advertise<geometry_msgs::PoseWithCovarianceStamped>("/initialpose", 1);

    // Init
    filter_stamp_ = Time::now();
    odom_sub_ = nh.subscribe("/odom", 10, &OdomEstimationNode::odom_callback, this);
    
    if (amcl_used_){
      ROS_INFO("Using AMCL");
      amcl_sub_ = nh.subscribe("/amcl_pose", 10, &OdomEstimationNode::amcl_callback, this);
    }
    else {
      ROS_DEBUG("Not using AMCL");
    }

    if (fabmap_used_){
      ROS_INFO("Using FABMAP");
      fabmap_sub_ = nh.subscribe("/fabmap_pose", 10, &OdomEstimationNode::fabmap_callback, this);
    }
    else {
      ROS_DEBUG("Not using FABMAP");
    } 
  };




  // destructor
  OdomEstimationNode::~OdomEstimationNode(){
  };

  // callback function for odom data
  void OdomEstimationNode::fabmap_callback(const FabmapConstPtr& fabmap)
  {
    assert(fabmap_used_);

    // get data
    fabmap_stamp_ = Time::now();//fabmap->header.stamp;
    fabmap_time_  = Time::now();
    poseMsgToTF(fabmap->pose.pose, fabmap_meas_);

    for (unsigned int i=0; i<6; i++) {
      for (unsigned int j=0; j<6; j++) {
        if (i == j && fabmap->pose.covariance[6*i+j] == 0.0) {
          fabmap_covariance_(i+1, j+1) = 0.1;
        } else {
          fabmap_covariance_(i+1, j+1) = fabmap->pose.covariance[6*i+j];
        }
      }
    }
    
    //my_filter_.addMeasurement(StampedTransform(fabmap_meas_.inverse(), fabmap_stamp_, "base_footprint", "fabmap"), fabmap_covariance_);
    my_filter_.addMeasurement(StampedTransform(fabmap_meas_.inverse(), fabmap_time_, "base_footprint", "fabmap"), fabmap_covariance_);
    particle_pub_.publish(fabmap);

      if (!fabmap_active_) {
        fabmap_active_ = true;
        /*if (!fabmap_initializing_){
          //fabmap_initializing_ = true;
          //fabmap_init_stamp_ = fabmap_stamp_;
          fabmap_init_stamp_ = fabmap_time_;
          ROS_INFO("Initializing FABMAP sensor");      
        }
        if (filter_stamp_ >= fabmap_init_stamp_){
          fabmap_active_ = true;
          fabmap_initializing_ = false;
          ROS_INFO("FABMAP sensor activated");      
        }
        else ROS_DEBUG("Waiting to activate FABMAP, because FABMAP measurements are still %f sec in the future.", 
          (fabmap_init_stamp_ - filter_stamp_).toSec());*/
      } 
  };




  // callback function for odom data
  void OdomEstimationNode::odom_callback(const OdomConstPtr& odom)
  {
    // receive data 
    odom_stamp_ = odom->header.stamp;
    odom_time_  = Time::now();
    Quaternion q;
    tf::quaternionMsgToTF(odom->pose.pose.orientation, q);
    odom_meas_  = Transform(q, Vector3(odom->pose.pose.position.x, odom->pose.pose.position.y, 0));
    for (unsigned int i=0; i<6; i++)
      for (unsigned int j=0; j<6; j++)
        odom_covariance_(i+1, j+1) = odom->pose.covariance[6*i+j];

      my_filter_.addMeasurement(StampedTransform(odom_meas_.inverse(), odom_stamp_, "base_footprint", "wheelodom"), odom_covariance_);

    // activate odom
      if (!odom_active_) {
        if (!odom_initializing_){
          odom_initializing_ = true;
          odom_init_stamp_ = odom_stamp_;
          ROS_INFO("Initializing Odom sensor");      
        }
        if ( filter_stamp_ >= odom_init_stamp_){
          odom_active_ = true;
          odom_initializing_ = false;
          ROS_INFO("Odom sensor activated");      
        }
        else ROS_DEBUG("Waiting to activate Odom, because Odom measurements are still %f sec in the future.", 
          (odom_init_stamp_ - filter_stamp_).toSec());
      }

  };


  void OdomEstimationNode::amcl_callback(const AmclConstPtr& amcl)
  {
    assert(amcl_used_);

    // get data
    amcl_stamp_ = amcl->header.stamp;
    amcl_time_  = Time::now();
    poseMsgToTF(amcl->pose.pose, amcl_meas_);
    for (unsigned int i=0; i<6; i++)
      for (unsigned int j=0; j<6; j++)
        amcl_covariance_(i+1, j+1) = amcl->pose.covariance[6*i+j];
      my_filter_.addMeasurement(StampedTransform(amcl_meas_.inverse(), amcl_stamp_, "base_footprint", "amcl"), amcl_covariance_);

    // activate amcl
      if (!amcl_active_) {
        if (!amcl_initializing_){
          amcl_initializing_ = true;
          amcl_init_stamp_ = amcl_stamp_;
          ROS_INFO("Initializing AMCL sensor");      
        }
        if (filter_stamp_ >= amcl_init_stamp_){
          amcl_active_ = true;
          amcl_initializing_ = false;
          ROS_INFO("AMCL sensor activated");      
        }
        else ROS_DEBUG("Waiting to activate AMCL, because AMCL measurements are still %f sec in the future.", 
          (amcl_init_stamp_ - filter_stamp_).toSec());
      }
    
  };



  // filter loop
  void OdomEstimationNode::spin(const ros::TimerEvent& e)
  {
    //ROS_DEBUG("Spin function at time %f", ros::Time::now().toSec());

    // initial value for filter stamp; keep this stamp when no sensors are active
    filter_stamp_ = Time::now();
    
    // only update filter when one of the sensors is active
    if (odom_active_ && (fabmap_active_ || amcl_active_)){

      // update filter at time where all sensor measurements are available
      if (odom_active_)   filter_stamp_ = min(filter_stamp_, odom_stamp_);
      //if (fabmap_active_) filter_stamp_ = min(filter_stamp_, fabmap_stamp_);
      if (amcl_active_)   filter_stamp_ = min(filter_stamp_, amcl_stamp_);
      //if (amcl_active_ && !fabmap_used_)   filter_stamp_ = min(filter_stamp_, amcl_stamp_);
      //if (fabmap_active_ && !amcl_used_) filter_stamp_ = min(filter_stamp_, fabmap_stamp_);
      //if (amcl_active_ && !fabmap_used_)   filter_stamp_ = min(filter_stamp_, amcl_stamp_);
      //if (amcl_active_&& fabmap_active_)   filter_stamp_ = min(filter_stamp_, max(fabmap_stamp_, amcl_stamp_));
      
      
      // update filterfabmap_stamp_ >= amcl_stamp_, amcl_stamp_ >= fabmap_stamp_
      if ( my_filter_.isInitialized() )  {
        //ROS_INFO("%d %d", fabmap_stamp_ > my_filter_.filter_time_old_, amcl_stamp_ > my_filter_.filter_time_old_);
        if (my_filter_.update(odom_active_, false, amcl_active_, filter_stamp_)){
          //ROS_INFO("Update");
          // output most recent estimate and relative covariance
          my_filter_.getEstimate(output_);
          pose_pub_.publish(output_);
          
          // broadcast most recent estimate to TransformArray
          StampedTransform tmp;
          my_filter_.getEstimate(ros::Time(), tmp);
          /*if(!vo_active_ && !amcl_active_)
            tmp.getOrigin().setZ(0.0);*/
          //odom_broadcaster_.sendTransform(StampedTransform(tmp.inverse(), tmp.stamp_, base_footprint_frame_, "map"));
          // Calculate drift
          relative_drift_ = tmp * odom_meas_.inverse();
          
        }

        odom_broadcaster_.sendTransform(StampedTransform(relative_drift_, Time::now() + transform_tolerance_, "map", "odom"));
      }

      // initialize filer with fabmap frame
      if (odom_active_ && fabmap_active_ && !amcl_used_ && !my_filter_.isInitialized()) {
        my_filter_.initialize(fabmap_meas_, fabmap_stamp_);
        ROS_INFO("Kalman filter initialized with fabmap and odom measurement");
      } else if (odom_active_ && !fabmap_used_ && amcl_active_ && !my_filter_.isInitialized()) {
        my_filter_.initialize(amcl_meas_, amcl_stamp_);
        ROS_INFO("Kalman filter initialized with amcl and odom measurement");
      } else if (odom_active_ && fabmap_active_ && amcl_active_ && !my_filter_.isInitialized()) {
        my_filter_.initialize(fabmap_meas_, fabmap_stamp_);
        ROS_INFO("Kalman filter initialized with fabmap/amcl and odom measurement");
      }
    }
  };

}; // namespace


using namespace estimation;
int main(int argc, char **argv)
{
  // Initialize ROS
  ros::init(argc, argv, "robot_pose_ekf_andrew");
  transform_tolerance_.fromSec(3);
  // create filter class
  OdomEstimationNode my_filter_node;

  ros::spin();
  
  return 0;
}
