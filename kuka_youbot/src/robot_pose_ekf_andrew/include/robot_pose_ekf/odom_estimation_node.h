#ifndef __ODOM_ESTIMATION_NODE__
#define __ODOM_ESTIMATION_NODE__

#include <ros/ros.h>
#include <tf/tf.h>
#include <tf/transform_listener.h>
#include <tf/transform_broadcaster.h>
#include "odom_estimation.h"
#include <robot_pose_ekf/GetStatus.h>

#include "nav_msgs/Odometry.h"
#include "geometry_msgs/Twist.h"
#include "sensor_msgs/Imu.h"
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/PoseWithCovarianceStamped.h"

#include <boost/thread/mutex.hpp>

#include <fstream>

namespace estimation
{

typedef boost::shared_ptr<nav_msgs::Odometry const> OdomConstPtr;
typedef boost::shared_ptr<geometry_msgs::PoseWithCovarianceStamped const> FabmapConstPtr;
typedef boost::shared_ptr<nav_msgs::Odometry const> AmclConstPtr;

class OdomEstimationNode
{
public:
  /// constructor
  OdomEstimationNode();

  /// destructor
  virtual ~OdomEstimationNode();

private:
  /// the mail filter loop that will be called periodically
  void spin(const ros::TimerEvent& e);

  /// callback function for odo data
  void odom_callback(const OdomConstPtr& odom);
  void amcl_callback(const AmclConstPtr& amcl);
  void fabmap_callback(const FabmapConstPtr& fabmap);


  /// get the status of the filter
  bool getStatus(robot_pose_ekf::GetStatus::Request& req, robot_pose_ekf::GetStatus::Response& resp);

  ros::NodeHandle node_;
  ros::Timer timer_;
  ros::Publisher pose_pub_, particle_pub_;
  ros::Subscriber odom_sub_, fabmap_sub_, amcl_sub_;
  // ekf filter
  OdomEstimation my_filter_;

  // estimated robot pose message to send
  geometry_msgs::PoseWithCovarianceStamped  output_; 

  // robot state
  tf::TransformListener    robot_state_;
  tf::TransformBroadcaster odom_broadcaster_;
  tf::Transform relative_drift_;

  // vectors
  tf::Transform odom_meas_, fabmap_meas_, amcl_meas_;
  tf::Transform base_fabmap_init_;
  tf::Transform base_amcl_init_;
  tf::StampedTransform camera_base_;
  ros::Time odom_time_, fabmap_time_, amcl_time_;
  ros::Time odom_stamp_, fabmap_stamp_, amcl_stamp_, filter_stamp_;
  ros::Time odom_init_stamp_, fabmap_init_stamp_, amcl_init_stamp_;
  bool odom_active_, fabmap_active_, amcl_active_;
  bool fabmap_used_, amcl_used_;
  bool odom_initializing_, fabmap_initializing_, amcl_initializing_;
  double timeout_;
  MatrixWrapper::SymmetricMatrix odom_covariance_, fabmap_covariance_, amcl_covariance_;
  bool debug_;
  std::string output_frame_, base_footprint_frame_, tf_prefix_;
}; // class

}; // namespace

#endif
