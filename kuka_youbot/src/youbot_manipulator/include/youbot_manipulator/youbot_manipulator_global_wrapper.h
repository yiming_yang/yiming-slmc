/*
 * youbot_manipulator_global_wrapper.h
 *
 *  Created on: 24 Feb 2014
 *      Author: yiming
 */

#ifndef YOUBOT_MANIPULATOR_GLOBAL_WRAPPER_H_
#define YOUBOT_MANIPULATOR_GLOBAL_WRAPPER_H_

namespace youbot_manipulator
{
	/**
	 * Youbot Manipulator Global Wrapper. Communicates with global navigation and searching
	 */
	class GlobalWrapper
	{
			/**
			 * @brief	Constructor
			 */
			GlobalWrapper(YoubotManipulator * manipulator_);


	};
}



#endif /* YOUBOT_MANIPULATOR_GLOBAL_WRAPPER_H_ */
