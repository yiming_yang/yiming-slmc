#ifndef ACTIONLIB_SERVERS_SINGLE_ACTION_SERVER_IMP_H_
#define ACTIONLIB_SERVERS_SINGLE_ACTION_SERVER_IMP_H_

#include <boost/bind.hpp>

namespace actionlib_servers
{

    template<class ActionSpec>
    SingleActionServer<ActionSpec>::SingleActionServer(std::string action_name,
                                                       GoalCallback goal_callback,
                                                       ExecuteCallback execute_callback,
                                                       bool can_cancel) :
        action_server_(nh_,
                       action_name,
                       boost::bind(&SingleActionServer<ActionSpec>::goalCallback, this, _1),
                       boost::bind(&SingleActionServer<ActionSpec>::cancelCallback, this, _1),
                       false),
        action_name_(action_name),
        goal_callback_(goal_callback),
        execute_callback_(execute_callback),
        has_active_goal_(false),
        execute_thread_(NULL),
        need_to_terminate_(false),
        can_cancel_(can_cancel),
        need_to_cancel_(false)
    {
        action_server_.start();
        if(execute_callback_ != NULL)
        {
            execute_thread_ = new boost::thread(boost::bind(&SingleActionServer<ActionSpec>::executeLoop, this));
        }
    }

    template<class ActionSpec>
    SingleActionServer<ActionSpec>::SingleActionServer(std::string action_name,
                                                       ExecuteCallback execute_callback,
                                                       bool can_cancel) :
        action_server_(nh_,
                       action_name,
                       boost::bind(&SingleActionServer<ActionSpec>::goalCallback, this, _1),
                       boost::bind(&SingleActionServer<ActionSpec>::cancelCallback, this, _1),
                       false),
        action_name_(action_name),
        goal_callback_(NULL),
        execute_callback_(execute_callback),
        has_active_goal_(false),
        execute_thread_(NULL),
        need_to_terminate_(false),
        can_cancel_(can_cancel),
        need_to_cancel_(false)
    {
        action_server_.start();
        if(execute_callback_ != NULL)
        {
            execute_thread_ = new boost::thread(boost::bind(&SingleActionServer<ActionSpec>::executeLoop, this));
        }
    }

    template<class ActionSpec>
    SingleActionServer<ActionSpec>::~SingleActionServer()
    {
        ROS_DEBUG("Destroying SingleActionServer");
        if(execute_thread_)
            shutdown_execute_thread();
    }

    template<class ActionSpec>
    void SingleActionServer<ActionSpec>::shutdown_execute_thread()
    {
        if(execute_callback_)
        {
            {
                ROS_DEBUG("Locking the terminate var");
                boost::mutex::scoped_lock terminate_lock(terminate_mutex_);
                need_to_terminate_ = true;
            }
            ROS_DEBUG("Joining with the execute_thread");
            execute_thread_->join();
            delete execute_thread_;
            execute_thread_ = NULL;
        }

    }

    template<class ActionSpec>
    void SingleActionServer<ActionSpec>::executeLoop()
    {
        ros::Rate loop_rate(100);
        // no sense trying to execute if ros has already been shutdown
        while(nh_.ok())
        {
            ROS_DEBUG("Starting executeLoop");
            
            // make sure the server isn't trying to shutdown
            bool need_to_terminate;
            {
                boost::mutex::scoped_lock terminate_lock(terminate_mutex_);
                need_to_terminate = need_to_terminate_;
            }
            if(need_to_terminate)
            {
                    
                ROS_DEBUG("Terminating the executeLoop");
                // let the client know we are shutting down
                boost::mutex::scoped_lock has_active_goal_lock(has_active_goal_mutex_);
                if(has_active_goal_)
                {
                    has_active_goal_lock.unlock();
                    boost::recursive_mutex::scoped_lock current_goal_lock(current_goal_mutex_);
                    current_goal_.setAborted(Result(), "Action server is shutting down");
                }
                break;
            }

            // actually execute the goal
            bool has_active_goal;
            {
                boost::mutex::scoped_lock has_active_goal_lock(has_active_goal_mutex_);
                has_active_goal = has_active_goal_;
            }
            if(has_active_goal)
            {
                ROS_DEBUG("Have an active goal. Executing its callback");
                execute_callback_(this, current_goal_.getGoal());
                
                {
                    boost::mutex::scoped_lock has_active_goal_lock(has_active_goal_mutex_);
                    has_active_goal = has_active_goal_;
                }
                if(has_active_goal)
                {
                    ROS_ERROR("Your execute callback did not mark the goal as either succeeded or aborted.\n"
                              "This is an error in your callback. Please fix it before running this server again\n"
                              "This goal has been marked aborted in case your function exited prematurely.\n");
                    setAborted(Result(), "Error in server's execution callback. Please contact the maintainer of this action server to have this bug fixed");
                }

                ROS_DEBUG("Finished execute callback");
            }
            else
            {
                ROS_DEBUG("Waiting for notification to start execute callback");
                loop_rate.sleep();
            }
            
        }
    }

    template<class ActionSpec>
    void SingleActionServer<ActionSpec>::setSucceeded()
    {
        setSucceeded(Result(), "");
    }

    template <class ActionSpec>
    void SingleActionServer<ActionSpec>::setSucceeded(const Result& result)
    {
        setSucceeded(result, "");
    }

    template<class ActionSpec>
    void SingleActionServer<ActionSpec>::setSucceeded(const Result& result, const std::string& text)
    {
        ROS_DEBUG("Goal was a success");
        boost::recursive_mutex::scoped_lock current_goal_lock(current_goal_mutex_);
        boost::mutex::scoped_lock has_active_goal_lock(has_active_goal_mutex_);
        boost::mutex::scoped_lock cancel_lock(cancel_mutex_);

        if(has_active_goal_)
            current_goal_.setSucceeded(result, text);

        
        need_to_cancel_ = false;
        has_active_goal_ = false;
        
    }

    template <class ActionSpec>
    void SingleActionServer<ActionSpec>::setAborted()
    {
        setAborted(Result(), "");
    }

    template <class ActionSpec>
    void SingleActionServer<ActionSpec>::setAborted(const Result& result)
    {
        setAborted(result, "");
    }

    template<class ActionSpec>
    void SingleActionServer<ActionSpec>::setAborted(const Result& result, const std::string& text)
    {
        ROS_DEBUG("Goal was aborted");

        {
            boost::mutex::scoped_lock has_active_goal_lock(has_active_goal_mutex_);
            if(has_active_goal_)
            {
                boost::recursive_mutex::scoped_lock current_goal_lock(current_goal_mutex_);            
                current_goal_.setAborted(result, text);
            }
        }

        // reset the has_active_goal_ flag
        {
            boost::mutex::scoped_lock has_active_goal_lock(has_active_goal_mutex_);            
            has_active_goal_ = false;
        }

        boost::mutex::scoped_lock cancel_lock(cancel_mutex_);                
        need_to_cancel_ = false;
    }

    template<class ActionSpec>
    bool SingleActionServer<ActionSpec>::ok()
    {
        boost::mutex::scoped_lock terminate_lock(terminate_mutex_);
        boost::mutex::scoped_lock cancel_lock(cancel_mutex_);
        return !(need_to_terminate_ || need_to_cancel_);
    }

    template<class ActionSpec>
    void SingleActionServer<ActionSpec>::goalCallback(GoalHandle handle)
    {
        ROS_DEBUG("Received a new goal request");

        bool has_active_goal;
        {
            boost::mutex::scoped_lock has_active_goal_lock(has_active_goal_mutex_);
            has_active_goal = has_active_goal_;
        }
        if(has_active_goal)
        {
            ROS_DEBUG("Goal was rejected");
            handle.setRejected(Result(), "Already executing a goal. Please wait and submit again");
            return;
        }

        bool goal_accepted = true;
        if(goal_callback_)
            goal_accepted = goal_callback_(handle.getGoal());

        if(!goal_accepted)
        {
            ROS_DEBUG("Goal was rejected by user callback");
            handle.setRejected(Result(), "Goal rejected by user callback");
            return;
        }
        
        ROS_DEBUG("Goal was accepted");
        handle.setAccepted("Starting goal");

        // set the current goal to this goal handle
        {
            boost::mutex::scoped_lock has_active_goal_lock(has_active_goal_mutex_);            
            boost::recursive_mutex::scoped_lock current_goal_lock(current_goal_mutex_);
            current_goal_ = handle;
            has_active_goal_ = true;
        }

        // the execute thread it has a new job
        ROS_DEBUG("done with goal callback");
    }

    template<class ActionSpec>
    void SingleActionServer<ActionSpec>::cancelCallback(GoalHandle handle)
    {
        ROS_DEBUG("Received a new cancel request");

        boost::mutex::scoped_lock has_active_goal_lock(has_active_goal_mutex_);
        boost::mutex::scoped_lock cancel_lock(cancel_mutex_);

        if(has_active_goal_ && can_cancel_)
        {
            need_to_cancel_ = true;
        }

    }

}

#endif
