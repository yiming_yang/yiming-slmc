# actionlib_servers

This package contains a number of ActionServers with better default behavior than the SimpleActionServer.
All of the action servers in this package should be interchangeable with one another.

Clients can continue using SimpleActionClients with no issues.

In addition to this README you may also find it valuable to look at the unit tests in the `tests/src` folder.
The tests should give you an idea of how to instantiate and use one of these action servers.

## Types

### ActionServerInterface

This is an abstract base class for the Action Servers in this package. If you are going to add a new action server
to this package it must inherit from this class and implement all of its methods

### SingleActionServer

This server accepts one goal at a time. When it is executing a goal it rejects all others.

### QueueingActionServer

This server executes one goal at a time, but allows goals to queue up. The queued goals are executed in
FIFO order.

# Usage

## Execute Callback

The minimum amount of code you must write to use these action servers is an execution callback. The execution
callbacks have the following method signature `void (ActionServerInterface<ActionSpec>*, const GoalConstPtr&)`.
Each execute callback must call either the `setSucceeded` or the `setAborted` method before returning. This
signals to the client what the outcome of the goal is.

Here is an example of a minimum execution callback for the action `actionlib::TestAction`. This execute callback
simple sets the goal as a success and returns.

```
#include <actionlib_servers/action_server_interface.h>
#include <actionlib/TestAction.h>

using actionlib_servers::ActionServerInterface;

void executeCallback(ActionServerInterface<actionlib::TestAction>* action_server, const ActionServerInterface<actionlib::TestAction>::GoalConstPtr& goal)
{
	action_server->setSucceeded();
}
```

Just like in a ROS subscriber your callback can access variables inside of the method's scope. Here is an example
of a class that has an execute callback as a method

```
#include <actionlib_servers/action_server_interface.h>
#include <actionlib/TestAction.h>

using actionlib_servers::ActionServerInterface;

class Example
{
public:

	Example(int member_var) :
		member_var_(member_var)
	{}

	void executeCallback(ActionServerInterface<actionlib::TestAction>* action_server, const ActionServerInterface<actionlib::TestAction>::GoalConstPtr& goal)
	{
		Result result;
		result.result = member_var_;
		action_server->setSucceeded(result, "example execution callback");
	}
	
private:
	int member_var_;
};

```

## Goal Callback

You can also optionally provide a goal callback. This function will be called after all of the other criteria
for the server has been checked. If the goal callback returns false the server will reject the goal otherwise
it will accept it. The signature for the goal callback is `bool (const ActionServerInterface<actionlib::TestAction>::GoalConstPtr& goal)`. The goal callback should **NOT** be a long running function.

Here is an example of a minimum execution callback for the action `actionlib::TestRequest`.

```
#include <actionlib_servers/action_server_interface.h>
#include <actionlib/TestRequest.h>

using actionlib_servers::ActionServerInterface;

bool goalCallback(const ActionServerInterface<actionlib::TestRequest>::GoalConstPtr& goal)
{
	if(goal->the_result == actionlib::TestRequstGoal::TERMINATE_REJECTED)
		return false;
	
	return true;
}
```

## SingleActionServer

Use this action server if you want something like a ROS Service Server that supports long running operations
and optionally feedback.

Here is an example of a SingleActionServer which computes the Fibonacci Numbers. This is based off of the
actionlib_tutorials

```
#include <actionlib_servers/action_server_interface.h>
#include <actionlib_servers/single_action_server.h>
#include <actionlib_tutorials/FibonacciAction.h>

using actionlib_servers::ActionServerInterface;
using actionlib_servers::SingleActionServer;

class FibonacciAction
{
public:
	FibonacciAction(std::string name) :
		as_(name, boost::bind(&FibonacciAction::executeCallback, this, _1, _2), false),
		action_name_(name)
	{}

	~FibonacciAction(void)
	{}

	void executeCallback(ActionServerInterface<actionlib_tutorials::FibonacciAction>* action_server,
	                     const ActionServerInterface<actionlib_tutorials::FibonacciAction>::GoalConstPtr& goal)
	{
		ros::Rate loop_rate(1);

	    actionlib_tutorials::FibonacciResult result;
		result.sequence.push_back(0);
		result.sequence.push_back(1);

	    ROS_INFO("%s: Executing, creating fibonacci sequence of order %i with seeds %i, %i", action_name_.c_str(), goal->order, result.sequence[0], result.sequence[1]);

	    for(int i=1; i<=goal->order; i++)
		{
			if(!action_server->ok())
			{
				ROS_INFO("%s: Aborting goal", action_name_.c_str());
				action_server.setAborted(result, "Execution of goal stopped");
				return;
			}
			result.sequence.push_back(result.sequence[i] + result.sequence[i-1]);
			loop_rate.sleep();
	    }

	    action_server->setSucceeded(result);
	}

private:
	actionlib_servers::SingleActionServer<actionlib_tutorials::FibonacciAction> as_;
	std::string action_name_;
};

int main(int argc, char** argv)
{
	ros::init(argc, argv, "fibonacci");

	FibonacciAction fibonacci(ros::this_node::getName());
	ros::spin();

    return 0;
}

```

This will instantiate a fibonacci server using the SingleActionServer class. It will accept a single goal
and reject any goals while it is executing.

Consider a case where we want to reject goals right away if they don't meet some criteria. Looking again
at the fibonacci example assume that we won't calculate any goals of order 10 or higher because they take too
long. In this case we can add a goal callback. Modifying the example above we now have

```
#include <actionlib_servers/action_server_interface.h>
#include <actionlib_servers/single_action_server.h>
#include <actionlib_tutorials/FibonacciAction.h>

using actionlib_servers::ActionServerInterface;
using actionlib_servers::SingleActionServer;

class FibonacciAction
{
public:
	FibonacciAction(std::string name) :
		as_(name, name, boost::bind(&FibonacciAction::goalCallback, this, _1),
			boost::bind(&FibonacciAction::executeCallback, this, _1, _2), false),
		action_name_(name)
	{}

	~FibonacciAction(void)
	{}

	void executeCallback(ActionServerInterface<actionlib_tutorials::FibonacciAction>* action_server,
	                     const ActionServerInterface<actionlib_tutorials::FibonacciAction>::GoalConstPtr& goal)
	{
		ros::Rate loop_rate(1);

	    actionlib_tutorials::FibonacciResult result;
		result.sequence.push_back(0);
		result.sequence.push_back(1);

	    ROS_INFO("%s: Executing, creating fibonacci sequence of order %i with seeds %i, %i", action_name_.c_str(), goal->order, result.sequence[0], result.sequence[1]);

	    for(int i=1; i<=goal->order; i++)
		{
			if(!action_server->ok())
			{
				ROS_INFO("%s: Aborting goal", action_name_.c_str());
				action_server.setAborted(result, "Execution of goal stopped");
				return;
			}
			result.sequence.push_back(result.sequence[i] + result.sequence[i-1]);
			loop_rate.sleep();
	    }

	    action_server->setSucceeded(result);
	}

	bool goalCallback(const ActionServerInterface<actionlib_tutorials::FibonacciAction>::GoalConstPtr& goal)
	{
		if(goal->order > 10)
			return false;

	    return true;
	}

private:
	actionlib_servers::SingleActionServer<actionlib_tutorials::FibonacciAction> as_;
	std::string action_name_;
};

int main(int argc, char** argv)
{
	ros::init(argc, argv, "fibonacci");

	FibonacciAction fibonacci(ros::this_node::getName());
	ros::spin();

    return 0;
}

```

## QueueingActionServer

The action servers are mostly interchangeable. For example you can take the above examples and simply
change the includes and the actionserver type to switch from a SingleActionServer to a QueueingActionServer.

---
**NOTE:** While the server implementations are interchangeable the clients may be expecting a certain behavior
you should always test when changing action server implementations
---

Here is the final SingleActionServer example with a QueueingActionServer that accepts up to 5 pending requests


```
#include <actionlib_servers/action_server_interface.h>
#include <actionlib_servers/queueing_action_server.h>
#include <actionlib_tutorials/FibonacciAction.h>

using actionlib_servers::ActionServerInterface;
using actionlib_servers::QueueingActionServer;

class FibonacciAction
{
public:
	FibonacciAction(std::string name) :
		as_(name, boost::bind(&FibonacciAction::goalCallback, this, _1),
			boost::bind(&FibonacciAction::executeCallback, this, _1, _2),
			5, false),
		action_name_(name)
	{}

	~FibonacciAction(void)
	{}

	void executeCallback(ActionServerInterface<actionlib_tutorials::FibonacciAction>* action_server,
	                     const ActionServerInterface<actionlib_tutorials::FibonacciAction>::GoalConstPtr& goal)
	{
		ros::Rate loop_rate(1);

	    actionlib_tutorials::FibonacciResult result;
		result.sequence.push_back(0);
		result.sequence.push_back(1);

	    ROS_INFO("%s: Executing, creating fibonacci sequence of order %i with seeds %i, %i", action_name_.c_str(), goal->order, result.sequence[0], result.sequence[1]);

	    for(int i=1; i<=goal->order; i++)
		{
			if(!action_server->ok())
			{
				ROS_INFO("%s: Aborting goal", action_name_.c_str());
				action_server.setAborted(result, "Execution of goal stopped");
				return;
			}
			result.sequence.push_back(result.sequence[i] + result.sequence[i-1]);
			loop_rate.sleep();
	    }

	    action_server->setSucceeded(result);
	}

	bool goalCallback(const ActionServerInterface<actionlib_tutorials::FibonacciAction>::GoalConstPtr& goal)
	{
		if(goal->order > 10)
			return false;

	    return true;
	}

private:
	actionlib_servers::QueueingActionServer<actionlib_tutorials::FibonacciAction> as_;
	std::string action_name_;
};

int main(int argc, char** argv)
{
	ros::init(argc, argv, "fibonacci");

	FibonacciAction fibonacci(ros::this_node::getName());
	ros::spin();

    return 0;
}

```
