#include <gtest/gtest.h>
#include <ros/ros.h>
#include <actionlib_servers/action_server_interface.h>
#include <actionlib_servers/queueing_action_server.h>
#include <actionlib/client/simple_action_client.h>
#include <actionlib/TestAction.h>

using actionlib_servers::ActionServerInterface;
using actionlib_servers::QueueingActionServer;

void executeCallback(float block_time,
                     ActionServerInterface<actionlib::TestAction>* action_server,
                     const ActionServerInterface<actionlib::TestAction>::GoalConstPtr& goal)
{
    ASSERT_TRUE(action_server != NULL);

    ros::Rate loop_rate(10);
    ros::Duration block_duration(block_time);
    ros::Time start_time = ros::Time::now();
    while(action_server->ok() && (ros::Time::now() - start_time) < block_duration)
    {
        loop_rate.sleep();
    }

    if(action_server->ok())
        action_server->setSucceeded();
    else
        action_server->setAborted();
}

bool goalCallback(bool result,
                  const ActionServerInterface<actionlib::TestAction>::GoalConstPtr& goal)
{
    return result;
}

void noFinalStateExecuteCallback(ActionServerInterface<actionlib::TestAction>* action_server,
                                 const ActionServerInterface<actionlib::TestAction>::GoalConstPtr& goal)
{

}

TEST(SingleGoal, goalSuccess)
{
    QueueingActionServer<actionlib::TestAction> action_server("test_action",
                                                              boost::bind(&executeCallback, 0.1, _1, _2));

    actionlib::SimpleActionClient<actionlib::TestAction> action_client("test_action", true);

    ros::AsyncSpinner spinner(1);
    spinner.start();

    bool server_exists = action_client.waitForServer(ros::Duration(1));
    ASSERT_TRUE(server_exists);

    actionlib::TestGoal goal;
    goal.goal = 42;
    action_client.sendGoal(goal);

    bool finished_before_timeout = action_client.waitForResult(ros::Duration(1));
    EXPECT_TRUE(finished_before_timeout);

    actionlib::SimpleClientGoalState state = action_client.getState();
    EXPECT_EQ("SUCCEEDED", state.toString());
}

TEST(SingleGoal, noFinalStateExecuteCallback)
{
    QueueingActionServer<actionlib::TestAction> action_server("test_action",
                                                              &noFinalStateExecuteCallback);

    actionlib::SimpleActionClient<actionlib::TestAction> action_client("test_action", true);

    ros::AsyncSpinner spinner(1);
    spinner.start();

    bool server_exists = action_client.waitForServer(ros::Duration(1));
    ASSERT_TRUE(server_exists);

    actionlib::TestGoal goal;
    goal.goal = 42;
    action_client.sendGoal(goal);

    bool finished_before_timeout = action_client.waitForResult(ros::Duration(1));
    EXPECT_TRUE(finished_before_timeout);

    actionlib::SimpleClientGoalState state = action_client.getState();
    EXPECT_EQ("ABORTED", state.toString());
}

TEST(SingleGoal, cancelCancellableServer)
{
   QueueingActionServer<actionlib::TestAction> action_server("test_action",
                                                             boost::bind(&executeCallback, 5, _1, _2),
                                                             10, true);

    actionlib::SimpleActionClient<actionlib::TestAction> action_client("test_action", true);

    ros::AsyncSpinner spinner(1);
    spinner.start();

    bool server_exists = action_client.waitForServer(ros::Duration(1));
    ASSERT_TRUE(server_exists);

    actionlib::TestGoal goal;
    goal.goal = 42;
    action_client.sendGoal(goal);

    ros::Duration(1).sleep();

    action_client.cancelGoal();

    bool finished_before_timeout = action_client.waitForResult(ros::Duration(5));
    EXPECT_TRUE(finished_before_timeout);

    actionlib::SimpleClientGoalState state = action_client.getState();
    EXPECT_EQ("ABORTED", state.toString());
}

TEST(SingleGoal, checkIfGoalAfterACancellationIsExecuted)
{
   QueueingActionServer<actionlib::TestAction> action_server("test_action",
                                                             boost::bind(&executeCallback, 5, _1, _2),
                                                             10, true);

    actionlib::SimpleActionClient<actionlib::TestAction> action_client("test_action", true);

    ros::AsyncSpinner spinner(1);
    spinner.start();

    bool server_exists = action_client.waitForServer(ros::Duration(1));
    ASSERT_TRUE(server_exists);

    actionlib::TestGoal goal;
    goal.goal = 42;
    action_client.sendGoal(goal);

    ros::Duration(1).sleep();

    action_client.cancelGoal();

    bool finished_before_timeout = action_client.waitForResult(ros::Duration(5));
    EXPECT_TRUE(finished_before_timeout);

    actionlib::SimpleClientGoalState state = action_client.getState();
    EXPECT_EQ("ABORTED", state.toString());

    // now send a new goal
    action_client.sendGoal(goal);

    finished_before_timeout = action_client.waitForResult(ros::Duration(6));
    EXPECT_TRUE(finished_before_timeout);

    state = action_client.getState();
    EXPECT_EQ("SUCCEEDED", state.toString());
}

TEST(SingleGoal, cancelUncancellableServer)
{
   QueueingActionServer<actionlib::TestAction> action_server("test_action",
                                                             boost::bind(&executeCallback, 5, _1, _2));

    actionlib::SimpleActionClient<actionlib::TestAction> action_client("test_action", true);

    ros::AsyncSpinner spinner(1);
    spinner.start();

    bool server_exists = action_client.waitForServer(ros::Duration(1));
    ASSERT_TRUE(server_exists);

    actionlib::TestGoal goal;
    goal.goal = 42;
    action_client.sendGoal(goal);

    ros::Duration(2).sleep();

    actionlib::SimpleClientGoalState state = action_client.getState();
    EXPECT_EQ("ACTIVE", state.toString());

    action_client.cancelGoal();

    bool finished_before_timeout = action_client.waitForResult(ros::Duration(5));
    EXPECT_TRUE(finished_before_timeout);

    state = action_client.getState();
    EXPECT_EQ("SUCCEEDED", state.toString());
}

TEST(SingleGoal, goalCallbackRejects)
{
    QueueingActionServer<actionlib::TestAction> action_server("test_action",
                                                              boost::bind(&goalCallback, false, _1),
                                                              boost::bind(&executeCallback, .1, _1, _2));
    actionlib::SimpleActionClient<actionlib::TestAction> action_client("test_action", true);

    ros::AsyncSpinner spinner(1);
    spinner.start();

    bool server_exists = action_client.waitForServer(ros::Duration(1));
    ASSERT_TRUE(server_exists);

    actionlib::TestGoal goal;
    goal.goal = 42;
    action_client.sendGoal(goal);

    ros::Duration(1).sleep();

    action_client.cancelGoal();

    bool finished_before_timeout = action_client.waitForResult(ros::Duration(5));
    EXPECT_TRUE(finished_before_timeout);

    actionlib::SimpleClientGoalState state = action_client.getState();
    EXPECT_EQ("REJECTED", state.toString());
}

TEST(MultipleGoals, goalPreempted)
{
    QueueingActionServer<actionlib::TestAction> action_server("test_action",
                                                              boost::bind(&executeCallback, 5, _1, _2));

    actionlib::SimpleActionClient<actionlib::TestAction> accepted_action_client("test_action", true);
    actionlib::SimpleActionClient<actionlib::TestAction> preempted_action_client("test_action", true);

    ros::AsyncSpinner spinner(1);
    spinner.start();

    bool server_exists = accepted_action_client.waitForServer(ros::Duration(1));
    ASSERT_TRUE(server_exists);

    actionlib::TestGoal goal;
    goal.goal = 42;
    accepted_action_client.sendGoal(goal);

    // wait to make sure the first goal is started
    ros::Duration(1).sleep();

    // send the second goal
    preempted_action_client.sendGoal(goal);

    // wait to make sure its been processed
    ros::Duration(1).sleep();

    // cancel the second goal
    preempted_action_client.cancelGoal();

    bool finished_before_timeout = accepted_action_client.waitForResult(ros::Duration(5));
    EXPECT_TRUE(finished_before_timeout);

    actionlib::SimpleClientGoalState accepted_state = accepted_action_client.getState();
    EXPECT_EQ("SUCCEEDED", accepted_state.toString());

    actionlib::SimpleClientGoalState preempted_state = preempted_action_client.getState();
    EXPECT_EQ("ABORTED", preempted_state.toString());

}

TEST(MultipleGoals, goalsSucceed)
{
    QueueingActionServer<actionlib::TestAction> action_server("test_action",
                                                              boost::bind(&executeCallback, 5, _1, _2));
    actionlib::SimpleActionClient<actionlib::TestAction> action_client1("test_action", true);
    actionlib::SimpleActionClient<actionlib::TestAction> action_client2("test_action", true);

    ros::AsyncSpinner spinner(1);
    spinner.start();

    bool server_exists = action_client1.waitForServer(ros::Duration(1));
    ASSERT_TRUE(server_exists);

    actionlib::TestGoal goal;
    goal.goal = 42;
    action_client1.sendGoal(goal);

    ros::Duration(1).sleep();

    action_client2.sendGoal(goal);


    bool finished_before_timeout = action_client1.waitForResult(ros::Duration(5));
    EXPECT_TRUE(finished_before_timeout);

    finished_before_timeout = action_client2.waitForResult(ros::Duration(6));
    EXPECT_TRUE(finished_before_timeout);

    actionlib::SimpleClientGoalState state1 = action_client1.getState();
    EXPECT_EQ("SUCCEEDED", state1.toString());

    actionlib::SimpleClientGoalState state2 = action_client2.getState();
    EXPECT_EQ("SUCCEEDED", state2.toString());
}

TEST(MultipleGoals, maxGoalsQueued)
{
    QueueingActionServer<actionlib::TestAction> action_server("test_action",
                                                              boost::bind(&executeCallback, 5, _1, _2),
                                                              1);
    actionlib::SimpleActionClient<actionlib::TestAction> accepted_client("test_action", true);
    actionlib::SimpleActionClient<actionlib::TestAction> queued_client("test_action", true);
    actionlib::SimpleActionClient<actionlib::TestAction> rejected_client("test_action", true);

    ros::AsyncSpinner spinner(1);
    spinner.start();

    bool server_exists = accepted_client.waitForServer(ros::Duration(1));
    ASSERT_TRUE(server_exists);

    actionlib::TestGoal goal;
    goal.goal = 42;
    accepted_client.sendGoal(goal);

    ros::Duration(1).sleep();

    queued_client.sendGoal(goal);

    ros::Duration(1).sleep();

    rejected_client.sendGoal(goal);

    bool finished_before_timeout = accepted_client.waitForResult(ros::Duration(5));
    EXPECT_TRUE(finished_before_timeout);

    finished_before_timeout = queued_client.waitForResult(ros::Duration(6));
    EXPECT_TRUE(finished_before_timeout);

    finished_before_timeout = rejected_client.waitForResult(ros::Duration(6));
    EXPECT_TRUE(finished_before_timeout);

    actionlib::SimpleClientGoalState accepted_state = accepted_client.getState();
    EXPECT_EQ("SUCCEEDED", accepted_state.toString());

    actionlib::SimpleClientGoalState queued_state = queued_client.getState();
    EXPECT_EQ("SUCCEEDED", queued_state.toString());

    actionlib::SimpleClientGoalState rejected_state = rejected_client.getState();
    EXPECT_EQ("REJECTED", rejected_state.toString());
}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "queueing_action_server_test");
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
