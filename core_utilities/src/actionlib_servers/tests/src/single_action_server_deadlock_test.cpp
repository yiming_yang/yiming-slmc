#include <ros/ros.h>
#include <gtest/gtest.h>
#include <actionlib_servers/action_server_interface.h>
#include <actionlib_servers/single_action_server.h>
#include <actionlib/client/simple_action_client.h>
#include <actionlib/TestAction.h>

using actionlib_servers::ActionServerInterface;
using actionlib_servers::SingleActionServer;

void executeCallback(float block_duration, ActionServerInterface<actionlib::TestAction>* action_server,
                     const ActionServerInterface<actionlib::TestAction>::GoalConstPtr& goal)
{
    ros::Duration block_time(block_duration);
    ros::Time start_time = ros::Time::now();
    while(action_server->ok() && ((ros::Time::now() - start_time) < block_time)) {};
    action_server->setSucceeded();
}

TEST(SingleActionServer, deadlockTest)
{
    float block_duration = 5;
    SingleActionServer<actionlib::TestAction> action_server("test_action",
                                                            boost::bind(&executeCallback, block_duration, _1, _2));

    ros::AsyncSpinner spinner(1);
    spinner.start();

    actionlib::TestGoal goal;

    actionlib::SimpleActionClient<actionlib::TestAction> client1("test_action", true);
    actionlib::SimpleActionClient<actionlib::TestAction> client2("test_action", true);


    actionlib::SimpleActionClient<actionlib::TestAction> *curr_client = &client1;
    actionlib::SimpleActionClient<actionlib::TestAction> *next_client = &client2;
    actionlib::SimpleActionClient<actionlib::TestAction> *temp_client;    

    curr_client->sendGoal(goal);
    ros::Duration(.1).sleep();
    next_client->sendGoal(goal);

    ros::Rate loop_rate(100);
    ros::Duration test_time(40);
    ros::Time start_time = ros::Time::now();

    int switch_times = 0;
    while(ros::ok() && (ros::Time::now() - start_time) < test_time)
    {
        actionlib::SimpleClientGoalState goal_state = next_client->getState();
        if(goal_state == actionlib::SimpleClientGoalState::ACTIVE ||
           goal_state == actionlib::SimpleClientGoalState::SUCCEEDED ||
           goal_state == actionlib::SimpleClientGoalState::ABORTED)
        {
            switch_times++;
            temp_client = curr_client;
            curr_client = next_client;
            next_client = temp_client;
        }
        else
            next_client->sendGoal(goal);

        loop_rate.sleep();
    }

    ros::Time end_time = ros::Time::now();

    int expected_switches = (end_time - start_time).toSec()/block_duration;
    
    // expect it to have switched at least once in 20 seconds
    // otherwise something went wrong
    EXPECT_GE(switch_times, expected_switches);
}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "action_server_deadlock_test");
    testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}
