#include <ros/ros.h>
#include <gtest/gtest.h>
#include <actionlib_servers/action_server_interface.h>
#include <actionlib_servers/queueing_action_server.h>
#include <actionlib/client/simple_action_client.h>
#include <actionlib/TestAction.h>
#include <queue>

using actionlib_servers::ActionServerInterface;
using actionlib_servers::QueueingActionServer;
using std::queue;

void executeCallback(float block_duration, ActionServerInterface<actionlib::TestAction>* action_server,
                     const ActionServerInterface<actionlib::TestAction>::GoalConstPtr& goal)
{
    ros::Duration block_time(block_duration);
    ros::Time start_time = ros::Time::now();
    while(action_server->ok() && ((ros::Time::now() - start_time) < block_time)) {};
    action_server->setSucceeded();
}

TEST(QueueingActionServer, deadlockTest)
{
    float block_duration = 5;
    int max_queue_size = 2;
    QueueingActionServer<actionlib::TestAction> action_server("test_action",
                                                              boost::bind(&executeCallback, block_duration, _1, _2),
                                                              max_queue_size);

    ros::AsyncSpinner spinner(1);
    spinner.start();

    actionlib::TestGoal goal;

    queue<actionlib::SimpleActionClient<actionlib::TestAction>* > clients;

    for(int i=0; i<max_queue_size+2; i++)
    {
        clients.push(new actionlib::SimpleActionClient<actionlib::TestAction>("test_action", true));
    }

    // fill up the server's queue
    actionlib::SimpleActionClient<actionlib::TestAction>* curr_client;
    for(int i=0; i < max_queue_size+1; i++)
    {
        curr_client = clients.front();
        clients.pop();
        curr_client->sendGoal(goal);
        clients.push(curr_client);
        ros::Duration(.1).sleep();
    }

    ros::Rate loop_rate(100);
    ros::Duration test_time(40);
    ros::Time start_time = ros::Time::now();

    int switch_times = 0;
    while(ros::ok() && (ros::Time::now() - start_time) < test_time)
    {
        actionlib::SimpleClientGoalState goal_state = curr_client->getState();
        if(goal_state == actionlib::SimpleClientGoalState::ACTIVE ||
           goal_state == actionlib::SimpleClientGoalState::SUCCEEDED ||
           goal_state == actionlib::SimpleClientGoalState::ABORTED)
        {
            switch_times++;
            clients.push(curr_client);
            curr_client = clients.front();
            clients.pop();
        }
        else
            curr_client->sendGoal(goal);

        loop_rate.sleep();
    }

    ros::Time end_time = ros::Time::now();

    int expected_switches = (end_time - start_time).toSec()/block_duration;
    
    // expect it to have switched at least once in 20 seconds
    // otherwise something went wrong
    EXPECT_GE(switch_times, expected_switches);

    while(!clients.empty())
    {
        curr_client = clients.front();
        clients.pop();
        delete curr_client;
    }
}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "action_server_deadlock_test");
    testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}
