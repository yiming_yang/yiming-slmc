macro(rosdoc_gendoc)
  find_package(Doxygen)
  if(NOT DOXYGEN_FOUND)
    message(FATAL_ERROR
      "Doxygen is needed to build the documentation.")
  endif()

  # Note: Changing this won't change where the docs are generated; this will
  # always be doc/html under the WORKING_DIRECTORY of the rosdoc_lite command.
  set(DOCUMENTATION_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/doc/html)
  set(DOCUMENTATION_OUTPUT_FILES ${DOCUMENTATION_OUTPUT_PATH}/index.html ${DOCUMENTATION_OUTPUT_PATH}/index-msg.html ${DOCUMENTATION_OUTPUT_PATH}/files.html)

  file(GLOB_RECURSE DOCUMENTATION_PROJECT_INCLUDES ${PROJECT_SOURCE_DIR}/include *.h *.hh *.inc *.c *.cc *.cpp)
  file(GLOB_RECURSE DOCUMENTATION_PROJECT_SOURCES  ${PROJECT_SOURCE_DIR}/src *.h *.hh *.inc *.c *.cc *.cpp)
  file(GLOB_RECURSE DOCUMENTATION_PROJECT_MESSAGES ${PROJECT_SOURCE_DIR}/msg *.msg)

  add_custom_command(OUTPUT ${DOCUMENTATION_OUTPUT_FILES}
                     COMMAND rosdoc_lite ../${PROJECT_NAME}
                     WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}
                     DEPENDS ${PROJECT_SOURCE_DIR}/mainpage.dox ${DOCUMENTATION_PROJECT_INCLUDES} ${DOCUMENTATION_PROJECT_SOURCES} ${DOCUMENTATION_PROJECT_MESSAGES}
                     COMMENT "Generating HTML documentation")

  add_custom_target(doc ALL DEPENDS ${DOCUMENTATION_OUTPUT_FILES})
endmacro(rosdoc_gendoc)