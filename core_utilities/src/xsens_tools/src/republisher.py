#!/usr/bin/env python
# license removed for brevity
import rospy
from geometry_msgs.msg import PoseArray

def callback(data):
    global pub
    m=PoseArray()
    m.header=data.header
    m.poses.append(data.poses[19])
    pub.publish(m)
    
def republisher():
    global pub
    rospy.init_node('xsens_republisher', anonymous=True)
    pub = rospy.Publisher('mvn_pose_sub', PoseArray, queue_size=10)
    rospy.Subscriber("mvn_pose", PoseArray, callback)
    rospy.spin()

if __name__ == '__main__':
    try:
        republisher()
    except rospy.ROSInterruptException: pass
