#ifndef FLUX_TEST_NODE_H
#define FLUX_TEST_NODE_H

#include "aico/AICOsolver.h"
#include <ros/ros.h>
#include <ros/package.h>
#include <kinematic_maps/EffPosition.h>
#include <kinematic_maps/SweepFlux.h>
#include <exotica/Initialiser.h>
#include <sensor_msgs/JointState.h>
#include <geometry_msgs/PoseStamped.h>
#include <eigen_conversions/eigen_msg.h>
#include <iostream>
#include <fstream>

class FluxTestNode
{
public:
    FluxTestNode();
private:
    ros::NodeHandle nh_;
    ros::NodeHandle nhg_;
    std::string resource_path_;
    ros::Publisher jointStatePublisher_;
    void save(std::string filename, Eigen::MatrixXd& data);
};

#endif // FLUX_TEST_NODE_H
