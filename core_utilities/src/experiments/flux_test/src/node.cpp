#include "flux_test/node.h"

using namespace exotica;

#include<stdio.h>
#include <termios.h>            //termios, TCSANOW, ECHO, ICANON
#include <unistd.h>
int getch()
{
  static struct termios oldt, newt;
  tcgetattr( STDIN_FILENO, &oldt);           // save old settings
  newt = oldt;
  newt.c_lflag &= ~(ICANON);                 // disable buffering
  newt.c_cc[VMIN] = 0;
  newt.c_cc[VTIME] = 0;
  tcsetattr( STDIN_FILENO, TCSANOW, &newt);  // apply new settings

  int c = getchar();  // read character (non-blocking)

  tcsetattr( STDIN_FILENO, TCSANOW, &oldt);  // restore old settings
  return c;
}

FluxTestNode::FluxTestNode() : nh_("~"), nhg_()
{

    {
        std::string problem_name, solver_name, config_name, command_topic;
        nh_.getParam("command_topic", command_topic);
        // Declarations
        Initialiser ini;
        MotionSolver_ptr tmp_sol;
        Server_ptr ser;
        PlanningProblem_ptr tmp_prob;
        jointStatePublisher_ = nhg_.advertise<sensor_msgs::JointState>(command_topic, 1);
        int key;

        // List problems and
        std::vector<std::string> impl;
        MotionSolver_fac::Instance().listImplementations(impl);
        for(int i=0;i<impl.size();i++) ROS_INFO_STREAM("Solver: '"<<impl[i]<<"'");


        std::vector<double> pose,pose2,q0_,qT_;
        nh_.getParam("config", config_name);
        nh_.getParam("problem", problem_name);
        nh_.getParam("solver", solver_name);
        nh_.getParam("pose", pose);
        nh_.getParam("pose2", pose2);
        nh_.getParam("q0", q0_);
        nh_.getParam("qT", qT_);
        double playback_speed;
        nh_.getParam("playback_speed", playback_speed);

        ROS_INFO_STREAM("Config: "<<config_name<<"\nSolver: "<<solver_name<<"\nProblem: "<<problem_name);

        // Initialise and solve
        if(ok(ini.initialise(config_name, ser, tmp_sol, tmp_prob,problem_name,solver_name)))
        {
            exotica::AICOsolver_ptr sol=boost::static_pointer_cast<exotica::AICOsolver>(tmp_sol);
            if(!ok(sol->specifyProblem(tmp_prob))) {INDICATE_FAILURE; return;}
            exotica::AICOProblem_ptr prob = boost::static_pointer_cast<exotica::AICOProblem>(tmp_prob);


            SweepFlux_ptr flux = boost::static_pointer_cast<exotica::SweepFlux>(prob->getTaskMaps()["Flux1"]);

            {
                Eigen::Affine3d e;
                geometry_msgs::Pose p;
                p.position.x=pose[0];
                p.position.y=pose[1];
                p.position.z=pose[2];
                p.orientation.x=pose[3];
                p.orientation.y=pose[4];
                p.orientation.z=pose[5];
                p.orientation.w=pose[6];

                tf::poseMsgToEigen(p,e);
                flux->transform(e);
            }

            SweepFlux_ptr flux2 = boost::static_pointer_cast<exotica::SweepFlux>(prob->getTaskMaps()["Flux2"]);

            {
                Eigen::Affine3d e;
                geometry_msgs::Pose p;
                p.position.x=pose2[0];
                p.position.y=pose2[1];
                p.position.z=pose2[2];
                p.orientation.x=pose2[3];
                p.orientation.y=pose2[4];
                p.orientation.z=pose2[5];
                p.orientation.w=pose2[6];

                tf::poseMsgToEigen(p,e);
                flux2->transform(e);
            }

            flux->initVis(nhg_,"flux_marker");
            flux->doVis();
            flux2->initVis(nhg_,"flux_marker");
            flux2->doVis();
            ros::spinOnce();

            int T = prob->getT();
            std::vector<Eigen::VectorXd> q0(T+2,Eigen::VectorXd::Zero(prob->getW().rows()));
            for(int t=0;t<=T;t++)
            {
                double w=((double)t)/((double)T);
                for(int i=0;i<q0[t].rows();i++)
                {
                    q0[t](i)=(1.0-w)*q0_[i]+w*qT_[i];
                }
                //ROS_WARN_STREAM(q0[t]);
            }

            Eigen::MatrixXd solution;
            ROS_INFO_STREAM("Calling solve()");
            {
                ros::WallTime start_time = ros::WallTime::now();
                sol->preupdateTrajectory_=true;
                if(ok(sol->Solve(q0,solution)))
                {
                    double time=ros::Duration((ros::WallTime::now() - start_time).toSec()).toSec();
                    //ROS_INFO_STREAM("Finished solving\nSolution:\n"<<solution);
                    ROS_INFO_STREAM("Finished solving ("<<time<<"s)");
                    sol->saveCosts(resource_path_ + std::string("costs.txt"));
                    ros::Rate loop_rate(1.0/prob->getTau());
                    sensor_msgs::JointState jnt;
                    jnt.name={"lwr_arm_0_joint","lwr_arm_1_joint","lwr_arm_2_joint",
                              "lwr_arm_3_joint","lwr_arm_4_joint","lwr_arm_5_joint",
                              "lwr_arm_6_joint"};
                    jnt.position.resize(solution.cols()+1);
                    int i=0;
                    key=0;
                    bool playing=true;
                    int state=0;
                    double tt, t=0.0;
                    while(ros::ok())
                    {
                        key=getch();
                        if(key=='p')
                        {
                            if(!playing)
                            {
                                playing=true;
                                switch(state)
                                {
                                    case 0:
                                      state=1; // Play forward
                                      i=0;
                                      ROS_INFO_STREAM("Playing forward");
                                      break;
                                    case 1:
                                      state=2; // Stop and pause
                                      ROS_INFO_STREAM("Stopped");
                                      tt=1.0;
                                      i=solution.rows()-1;
                                      break;
                                    case 2:
                                      state=3; // Play backward
                                      i=solution.rows()-1;
                                      ROS_INFO_STREAM("Playing backward");
                                      break;
                                    case 3:
                                      state=0;
                                      ROS_INFO_STREAM("Stopped");
                                      tt=1.0;
                                      break;
                                }
                            }
                        }
                        else
                        {
                            key=0;
                        }


                        flux->doVis();
                        flux2->doVis();
                        jnt.header.stamp=ros::Time::now();
                        double tmp_t=(cos(t*M_PI*0.5+M_PI))*0.5+0.5;
                        i=(int)(tmp_t*((double)prob->getT()-1.0));
                        double w=tmp_t*((double)prob->getT()-1.0)-(double)i;
                        for(int j=0;j<solution.cols();j++)
                            jnt.position[j]=solution(i,j)*(1.0-w)+solution(std::min(i+1,prob->getT()-1),j)*w;
                        jnt.position[solution.cols()]=0.0;
                        jointStatePublisher_.publish(jnt);


                        if(state==1 && playing)
                        {
                            t+=1.0/(double)prob->getT()*playback_speed;
                            if(t>=1.0)
                            {
                                playing=false;
                                t=1.0;
                                ROS_INFO_STREAM("Forward playback finished");
                            }
                        }
                        if(state==3 && playing)
                        {
                            t-=1.0/(double)prob->getT()*playback_speed;
                            if(t<=0.0)
                            {
                                playing=false;
                                t=0.0;
                                ROS_INFO_STREAM("Backward playback finished");
                            }
                        }
                        if((state==0||state==2)&&playing)
                        {
                            if(tt>0.0)
                            {
                                tt-=prob->getTau();
                            }
                            else
                            {
                                playing=false;
                                ROS_INFO_STREAM("Press 'p' to play");
                            }
                        }

                        ros::spinOnce();
                        loop_rate.sleep();
                    }
                }
                else
                {
                    ROS_INFO_STREAM("Failed to find solution");
                }
            }
        }
    }
}


int main(int argc, char **argv)
{
    ros::init(argc, argv, "FluxTestAICO");
    ROS_INFO_STREAM("Started");
    FluxTestNode ex;
    ros::spin();
}
