#include "flux_test/node_baxter.h"

using namespace exotica;

FluxTestNode::FluxTestNode() : nh_("~"), nhg_()
{

    {
        // Declarations
        Initialiser ini;
        MotionSolver_ptr tmp_sol;
        Server_ptr ser;
        PlanningProblem_ptr tmp_prob;
        jointStatePublisher_ = nhg_.advertise<sensor_msgs::JointState>("/joint_states", 1);

        // List problems and
        std::vector<std::string> impl;
        MotionSolver_fac::Instance().listImplementations(impl);
        for(int i=0;i<impl.size();i++) ROS_INFO_STREAM("Solver: '"<<impl[i]<<"'");

        std::string problem_name, solver_name, config_name;
        std::vector<double> pose,pose2,q0_,qT_;
        nh_.getParam("config", config_name);
        nh_.getParam("problem", problem_name);
        nh_.getParam("solver", solver_name);
        nh_.getParam("pose", pose);
        nh_.getParam("pose2", pose2);
        nh_.getParam("q0", q0_);
        nh_.getParam("qT", qT_);

        ROS_INFO_STREAM("Config: "<<config_name<<"\nSolver: "<<solver_name<<"\nProblem: "<<problem_name);

        // Initialise and solve
        if(ok(ini.initialise(config_name, ser, tmp_sol, tmp_prob,problem_name,solver_name)))
        {
            exotica::AICOsolver_ptr sol=boost::static_pointer_cast<exotica::AICOsolver>(tmp_sol);
            if(!ok(sol->specifyProblem(tmp_prob))) {INDICATE_FAILURE; return;}
            exotica::AICOProblem_ptr prob = boost::static_pointer_cast<exotica::AICOProblem>(tmp_prob);


            SweepFlux_ptr flux = boost::static_pointer_cast<exotica::SweepFlux>(prob->getTaskMaps()["Flux1"]);

            {
                Eigen::Affine3d e;
                geometry_msgs::Pose p;
                p.position.x=pose[0];
                p.position.y=pose[1];
                p.position.z=pose[2];
                p.orientation.x=pose[3];
                p.orientation.y=pose[4];
                p.orientation.z=pose[5];
                p.orientation.w=pose[6];

                tf::poseMsgToEigen(p,e);
                flux->transform(e);
            }

            SweepFlux_ptr flux2 = boost::static_pointer_cast<exotica::SweepFlux>(prob->getTaskMaps()["Flux2"]);

            {
                Eigen::Affine3d e;
                geometry_msgs::Pose p;
                p.position.x=pose2[0];
                p.position.y=pose2[1];
                p.position.z=pose2[2];
                p.orientation.x=pose2[3];
                p.orientation.y=pose2[4];
                p.orientation.z=pose2[5];
                p.orientation.w=pose2[6];

                tf::poseMsgToEigen(p,e);
                flux2->transform(e);
            }

            flux->initVis(nhg_,"flux_marker");
            flux->doVis();
            flux2->initVis(nhg_,"flux_marker");
            flux2->doVis();
            ros::spinOnce();

            int T = prob->getT();
            std::vector<Eigen::VectorXd> q0(T+2,Eigen::VectorXd::Zero(prob->k_scenes_["FluxScene1"]->getNumJoints()));
            for(int t=0;t<=T;t++)
            {
                double w=((double)t)/((double)T);
                for(int i=0;i<q0[t].rows();i++)
                {
                    q0[t](i)=(1.0-w)*q0_[i]+w*qT_[i];
                }
                //ROS_WARN_STREAM(q0[t]);
            }

            Eigen::MatrixXd solution;
            ROS_INFO_STREAM("Calling solve()");
            {
                ros::WallTime start_time = ros::WallTime::now();
                sol->preupdateTrajectory_=true;
                if(ok(sol->Solve(q0,solution)))
                {
                    double time=ros::Duration((ros::WallTime::now() - start_time).toSec()).toSec();
                    //ROS_INFO_STREAM("Finished solving\nSolution:\n"<<solution);
                    ROS_INFO_STREAM("Finished solving ("<<time<<"s)");
                    sol->saveCosts(resource_path_ + std::string("costs.txt"));
                    save("./trajectory.txt", solution);
                    ROS_INFO_STREAM("\n"<<solution);
                    ros::Rate loop_rate(1.0/prob->getTau());
                    sensor_msgs::JointState jnt;
                    jnt.name={"right_s0","right_s1","right_e0","right_e1","right_w0","right_w1","right_w2",
                              "left_s0","left_s1","left_e0","left_e1","left_w0","left_w1","left_w2",
                              "head_pan"};
                    jnt.position.resize(solution.cols()+1);
                    int i=0;
                    while(ros::ok())
                    {
                        flux->doVis();
                        flux2->doVis();
                        jnt.header.stamp=ros::Time::now();
                        for(int j=0;j<solution.cols();j++)
                            jnt.position[j]=solution(i,j);
                        jnt.position[solution.cols()]=0.0;
                        jointStatePublisher_.publish(jnt);
                        i++;
                        if(i>=solution.rows()) i=0;
                        ros::spinOnce();
                        loop_rate.sleep();
                    }
                }
                else
                {
                    ROS_INFO_STREAM("Failed to find solution");
                }
            }
        }
    }
}

void FluxTestNode::save(std::string filename, Eigen::MatrixXd& data)
{
    std::ofstream myfile;
    myfile.open (filename);
    myfile << data;
    myfile.close();
}


int main(int argc, char **argv)
{
    ros::init(argc, argv, "FluxTestAICO");
    ROS_INFO_STREAM("Started");
    FluxTestNode ex;
    ros::spin();
}
