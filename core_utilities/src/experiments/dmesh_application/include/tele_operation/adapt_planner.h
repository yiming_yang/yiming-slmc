/*
 * adapt_planner.h
 *
 *  Created on: 18 Aug 2014
 *      Author: yiming
 */

#ifndef ADAPT_PLANNER_H_
#define ADAPT_PLANNER_H_

#include <ros/ros.h>
#include <tele_operation/goal_converter.h>
#include <dmesh_ros/dmesh_ros.h>
#include <exotica/MeshVertex.h>
#include <geometry_msgs/PoseArray.h>
#include <exotica/Initialiser.h>
#include <moveit_msgs/DisplayRobotState.h>
#include <moveit_msgs/DisplayTrajectory.h>
#include <moveit/robot_state/conversions.h>
#include <std_msgs/MultiArrayLayout.h>
#include <std_msgs/MultiArrayDimension.h>
#include <std_msgs/Float64MultiArray.h>
#include <sensor_msgs/JointState.h>
#include <lwr_driver/FriCommandJointPosition.h>
namespace exotica
{
	//	Adapt planner. Use simple IK solver to reach the goal, while adapting to environmental changes
	class AdaptPlanner
	{
		public:
			//	Default constructor
			AdaptPlanner();

			//	Default destructor
			virtual ~AdaptPlanner();

			//	Initialise the planner
			EReturn Initialisation();

		private:
			//	Goal callback
			void goalCallback(const geometry_msgs::PoseArrayConstPtr & goal);

			//	Robot state callback. There should be a simpler way of doing this. NEED TO BE IMPROVED.
			void stateCallback(const sensor_msgs::JointStateConstPtr & joints);

			// Get the end-effector pose
			void effPoseCallback(const ros::TimerEvent&);

			void timeCallback(const ros::TimerEvent&);

			//	Initialisation flag
			bool initialised_;

			//	Busy flag
			bool busy_;

			//	Boost locker
			boost::mutex::scoped_lock lock;

			//	ROS node handle
			ros::NodeHandle nh_;

			//	Goal subscriber
			ros::Subscriber goal_sub_;

			//	State subscriber
			ros::Subscriber state_sub_;

			//	Trajectory publisher
			ros::Publisher traj_pub_;

			//	Goal vertices publisher
			ros::Publisher goal_pub_;

			//	Joint state publisher
			ros::Publisher robot_state_pub_;

			//	Goal vertices
			exotica::MeshVertex goal_vertex_;

			//IKsolver_ptr ik_solver_;
			IKsolver_ptr ik_solver_;

			//MoveIt! Properties
			robot_model_loader::RobotModelLoader rm_loader_;
			robot_model::RobotModelPtr robot_model_;
			robot_state::RobotStatePtr robot_state_;
			const robot_model::JointModelGroup* group_;

			std::vector<std::string> link_names_;
			DMeshROS_Ptr dmeshros_ptr_;

			//	If not true, then just solve for once
			bool isFollow_;
			double tol_;
			//	True if use on real robot
			bool real_robot_;
			//	FRI command publisher
			ros::Publisher fri_pub_;
			ros::Publisher disp_pub_;
			lwr_driver::FriCommandJointPosition fri_cmd_;
			sensor_msgs::JointState jstate_;
			ros::Timer timer_;

			bool mug_ok_;

			geometry_msgs::PoseArrayConstPtr static_goal;
			robot_state::RobotStatePtr true_state_;
			bool freeEff_;
			std::map<std::string, int> baxter_names_;
	};
}

#endif /* ADAPT_PLANNER_H_ */
