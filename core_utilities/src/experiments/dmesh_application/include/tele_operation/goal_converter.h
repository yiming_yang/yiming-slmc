/*
 * goal_converter.h
 *
 *  Created on: 18 Aug 2014
 *      Author: yiming
 */

#ifndef GOAL_CONVERTER_H_
#define GOAL_CONVERTER_H_

#include <ik_solver/ik_solver.h>
#include <exotica/Initialiser.h>

namespace exotica
{
	//	Converter end-effector goal to mesh gaol
	class GoalConverter
	{
		public:
			//	Default constructor
			GoalConverter();

			//	Default destructor
			virtual ~GoalConverter();

			/*
			 * \brief	Initialise the converter
			 * @param	file		Initialisation file address
			 * @param	links		The eff link names
			 * @param	robot_size	Robot links size
			 */
			EReturn Initialisation(const std::string & file, const std::vector<std::string> links,
					int robot_size);

			/**
			 * \brief	Get the mesh goal from end-effector goal
			 * @param	q0			Start configuration
			 * @param	eff_goal	End-effector goal
			 * @param	solution	Original solution
			 * @param	phi			Goal pose (mesh goal)
			 */
			EReturn getMeshGoal(const Eigen::VectorXd & q0, const Eigen::VectorXd & eff_goal,
					Eigen::MatrixXd & solution, Eigen::VectorXd & phi);
		private:
			//	IK solver
			IKsolver_ptr ik_solver_;

			//	Robot links size
			int robot_size_;

			//	Link names
			std::vector<std::string> links_;

			//	Initialisation flag
			bool initialised_;
	};
}

#endif /* GOAL_CONVERTER_H_ */
