/*
 * vision_objects.h
 *
 *  Created on: 16 Dec 2014
 *      Author: yiming
 */

#ifndef EXOTENSIONS_TELE_OPERATION_INCLUDE_TELE_OPERATION_VISION_OBJECTS_H_
#define EXOTENSIONS_TELE_OPERATION_INCLUDE_TELE_OPERATION_VISION_OBJECTS_H_

#include <ros/ros.h>
#include <interactive_markers/interactive_marker_server.h>
#include <geometry_msgs/PoseArray.h>
#include <boost/thread/mutex.hpp>
#include <exotica/MeshVertex.h>
#include <tf/transform_broadcaster.h>

//	This is used to convert Karl's vision tracking results.
class vision_objects
{
	public:

};

#endif /* EXOTENSIONS_TELE_OPERATION_INCLUDE_TELE_OPERATION_VISION_OBJECTS_H_ */
