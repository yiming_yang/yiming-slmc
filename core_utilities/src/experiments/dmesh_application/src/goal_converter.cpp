/*
 * goal_converter.cpp
 *
 *  Created on: 18 Aug 2014
 *      Author: yiming
 */

#include "tele_operation/goal_converter.h"
namespace exotica
{
	GoalConverter::GoalConverter() :
			initialised_(false), robot_size_(0)
	{
		//TODO
	}

	GoalConverter::~GoalConverter()
	{
		//TODO
	}

	EReturn GoalConverter::Initialisation(const std::string & file,
			const std::vector<std::string> links, int robot_size)
	{
		Initialiser ini;
		if (file.length() == 0)
		{
			ERROR("Can't load Mid Goal config file!");
			return FAILURE;
		}
		else
			INFO("Loading Mid Goal config from: " << file);

		std::string prob_name, sol_name;
		MotionSolver_ptr sol;
		PlanningProblem_ptr prob;
		Server_ptr ser;
		if (!ok(ini.initialise(file, ser, sol, prob)))
		{
			INDICATE_FAILURE
			return FAILURE;
		}
		if (sol->type().compare("exotica::IKsolver") != 0)
		{
			INFO("Teleoperation can only use IKsolver, [" << sol->type() << "] is not supported");
			return FAILURE;
		}
		robot_size_ = robot_size;
		ik_solver_ = boost::static_pointer_cast<IKsolver>(sol);
		links_ = links;
		return SUCCESS;
	}

	EReturn GoalConverter::getMeshGoal(const Eigen::VectorXd & q0, const Eigen::VectorXd & eff_goal,
			Eigen::MatrixXd & solution, Eigen::VectorXd & phi)
	{

		if (!ok(ik_solver_->setGoal("GoalConverter", eff_goal)))
		{
			INDICATE_FAILURE
			return FAILURE;
		}
		if (!ok(ik_solver_->Solve(q0, solution)))
		{
			INDICATE_FAILURE
			return FAILURE;
		}
		return SUCCESS;
	}

}

