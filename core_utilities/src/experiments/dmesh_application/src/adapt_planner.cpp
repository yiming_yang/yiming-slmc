/*
 * adapt_planner.cpp
 *
 *  Created on: 18 Aug 2014
 *      Author: yiming
 */

#include "tele_operation/adapt_planner.h"
#include <tf/transform_broadcaster.h>

namespace exotica
{
	AdaptPlanner::AdaptPlanner() :
					nh_("/AdaptPlanner"),
					initialised_(false),
					busy_(false),
					traj_pub_(nh_.advertise<moveit_msgs::DisplayTrajectory>("/OriginalTrajectory", 1)),
					robot_state_pub_(nh_.advertise<sensor_msgs::JointState>("/move_group/fake_controller_joint_states", 1)),
					disp_pub_(nh_.advertise<moveit_msgs::DisplayRobotState>("/DMeshRobotState", 1)),
					isFollow_(false),
					real_robot_(false)
	{
		robot_model_ = rm_loader_.getModel();
		if (!robot_model_)
		{
			ROS_ERROR("Could not load robot description");
		}

		robot_state_.reset(new robot_state::RobotState(robot_model_));
		if (!robot_state_)
		{
			ROS_ERROR("Could not get RobotState from Model");
		}

		robot_state_->setToDefaultValues();
	}

	AdaptPlanner::~AdaptPlanner()
	{
//TODO
	}

	EReturn AdaptPlanner::Initialisation()
	{
		Initialiser ini;
		std::string filename;
		if (!nh_.hasParam("/AdaptPlanner/EXOTicaConfig"))
		{
			INDICATE_FAILURE
			return FAILURE;
		}
		nh_.getParam("/AdaptPlanner/EXOTicaConfig", filename);
		if (filename.length() == 0)
		{
			std::cout << "Can't load tele operation EXOTica config file!" << std::endl;
			return FAILURE;
		}
		else
			std::cout << "Loading tele operation ik solver config from: " << filename << std::endl;

		MotionSolver_ptr sol;
		PlanningProblem_ptr prob;
		Server_ptr ser;
		if (!ok(ini.initialise(filename, ser, sol, prob)))
		{
			ROS_INFO("EXOTICA Initialisation failed");
			//return FAILURE;
		}
		if (sol->type().compare("exotica::IKsolver") != 0)
		{
			std::cout << "Should use IKsolver, [" << sol->type() << "] is not supported" << std::endl;
			return FAILURE;
		}
		ik_solver_ = boost::static_pointer_cast<IKsolver>(sol);
		if (!ok(ik_solver_->specifyProblem(prob)))
		{
			INDICATE_FAILURE
			return FAILURE;
		}
		bool found = false;
		for (auto & it : ik_solver_->getProblem()->getTaskMaps())
		{
			if (it.first.compare("AdaptMeshMap") == 0)
			{
				dmeshros_ptr_ = boost::static_pointer_cast<DMeshROS>(it.second);
				found = true;
				break;
			}
		}
		if (!found)
		{
			INDICATE_FAILURE
			return FAILURE;
		}
		if (!ok(dmeshros_ptr_->setProblemPtr(prob)))
		{
			INDICATE_FAILURE
			return FAILURE;
		}

		//{	"base","lwr_arm_1_link","lwr_arm_2_link","lwr_arm_3_link","lwr_arm_4_link","lwr_arm_5_link","lwr_arm_6_link","lwr_arm_7_link","sdh_grasp_link"};

		nh_.getParam("/AdaptPlanner/IsFollow", isFollow_);
		std::string topic;
		if (!nh_.hasParam("/AdaptPlanner/PlanningGroup"))
		{
			INDICATE_FAILURE
			return FAILURE;
		}
		if (!nh_.getParam("/AdaptPlanner/PlanningGroup", topic))
		{
			INDICATE_FAILURE
			return FAILURE;
		}
		group_ = robot_state_->getJointModelGroup(topic);
		if (!nh_.hasParam("/AdaptPlanner/GoalTopic"))
		{
			INDICATE_FAILURE
			return FAILURE;
		}
		if (!nh_.getParam("/AdaptPlanner/GoalTopic", topic))
		{
			INDICATE_FAILURE
			return FAILURE;
		}
		goal_sub_ =
				nh_.subscribe<geometry_msgs::PoseArray>(topic, 1, boost::bind(&exotica::AdaptPlanner::goalCallback, this, _1));
		topic = "/joint_states";

		if (!nh_.getParam("/AdaptPlanner/RealRobot", real_robot_))
		{
			INDICATE_FAILURE
			return FAILURE;
		}
//		if (real_robot_)
//		{
//			state_sub_ =
//					nh_.subscribe<sensor_msgs::JointState>(topic, 1, boost::bind(&exotica::AdaptPlanner::stateCallback, this, _1));
//		}
		if (!nh_.hasParam("/AdaptPlanner/FriCommandTopic"))
		{
			INDICATE_FAILURE
			return FAILURE;
		}
		if (!nh_.getParam("/AdaptPlanner/FriCommandTopic", topic))
		{
			INDICATE_FAILURE
			return FAILURE;
		}
		if (real_robot_)
		{
			fri_pub_ = nh_.advertise<lwr_driver::FriCommandJointPosition>(topic, 1);
			fri_cmd_.jointPosition.resize(7);
		}
		goal_vertex_.name = "Goal";
		goal_vertex_.type = exotica::MeshVertex::GOAL;
		if (!nh_.hasParam("/AdaptPlanner/GoalLinks"))
		{
			INDICATE_FAILURE
			return FAILURE;
		}
		if (!nh_.getParam("/AdaptPlanner/GoalLinks", goal_vertex_.toLinks))
		{
			INDICATE_FAILURE
			return FAILURE;
		}
		goal_vertex_.radius = 0;
		ROS_INFO("Adapt Planner has been initialised");

//		frame_timer_ =
//				nh_.createTimer(ros::Duration(0.02), boost::bind(&exotica::AdaptPlanner::effPoseCallback, this, _1));

		if (!nh_.getParam("/AdaptPlanner/Tolerance", tol_))
		{
			INDICATE_FAILURE
			return FAILURE;
		}

		initialised_ = true;
		return SUCCESS;
	}

	void AdaptPlanner::effPoseCallback(const ros::TimerEvent&)
	{
		static tf::TransformBroadcaster br;
		tf::Transform t;
		Eigen::Vector3d eff_pose =
				robot_state_->getGlobalLinkTransform(goal_vertex_.toLinks[0]).translation();
		t.setOrigin(tf::Vector3(eff_pose(0), eff_pose(1), eff_pose(2)));
		t.setRotation(tf::Quaternion::getIdentity());
		br.sendTransform(tf::StampedTransform(t, ros::Time::now(), "base", "eff_pose"));
	}

	void AdaptPlanner::goalCallback(const geometry_msgs::PoseArrayConstPtr & goal)
	{
		LOCK(lock_);
		if (initialised_ && !busy_)
		{
			if (true)
			{
//				if (sovling_cnt_ + 1 < 10)
//					std::cout << "0" << (sovling_cnt_ + 1) << " %\n"<<std::flush;
//				else
//					std::cout << (sovling_cnt_ + 1) << " %\n"<<std::flush;
				nh_.setParam("/WeldingGoal/isOK", true);
				bool isOK = false;
				Eigen::VectorXd eff_goal(goal->poses.size() * 3);
				for (int i = 0; i < goal->poses.size(); i++)
				{
					eff_goal(3 * i) = goal->poses[i].position.x;
					eff_goal(3 * i + 1) = goal->poses[i].position.y;
					eff_goal(3 * i + 2) = goal->poses[i].position.z;
				}
				goal_vertex_.position = goal->poses[0].position;
				goal_vertex_.w = 10;
				dmeshros_ptr_->updateExternal(goal_vertex_);

				bool useOri = false;
				if (!nh_.getParam("/AdaptPlanner/useOrientation", useOri))
				{
					INDICATE_FAILURE
				}
				exotica::MeshVertex gripper_oz_vertex = goal_vertex_;
				if (useOri)
				{
					gripper_oz_vertex.name = "goal_ori";
					gripper_oz_vertex.position.z += 0.1;
					gripper_oz_vertex.w = 2;
					gripper_oz_vertex.toLinks=
					{	"pen_ori_link"};
					dmeshros_ptr_->updateExternal(gripper_oz_vertex);

				}
				Eigen::VectorXd q_in, mesh_goal, tmpmesh_goal;
				busy_ = true;

				if (!isFollow_)
					robot_state_->setToDefaultValues();
				robot_state_->copyJointGroupPositions(group_, q_in);
				//eff_goal = robot_state_->getGlobalLinkTransform("link_tip").translation();

				if (!ok(dmeshros_ptr_->getGoalLaplace(mesh_goal)))
					std::cout << "Get mesh goal failed" << std::endl;

				if (!ok(ik_solver_->setGoal("AdaptIKTask", mesh_goal)))
				//|| !ok(teleop_ik_solver_->setGoal("EffTask", eff_goal)))
				{
					INDICATE_FAILURE
					std::cout << "Adapt IK Task, set goal failed" << std::endl;
				}
				else
				{
					std::vector<Eigen::MatrixXd> solutions;
					solutions.resize(0);
					Eigen::MatrixXd solution;
					Eigen::VectorXd q_out;
					int cnt = 0;
					ros::Time start = ros::Time::now();
					ros::Duration t;

					if (ok(ik_solver_->Solve(q_in, solution)))
					{
						//solutions.push_back(solution);
						q_out = solution.row(solution.rows() - 1);
						//if (!real_robot_)
						robot_state_->setJointGroupPositions(group_, q_out);
						Eigen::VectorXd eff_actual =
								robot_state_->getGlobalLinkTransform(goal_vertex_.toLinks[0]).translation();
						Eigen::VectorXd err_vec = eff_goal - eff_actual;

						if (useOri)
						{
							Eigen::VectorXd orient_actual =
									robot_state_->getGlobalLinkTransform(gripper_oz_vertex.toLinks[0]).translation();

						}
						double err = err_vec.norm();
						moveit_msgs::DisplayRobotState msg;
						robot_state::robotStateToRobotStateMsg(*robot_state_, msg.state);
						disp_pub_.publish(msg);
						if (real_robot_)
						{
							for (int i = 0; i < q_out.rows(); i++)
								fri_cmd_.jointPosition[i] = q_out(i);
							fri_pub_.publish(fri_cmd_);
						}
						if (err < tol_)
							nh_.setParam("/WeldingGoal/isOK", true);
					}
					q_in = q_out;

				}
				busy_ = false;
			}
//			else
//			{
//				std::cout << "\nFinished solving for data recording" << std::endl;
//				ros::shutdown();
//				busy_ = true;
//			}
		}
	}

	void AdaptPlanner::stateCallback(const sensor_msgs::JointStateConstPtr & joints)
	{
		LOCK(lock_);
		robot_state::jointStateToRobotState(*joints, *robot_state_);

	}
}

int main(int argc, char** argv)
{
	ros::init(argc, argv, "AdaptPlanner");
	ros::AsyncSpinner sp(2);
	sp.start();
	exotica::AdaptPlanner adp;
	adp.Initialisation();
	ros::waitForShutdown();
	return 0;
}

