/*
 * dummy_goal.cpp
 *
 *  Created on: 23 Jul 2014
 *      Author: yiming
 */

#include <ros/ros.h>

#include <interactive_markers/interactive_marker_server.h>
#include <geometry_msgs/PoseArray.h>
#include <boost/thread/mutex.hpp>
#include <exotica/MeshVertex.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include <kdl_parser/kdl_parser.hpp>
geometry_msgs::PoseArray goal_pose_;
boost::mutex::scoped_lock lock_;
visualization_msgs::Marker marker;
bool increase = true;
double current = 0;
geometry_msgs::Pose tmp;
void timeCallback(const ros::TimerEvent&) {
	static tf::TransformBroadcaster br;

	static tf::TransformListener listener;
	tf::StampedTransform target;
	try {
		listener.lookupTransform("/base", "/cookie_crisp", ros::Time(0),
				target);
	} catch (tf::TransformException ex) {
		ROS_ERROR("%s", ex.what());
		ros::Duration(.001).sleep();
	}

	target.setOrigin(
			tf::Vector3(target.getOrigin().x() + 0.025,
					target.getOrigin().y() - 0.02,
					target.getOrigin().z() + 0.04));
	tf::poseTFToMsg(target, tmp);
	KDL::Frame target_frame(
			KDL::Rotation::Quaternion(target.getRotation().x(),
					target.getRotation().y(), target.getRotation().z(),
					target.getRotation().w()),
			KDL::Vector(target.getOrigin().x(), target.getOrigin().y(),
					target.getOrigin().z()));

	if (increase) {
		if (current <= 0.1) {
			current += 0.001;
		} else
			increase = false;
	} else {
		if (current >= -0.1) {
			current -= 0.001;
		} else
			increase = true;
	}

	KDL::Frame offset = KDL::Frame(KDL::Vector(current, .13, 0));

	target_frame = target_frame * offset;
	goal_pose_.poses[0].position.x = target_frame.p.x();
	goal_pose_.poses[0].position.y = target_frame.p.y();
	goal_pose_.poses[0].position.z = target_frame.p.z();
	target_frame.M.GetQuaternion(goal_pose_.poses[0].orientation.x,
			goal_pose_.poses[0].orientation.y,
			goal_pose_.poses[0].orientation.z,
			goal_pose_.poses[0].orientation.w);
}

int main(int argc, char** argv) {
	ros::init(argc, argv, "plane_object");

	ros::NodeHandle nh;
	ros::Publisher vis_pub = nh.advertise<visualization_msgs::Marker>(
			"vision_goal_marker", 0);
	ros::Publisher vis_pub2 = nh.advertise<visualization_msgs::Marker>(
			"target_point", 0);
	ros::Publisher pub = nh.advertise<geometry_msgs::PoseArray>(
			"/dummy_goal/goal", 10);

	marker.header.frame_id = "/base";
	marker.header.stamp = ros::Time();
	marker.ns = "my_namespace";
	marker.id = 0;
	marker.type = visualization_msgs::Marker::CUBE;
	marker.action = visualization_msgs::Marker::ADD;
	marker.pose.position.x = 0.6;
	marker.pose.position.y = 0;
	marker.pose.position.z = 0.35;
	marker.pose.orientation.x = 0.0;
	marker.pose.orientation.y = 0.0;
	marker.pose.orientation.z = 0.0;
	marker.pose.orientation.w = 1.0;
	marker.scale.x = 0.275;
	marker.scale.y = 0.19;
	marker.scale.z = 0.06;
	marker.color.r = 1.0;
	marker.color.a = .4;
	ros::Timer frame_timer = nh.createTimer(ros::Duration(0.02), timeCallback);
	goal_pose_.header.frame_id = "/base";
	goal_pose_.poses.resize(1);

	visualization_msgs::Marker marker2 = marker;
	marker2.type = visualization_msgs::Marker::SPHERE;
	marker2.scale.x = 0.05;
	marker2.scale.y = 0.05;
	marker2.scale.z = 0.05;
	marker2.color.a = 1;
	while (ros::ok()) {
		marker2.pose = goal_pose_.poses[0];
		ros::spinOnce();
		marker.pose = tmp;
		vis_pub.publish(marker);
		vis_pub2.publish(marker2);
		pub.publish(goal_pose_);
		ros::Duration(0.02).sleep();
	}
	ros::waitForShutdown();
	return 0;
}
