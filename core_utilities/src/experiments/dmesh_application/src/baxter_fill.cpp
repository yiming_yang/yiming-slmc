/*
 * baxter_fill.cpp
 *
 *  Created on: 30 Jan 2015
 *      Author: yiming
 */

#include "tele_operation/adapt_planner.h"
#include <tf/transform_broadcaster.h>

namespace exotica
{
	AdaptPlanner::AdaptPlanner() :
					nh_("/AdaptPlanner"),
					initialised_(false),
					busy_(false),
					traj_pub_(nh_.advertise<moveit_msgs::DisplayTrajectory>("/OriginalTrajectory", 1)),
					robot_state_pub_(nh_.advertise<sensor_msgs::JointState>("/BaxterRepublish/joint_states", 1)),
					disp_pub_(nh_.advertise<moveit_msgs::DisplayRobotState>("/DMeshRobotState", 1)),
					isFollow_(false),
					real_robot_(false),
					mug_ok_(false)
	{
		robot_model_ = rm_loader_.getModel();
		if (!robot_model_)
		{
			ROS_ERROR("Could not load robot description");
		}

		robot_state_.reset(new robot_state::RobotState(robot_model_));
		if (!robot_state_)
		{
			ROS_ERROR("Could not get RobotState from Model");
		}

		robot_state_->setToDefaultValues();

		true_state_.reset(new robot_state::RobotState(robot_model_));
		true_state_->setToDefaultValues();
	}

	AdaptPlanner::~AdaptPlanner()
	{
//TODO
	}

	EReturn AdaptPlanner::Initialisation()
	{
		Initialiser ini;
		std::string filename;
		if (!nh_.hasParam("/AdaptPlanner/EXOTicaConfig"))
		{
			INDICATE_FAILURE
			return FAILURE;
		}
		nh_.getParam("/AdaptPlanner/EXOTicaConfig", filename);
		if (filename.length() == 0)
		{
			std::cout << "Can't load tele operation EXOTica config file!" << std::endl;
			return FAILURE;
		}
		else
			std::cout << "Loading tele operation ik solver config from: " << filename << std::endl;

		MotionSolver_ptr sol;
		PlanningProblem_ptr prob;
		Server_ptr ser;
		if (!ok(ini.initialise(filename, ser, sol, prob)))
		{
			ROS_INFO("EXOTICA Initialisation failed");
			//return FAILURE;
		}
		if (sol->type().compare("exotica::IKsolver") != 0)
		{
			std::cout << "Should use IKsolver, [" << sol->type() << "] is not supported" << std::endl;
			return FAILURE;
		}
		ik_solver_ = boost::static_pointer_cast<IKsolver>(sol);
		if (!ok(ik_solver_->specifyProblem(prob)))
		{
			INDICATE_FAILURE
			return FAILURE;
		}
		bool found = false;
		for (auto & it : ik_solver_->getProblem()->getTaskMaps())
		{
			if (it.first.compare("AdaptMeshMap") == 0)
			{
				dmeshros_ptr_ = boost::static_pointer_cast<DMeshROS>(it.second);
				found = true;
				break;
			}
		}
		if (!found)
		{
			INDICATE_FAILURE
			return FAILURE;
		}
		if (!ok(dmeshros_ptr_->setProblemPtr(prob)))
		{
			INDICATE_FAILURE
			return FAILURE;
		}

		//{	"base","lwr_arm_1_link","lwr_arm_2_link","lwr_arm_3_link","lwr_arm_4_link","lwr_arm_5_link","lwr_arm_6_link","lwr_arm_7_link","sdh_grasp_link"};

		nh_.getParam("/AdaptPlanner/IsFollow", isFollow_);
		std::string topic;
		if (!nh_.hasParam("/AdaptPlanner/PlanningGroup"))
		{
			INDICATE_FAILURE
			return FAILURE;
		}
		if (!nh_.getParam("/AdaptPlanner/PlanningGroup", topic))
		{
			INDICATE_FAILURE
			return FAILURE;
		}
		group_ = robot_state_->getJointModelGroup(topic);

		topic = "/robot/joint_states";

		if (!nh_.getParam("/AdaptPlanner/RealRobot", real_robot_))
		{
			INDICATE_FAILURE
			return FAILURE;
		}

		if (real_robot_)
		{
			state_sub_ =
					nh_.subscribe<sensor_msgs::JointState>(topic, 1, boost::bind(&exotica::AdaptPlanner::stateCallback, this, _1));
		}

		if (!nh_.getParam("/BaxterFill/FreeEff", freeEff_))
		{
			INDICATE_FAILURE
			return FAILURE;
		}
		if (!freeEff_)
		{
			if (!nh_.hasParam("/AdaptPlanner/GoalTopic"))
			{
				INDICATE_FAILURE
				return FAILURE;
			}
			if (!nh_.getParam("/AdaptPlanner/GoalTopic", topic))
			{
				INDICATE_FAILURE
				return FAILURE;
			}
			goal_sub_ =
					nh_.subscribe<geometry_msgs::PoseArray>(topic, 1, boost::bind(&exotica::AdaptPlanner::goalCallback, this, _1));
		}
		if (!nh_.hasParam("/AdaptPlanner/FriCommandTopic"))
		{
			INDICATE_FAILURE
			return FAILURE;
		}
		if (!nh_.getParam("/AdaptPlanner/FriCommandTopic", topic))
		{
			INDICATE_FAILURE
			return FAILURE;
		}
		if (real_robot_)
		{
			fri_pub_ = nh_.advertise<lwr_driver::FriCommandJointPosition>(topic, 1);
			fri_cmd_.jointPosition.resize(7);
		}
		goal_vertex_.name = "Goal";
		goal_vertex_.type = exotica::MeshVertex::GOAL;
		if (!nh_.hasParam("/AdaptPlanner/GoalLinks"))
		{
			INDICATE_FAILURE
			return FAILURE;
		}
		if (!nh_.getParam("/AdaptPlanner/GoalLinks", goal_vertex_.toLinks))
		{
			INDICATE_FAILURE
			return FAILURE;
		}
		goal_vertex_.radius = 0;
		ROS_INFO("Adapt Planner has been initialised");

//		frame_timer_ =
//				nh_.createTimer(ros::Duration(0.02), boost::bind(&exotica::AdaptPlanner::effPoseCallback, this, _1));

		if (!nh_.getParam("/AdaptPlanner/Tolerance", tol_))
		{
			INDICATE_FAILURE
			return FAILURE;
		}

		nh_.setParam("/BaxterFill/startFill", false);

		found = false;
		for (int i = 0; i < robot_model_->getSRDF()->getGroupStates().size(); i++)
		{
			if (robot_model_->getSRDF()->getGroupStates()[i].name_.compare("FillStart") == 0)
			{
				for (auto & it : robot_model_->getSRDF()->getGroupStates()[i].joint_values_)
				{
					robot_state_->setVariablePosition(it.first, it.second[0]);
				}
				found = true;
			}
		}
		if (found)
		{
			robot_state_->update(true);
			moveit_msgs::DisplayRobotState msg;
			robot_state::robotStateToRobotStateMsg(*robot_state_, msg.state);
			disp_pub_.publish(msg);
			moveit::core::robotStateToJointStateMsg(*robot_state_, jstate_);
			robot_state_pub_.publish(jstate_);
			std::cerr << "Going to start pose" << std::endl;
		}
		else
		{
			std::cerr << "Robot pose 'FillStart' not found" << std::endl;
			INDICATE_FAILURE
			;
			return MMB_NIN;
		}

		if (freeEff_)
			timer_ =
					nh_.createTimer(ros::Duration(0.02), boost::bind(&exotica::AdaptPlanner::timeCallback, this, _1));
		std::vector<std::string> tmp = { "right_s0", "right_s1", "right_e0", "right_e1", "right_w0",
				"right_w1", "right_w2", "left_s0", "left_s1", "left_e0", "left_e1", "left_w0",
				"left_w1", "left_w2" };
		for (int i = 0; i < tmp.size(); i++)
			baxter_names_[tmp[i]] = -1;
		initialised_ = true;
		return SUCCESS;
	}

	void AdaptPlanner::timeCallback(const ros::TimerEvent&)
	{
		static bool first = true;
		if (first)
		{
			geometry_msgs::PoseArray goal;
			goal.poses.resize(1);
			goal.poses[0].position.x = 0.6;
			goal.poses[0].position.y = 0;
			goal.poses[0].position.z = 0.12;
			static_goal.reset(new geometry_msgs::PoseArray(goal));
			ros::Duration(4).sleep();
			first = false;
		}
		goalCallback(static_goal);
	}
	void AdaptPlanner::effPoseCallback(const ros::TimerEvent&)
	{
		static tf::TransformBroadcaster br;
		tf::Transform t;
		Eigen::Vector3d eff_pose =
				robot_state_->getGlobalLinkTransform(goal_vertex_.toLinks[0]).translation();
		t.setOrigin(tf::Vector3(eff_pose(0), eff_pose(1), eff_pose(2)));
		t.setRotation(tf::Quaternion::getIdentity());
		br.sendTransform(tf::StampedTransform(t, ros::Time::now(), "base", "eff_pose"));
	}

	void AdaptPlanner::goalCallback(const geometry_msgs::PoseArrayConstPtr & goal)
	{
		LOCK(lock_);
		if (initialised_ && !busy_)
		{
			if (true)
			{
//				if (sovling_cnt_ + 1 < 10)
//					std::cout << "0" << (sovling_cnt_ + 1) << " %\n"<<std::flush;
//				else
//					std::cout << (sovling_cnt_ + 1) << " %\n"<<std::flush;
				Eigen::VectorXd eff_goal(goal->poses.size() * 3);
				for (int i = 0; i < goal->poses.size(); i++)
				{
					eff_goal(3 * i) = goal->poses[i].position.x;
					eff_goal(3 * i + 1) = goal->poses[i].position.y;
					eff_goal(3 * i + 2) = goal->poses[i].position.z;
				}
				goal_vertex_.position = goal->poses[0].position;

				bool fill = false;
				nh_.getParam("/BaxterFill/startFill", fill);

				exotica::MeshVertex mug_x = goal_vertex_;
				mug_x.name = "mug_x_goal";
				mug_x.position.y -= 0.2;
				mug_x.w = 100;
				mug_x.toLinks=
				{	"mug_x_link"};

				exotica::MeshVertex mug_y = goal_vertex_;
				mug_y.name = "mug_y_goal";
				mug_y.position.x += 0.2;
				mug_y.w = 100;
				mug_y.toLinks=
				{	"mug_y_link"};

				if (!fill)
				{
					goal_vertex_.w = 10;
					dmeshros_ptr_->updateExternal(goal_vertex_);
					dmeshros_ptr_->updateExternal(mug_x);
					dmeshros_ptr_->updateExternal(mug_y);
				}

				Eigen::VectorXd q_in, mesh_goal, tmpmesh_goal;
				busy_ = true;

				if (!isFollow_)
					robot_state_->setToDefaultValues();
				robot_state_->copyJointGroupPositions(group_, q_in);
				//eff_goal = robot_state_->getGlobalLinkTransform("link_tip").translation();

				if (!ok(dmeshros_ptr_->getGoalLaplace(mesh_goal)))
					std::cout << "Get mesh goal failed" << std::endl;

				if (!ok(ik_solver_->setGoal("AdaptIKTask", mesh_goal)))
				//|| !ok(teleop_ik_solver_->setGoal("EffTask", eff_goal)))
				{
					INDICATE_FAILURE
					std::cout << "Adapt IK Task, set goal failed" << std::endl;
				}
				else
				{
					std::vector<Eigen::MatrixXd> solutions;
					solutions.resize(0);
					Eigen::MatrixXd solution;
					Eigen::VectorXd q_out;
					int cnt = 0;
					ros::Time start = ros::Time::now();
					ros::Duration t;
					Eigen::VectorXd tmp(3),tmp1(3),tmp2(3);

					tmp1 = robot_state_->getGlobalLinkTransform("bottle_tip_link").translation();
					tmp2 = robot_state_->getGlobalLinkTransform("bottle_link").translation();
					if (ok(ik_solver_->Solve(q_in, solution)))
					{
						//solutions.push_back(solution);
						q_out = solution.row(solution.rows() - 1);
						//if (!real_robot_)
						robot_state_->setJointGroupPositions(group_, q_out);
						Eigen::VectorXd eff_actual =
								robot_state_->getGlobalLinkTransform(goal_vertex_.toLinks[0]).translation();
						Eigen::VectorXd err_vec = eff_goal - eff_actual;

						if (fill)
						{
//							if (!mug_ok_)
//							{
//								dmeshros_ptr_->removeVertex(goal_vertex_.name);
//								dmeshros_ptr_->removeVertex("mug_top_goal");
//							}

							exotica::MeshVertex tmp_mug = goal_vertex_;
							if (!freeEff_)
							{
								tmp =
										robot_state_->getGlobalLinkTransform(goal_vertex_.toLinks[0]).translation();
							}
							else
							{
								tmp =
										true_state_->getGlobalLinkTransform(goal_vertex_.toLinks[0]).translation();
							}
							tmp_mug.position.x = tmp(0);
							tmp_mug.position.y = tmp(1);
							tmp_mug.position.z = tmp(2);
							if (!dmeshros_ptr_->hasActiveObstacle())
							{

								dmeshros_ptr_->updateExternal(goal_vertex_);

								mug_x.position = goal_vertex_.position;
								mug_x.position.y -= 0.2;
								mug_y.position = goal_vertex_.position;
								mug_y.position.x += 0.2;
								dmeshros_ptr_->updateExternal(mug_x);
								dmeshros_ptr_->updateExternal(mug_y);
							}
							else
							{
								dmeshros_ptr_->updateExternal(tmp_mug);
								mug_x.position = tmp_mug.position;
								mug_x.position.y -= 0.2;

								mug_y.position = tmp_mug.position;
								mug_y.position.x += 0.2;
								dmeshros_ptr_->updateExternal(mug_x);
								dmeshros_ptr_->updateExternal(mug_y);
							}

							exotica::MeshVertex bottle_tip = tmp_mug;
							bottle_tip.name = "bottle_tip_goal";
							bottle_tip.position.z += 0.15;
							bottle_tip.w = 100;
							bottle_tip.toLinks=
							{	"bottle_tip_link"};
							dmeshros_ptr_->updateExternal(bottle_tip);

							exotica::MeshVertex bottle_base = tmp_mug;
							bottle_base.name = "bottle_goal";
							bottle_base.w = 2;
							bottle_base.radius = 0.1;
							bottle_base.position.z += 0.3;
							bottle_base.toLinks=
							{	"bottle_link"};
							dmeshros_ptr_->updateExternal(bottle_base);
							mug_ok_ = true;

						}
						double err = err_vec.norm();
						moveit_msgs::DisplayRobotState msg;
						robot_state::robotStateToRobotStateMsg(*robot_state_, msg.state);
						disp_pub_.publish(msg);
						moveit::core::robotStateToJointStateMsg(*robot_state_, jstate_);
						robot_state_pub_.publish(jstate_);
						if (err < tol_)
							nh_.setParam("/WeldingGoal/isOK", true);
					}
					q_in = q_out;

				}
				busy_ = false;
			}
//			else
//			{
//				std::cout << "\nFinished solving for data recording" << std::endl;
//				ros::shutdown();
//				busy_ = true;
//			}
		}
	}

	void AdaptPlanner::stateCallback(const sensor_msgs::JointStateConstPtr & joints)
	{
		LOCK(lock_);
		static bool first = true;
		if (first)
		{
			for (int i = 0; i < joints->name.size(); i++)
			{
				if (baxter_names_.find(joints->name[i]) != baxter_names_.end())
					baxter_names_.find(joints->name[i])->second = i;
			}
			first = false;
		}
		for (auto & it : baxter_names_)
		{
			true_state_->setVariablePosition(it.first, joints->position[it.second]);
		}
		true_state_->update(true);
	}
}

int main(int argc, char** argv)
{
	ros::init(argc, argv, "AdaptPlanner");
	ros::AsyncSpinner sp(2);
	sp.start();
	exotica::AdaptPlanner adp;
	adp.Initialisation();
	ros::waitForShutdown();
	return 0;
}

