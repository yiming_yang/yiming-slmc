/*
 * dummy_object.cpp
 *
 *  Created on: 23 Jul 2014
 *      Author: yiming
 */

#include <ros/ros.h>

#include <interactive_markers/interactive_marker_server.h>
#include <geometry_msgs/PoseArray.h>
#include <boost/thread/mutex.hpp>
#include <exotica/MeshVertexArray.h>
exotica::MeshVertexArray obs_pose_;
boost::mutex::scoped_lock lock_;
bool update;
void obsFeedback(const visualization_msgs::InteractiveMarkerFeedbackConstPtr &feedback)
{
	boost::mutex::scoped_lock(lock_);
	obs_pose_.vertices.resize(1);
	obs_pose_.vertices[0].name = feedback->marker_name;
	obs_pose_.vertices[0].type = exotica::MeshVertex::OBSTACLE_TO_ALL;
	obs_pose_.vertices[0].position = feedback->pose.position;
	obs_pose_.vertices[0].toLinks.clear();
	obs_pose_.vertices[0].radius = 0.07;
	obs_pose_.vertices[0].w=2;
	update = true;
}

int main(int argc, char** argv)
{
	ros::init(argc, argv, "dummy_object");
	ros::NodeHandle nh;
	ros::Publisher pub =
			nh.advertise<exotica::MeshVertexArray>("/dummy_object/external_objects", 10);
	ros::AsyncSpinner sp(1);
	interactive_markers::InteractiveMarkerServer server("dummy_obs_server");
	visualization_msgs::InteractiveMarker int_obs_marker, int_obs_marker2;
	visualization_msgs::Marker obs_marker;
	obs_marker.type = visualization_msgs::Marker::SPHERE;
	obs_marker.scale.x = 0.14;
	obs_marker.scale.y = 0.14;
	obs_marker.scale.z = 0.14;
	obs_marker.color.r = 0.5;
	obs_marker.color.g = 0.5;
	obs_marker.color.b = 0.5;
	obs_marker.color.a = 1;

	visualization_msgs::InteractiveMarkerControl obs_control;
	obs_control.always_visible = true;
	obs_control.markers.push_back(obs_marker);
	int_obs_marker.header.frame_id = "/base";
	int_obs_marker.name = "obstacle1";
	int_obs_marker.pose.position.x = 2;
	int_obs_marker.pose.position.y = 2;
	int_obs_marker.pose.position.z = 2;

	int_obs_marker.controls.push_back(obs_control);
	int_obs_marker.controls[0].interaction_mode =
			visualization_msgs::InteractiveMarkerControl::MOVE_3D;

	int_obs_marker2 = int_obs_marker;
	int_obs_marker2.name = "obstacle2";

	update = false;
	server.insert(int_obs_marker);
	server.setCallback(int_obs_marker.name, &obsFeedback);
	server.insert(int_obs_marker2);
	server.setCallback(int_obs_marker2.name, &obsFeedback);
	server.applyChanges();

	ros::Duration d(0.01);
	sp.start();
	while (ros::ok())
	{
		if (update)
		{
			pub.publish(obs_pose_);
			update = false;
		}
		ros::spinOnce();
		d.sleep();
	}
}

