/*
 * vision_obs.cpp
 *
 *  Created on: 17 Dec 2014
 *      Author: yiming
 */

#include <ros/ros.h>

#include <interactive_markers/interactive_marker_server.h>
#include <geometry_msgs/PoseArray.h>
#include <boost/thread/mutex.hpp>
#include <exotica/MeshVertex.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
exotica::MeshVertex obs_pose_;
boost::mutex::scoped_lock lock_;

int main(int argc, char** argv) {
	ros::init(argc, argv, "dummy_object");
	ros::NodeHandle nh;
	ros::Publisher pub = nh.advertise<exotica::MeshVertex>(
			"/dummy_object/external_objects", 10);
	ros::Publisher vis_pub = nh.advertise<visualization_msgs::Marker>(
			"vision_obs_marker", 0);
	ros::AsyncSpinner sp(1);
	visualization_msgs::Marker obs_marker;
	obs_marker.header.frame_id = "/base";
	obs_marker.type = visualization_msgs::Marker::CUBE;
	obs_marker.scale.x = 0.185;
	obs_marker.scale.y = 0.1;
	obs_marker.scale.z = 0.13;
	obs_marker.color.r = 0.5;
	obs_marker.color.g = 0.5;
	obs_marker.color.b = 0.5;
	obs_marker.color.a = 1.0;

	obs_pose_.name = "obstalce1";
	obs_pose_.type = exotica::MeshVertex::OBSTACLE_TO_ALL;
	obs_pose_.toLinks.clear();
	obs_pose_.radius = 0.1;

	static tf::TransformBroadcaster br;

	static tf::TransformListener listener;
	tf::StampedTransform target;

	ros::Duration d(0.01);
	sp.start();
	while (ros::ok()) {
		try {
			listener.lookupTransform("/base", "/netgear_box", ros::Time(0),
					target);
		} catch (tf::TransformException ex) {
			ROS_ERROR("%s", ex.what());
			ros::Duration(.001).sleep();
		}
		tf::poseTFToMsg(target, obs_marker.pose);

		obs_marker.pose.position.x += 0.025;
		obs_marker.pose.position.y -= 0.02;
		obs_marker.pose.position.z += 0.02;
		obs_pose_.position = obs_marker.pose.position;

		vis_pub.publish(obs_marker);
		pub.publish(obs_pose_);
		d.sleep();
	}
}

