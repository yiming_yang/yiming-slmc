/*
 * welding_goal.cpp
 *
 *  Created on: 15 Jan 2015
 *      Author: yiming
 */
#include <ros/ros.h>

#include <interactive_markers/interactive_marker_server.h>
#include <geometry_msgs/PoseArray.h>
#include <boost/thread/mutex.hpp>
#include <exotica/MeshVertex.h>
geometry_msgs::PoseArray goal_pose_;
geometry_msgs::Point base_pose_;
boost::mutex::scoped_lock lock_;
bool update;

int cnt = 0;
double increase = true;
void goalFeedback(const visualization_msgs::InteractiveMarkerFeedbackConstPtr &feedback)
{
	boost::mutex::scoped_lock(lock_);
	base_pose_ = feedback->pose.position;
	base_pose_.z += 0.033;
	update = true;
}

int main(int argc, char** argv)
{
	ros::init(argc, argv, "welding_goal");
	ros::NodeHandle nh;
	ros::Publisher pub = nh.advertise<geometry_msgs::PoseArray>("/welding_goal/goal", 1);
	ros::AsyncSpinner sp(1);
	interactive_markers::InteractiveMarkerServer server("welding_goal_server");
	visualization_msgs::InteractiveMarker int_marker;
	visualization_msgs::Marker base_marker;
	base_marker.type = visualization_msgs::Marker::CUBE;
	base_marker.scale.x = 0.1914;
	base_marker.scale.y = 0.2723;
	base_marker.scale.z = 0.06;
	base_marker.color.r = 0.6;
	base_marker.color.g = 0.6;
	base_marker.color.b = 0;
	base_marker.color.a = 0.9;

	visualization_msgs::Marker top_marker;
	top_marker.type = visualization_msgs::Marker::LINE_STRIP;
	top_marker.scale.x = 0.01;
	top_marker.color.r = 0;
	top_marker.color.g = 0;
	top_marker.color.b = 0;
	top_marker.color.a = 1;
	top_marker.pose.orientation.z = 1;
	top_marker.points.resize(3);
	top_marker.points[0].x = -0.0957;
	top_marker.points[0].y = -0.1361;
	top_marker.points[0].z = 0.032;
	top_marker.points[1].x = 0.0957;
	top_marker.points[1].y = 0.0;
	top_marker.points[1].z = 0.032;
	top_marker.points[2].x = -0.0957;
	top_marker.points[2].y = 0.1361;
	top_marker.points[2].z = 0.032;

	goal_pose_.header.frame_id = "/base";
	goal_pose_.poses.resize(1);

	visualization_msgs::InteractiveMarkerControl goal_control;
	goal_control.always_visible = true;
	goal_control.markers.push_back(base_marker);
	goal_control.markers.push_back(top_marker);
	int_marker.header.frame_id = "/base";
	int_marker.name = "Goal";
	int_marker.pose.position.x = 0.7;
	int_marker.pose.position.y = 0;
	int_marker.pose.position.z = 0;
	base_pose_ = int_marker.pose.position;
	base_pose_.z += 0.033;
	int_marker.controls.push_back(goal_control);
	int_marker.controls[0].interaction_mode = visualization_msgs::InteractiveMarkerControl::MOVE_3D;
	server.insert(int_marker);
	server.setCallback(int_marker.name, &goalFeedback);
	server.applyChanges();
	std::vector<double> goal_y, goal_x;
	int steps = 5000;
	goal_y.resize(steps);
	goal_x.resize(steps);
	double delta_x = 0.1914 / (steps / 2), delta_y = 0.1361 / (steps / 2);
	for (int i = 0; i < steps / 2; ++i)
	{
		goal_x[i] = 0.0957 - i * delta_x;
		goal_y[i] = i * delta_y - 0.1361;
	}
	for (int i = 0; i < steps / 2; ++i)
	{
		goal_x[i + steps/2] = -0.0957 + i * delta_x;
		goal_y[i + steps/2] = i * delta_y;
	}

	ros::Duration d(0.001);
	sp.start();
	std::cout << "Press to start goal publishing" << std::endl;
	getchar();
	bool isOK = false;
	bool first = true;
	while (ros::ok())
	{
		nh.getParam("/WeldingGoal/isOK", isOK);

		goal_pose_.poses[0].position = base_pose_;
		if (increase)
		{
			goal_pose_.poses[0].position.x += goal_x[cnt];
			goal_pose_.poses[0].position.y += goal_y[cnt];
			if (isOK)
			{
				cnt++;
				if (cnt > steps - 1)
				{
					increase = false;
					cnt = steps - 1;
				}
			}
		}
		else
		{
			goal_pose_.poses[0].position.x += goal_x[cnt];
			goal_pose_.poses[0].position.y += goal_y[cnt];
			if (isOK)
			{
				cnt--;
				if (cnt < 0)
				{
					increase = true;
					cnt = 0;
				}
			}
		}
		//std::cout << "Goal x=" << goal_pose_.poses[0].position.x << " y=" << goal_pose_.poses[0].position.y << " z=" << goal_pose_.poses[0].position.z << std::endl;

		pub.publish(goal_pose_);
		ros::spinOnce();
		d.sleep();
	}
	std::cout << "Goal publishing wait for shutdown" << std::endl;
	ros::waitForShutdown();
	return 0;
}

