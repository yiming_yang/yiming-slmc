/*
 * xsens_obs.cpp
 *
 *  Created on: 21 Jan 2015
 *      Author: yiming
 */

#include <ros/ros.h>
#include <kdl_conversions/kdl_msg.h>
#include <geometry_msgs/PoseArray.h>
#include <boost/thread/mutex.hpp>
#include <exotica/MeshVertexArray.h>
#include <visualization_msgs/MarkerArray.h>
#include <string>
class xsens_obs
{
	public:
		xsens_obs(const std::string & sub_topic, const std::string & pub_topic) :
				nh_("/xsens_obs"), sp_(1)
		{
			sp_.start();
			sub_ =
					nh_.subscribe<geometry_msgs::PoseArray>(sub_topic, 1, boost::bind(&xsens_obs::xsensCallback, this, _1));
			pub_ = nh_.advertise<exotica::MeshVertexArray>("/xsens_mesh_obs", 1);
			marker_pub_ = nh_.advertise<visualization_msgs::Marker>("/xsens_mesh_obs_marker", 1);
			exts_.vertices.resize(2);
			for (int i = 0; i < 2; i++)
			{
				exts_.vertices[i].name = "XsensObs" + std::to_string(i);
				exts_.vertices[i].type = exotica::MeshVertex::OBSTACLE_TO_ALL;
				exts_.vertices[i].toLinks.clear();
				exts_.vertices[i].radius = 0.07;
				exts_.vertices[i].w = 2;
			}
			index_=
			{	10,14};

			marker_.type = marker_.SPHERE_LIST;
			marker_.header.frame_id = "/base";
			marker_.scale.x = marker_.scale.y = marker_.scale.z = 0.05;
			std_msgs::ColorRGBA c;
			c.r = c.g = c.b = 0.5;
			c.a = 1;
			marker_.color = c;
			calibration();
		}

		~xsens_obs()
		{

		}

		//	Calibration
		bool calibration()
		{
//			ROS_INFO("XSENS-LWR Calibration");
//			ROS_INFO("Press any key to start calibration");
//			getchar();
//			ready_for_calibration_ = false;
//			ros::Duration(2).sleep();
//			if (!ready_for_calibration_)
//			{
//				ROS_INFO("No data received, calibration failed");
//				return false;
//			}
//			KDL::Frame corner(KDL::Rotation::Identity(), KDL::Vector(0.35, 0.38, 0.02));
//			offset_ = corner * hand_in_foot_.Inverse();
//			ROS_INFO("Calibration Succeeded");

			//	Right
			offset_ = KDL::Frame(KDL::Rotation::Identity(), KDL::Vector(0.2, 1.1, -0.83));
			//	Left
			//offset_ = KDL::Frame(KDL::Rotation::Identity(), KDL::Vector(0.2, 1.28, -0.93));
			return true;
		}
	private:
		void xsensCallback(const geometry_msgs::PoseArrayConstPtr & poses)
		{
//			Index
//			| Right Shoulder  | 7                         |
//			| Right Upper Arm | 8                         |
//			| Right Forearm   | 9                         |
//			| Right Hand      | 10                        |
//			| Left Shoulder   | 11                        |
//			| Left Upper Arm  | 12                        |
//			| Left Forearm    | 13                        |
//			| Left Hand       | 14                        |
			if (poses->poses.size() < 22)
				ROS_ERROR_STREAM("XSENS size incorrect " << poses->poses.size());

			marker_.points.resize(poses->poses.size());

			tf::PoseMsgToKDL(poses->poses[21], foot);

			for (int i = 0; i < poses->poses.size(); i++)
			{

				KDL::Frame tmp;
				tf::PoseMsgToKDL(poses->poses[i], tmp);

				tmp = offset_ * (tmp * foot.Inverse());
				tf::pointKDLToMsg(tmp.p, marker_.points[i]);

				//	Right hand
				if (i == 10)
				{
					exts_.vertices[0].position = marker_.points[i];
				}
				//	Left hand
				if (i == 14)
				{
					exts_.vertices[1].position = marker_.points[i];
				}
			}

			pub_.publish(exts_);
			marker_pub_.publish(marker_);

		}
		ros::Timer frame_timer;
		KDL::Frame offset_;
		ros::NodeHandle nh_;
		ros::Subscriber sub_;
		ros::Publisher pub_;
		ros::Publisher marker_pub_;
		visualization_msgs::Marker marker_;
		exotica::MeshVertexArray exts_;
		std::vector<int> index_;
		ros::AsyncSpinner sp_;
		KDL::Frame foot;
};

int main(int argc, char** argv)
{
	ros::init(argc, argv, "xsens_obstacle");

	xsens_obs xsens("/mvn_pose", "/xsens_obs");
	ros::waitForShutdown();
	return 0;
}
