/*
 * dummy_goal.cpp
 *
 *  Created on: 23 Jul 2014
 *      Author: yiming
 */

#include <ros/ros.h>

#include <interactive_markers/interactive_marker_server.h>
#include <geometry_msgs/PoseArray.h>
#include <boost/thread/mutex.hpp>
#include <exotica/MeshVertex.h>
geometry_msgs::PoseArray goal_pose_;
boost::mutex::scoped_lock lock_;
bool update;
void goalFeedback(const visualization_msgs::InteractiveMarkerFeedbackConstPtr &feedback)
{
	boost::mutex::scoped_lock(lock_);
	goal_pose_.poses[0].position = feedback->pose.position;
	update = true;
}

int main(int argc, char** argv)
{
	ros::init(argc, argv, "dummy_goal");
	ros::NodeHandle nh;
	ros::Publisher pub = nh.advertise<geometry_msgs::PoseArray>("/dummy_goal/goal", 1);
	ros::AsyncSpinner sp(1);
	interactive_markers::InteractiveMarkerServer server("dummy_goal_server");
	visualization_msgs::InteractiveMarker int_marker;
	visualization_msgs::Marker marker;
	marker.type = visualization_msgs::Marker::SPHERE;
	marker.scale.x = 0.15;
	marker.scale.y = 0.15;
	marker.scale.z = 0.15;
	marker.color.r = 1;
	marker.color.g = 0;
	marker.color.b = 0;
	marker.color.a = 0.4;


	goal_pose_.header.frame_id = "/base";
	goal_pose_.poses.resize(1);
	visualization_msgs::InteractiveMarkerControl obs_control;
	obs_control.always_visible = true;
	obs_control.markers.push_back(marker);
	int_marker.header.frame_id = "/base";
	int_marker.name = "Goal";
	int_marker.pose.position.x = 0.6;
	int_marker.pose.position.y = 0;
	int_marker.pose.position.z = 0.15;
	goal_pose_.poses[0]=int_marker.pose;
	int_marker.controls.push_back(obs_control);
	int_marker.controls[0].interaction_mode = visualization_msgs::InteractiveMarkerControl::MOVE_3D;
	server.insert(int_marker);
	server.setCallback(int_marker.name, &goalFeedback);
	server.applyChanges();

	ros::Duration d(0.005);
	sp.start();
	std::cout << "Press to start goal publishing" << std::endl;
	getchar();
	while (ros::ok())
	{
		pub.publish(goal_pose_);
		ros::spinOnce();
		d.sleep();
	}

	ros::waitForShutdown();
	return 0;
}
