/*
 * welding_vision_goal.cpp
 *
 *  Created on: 21 Jan 2015
 *      Author: yiming
 */

#include <ros/ros.h>

#include <interactive_markers/interactive_marker_server.h>
#include <geometry_msgs/PoseArray.h>
#include <boost/thread/mutex.hpp>
#include <exotica/MeshVertex.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
geometry_msgs::PoseArray goal_pose_;
geometry_msgs::Point base_pose_;
boost::mutex::scoped_lock lock_;
bool update;

int cnt = 0;
double increase = true;
void timeCallback(const ros::TimerEvent&)
{
	static tf::TransformBroadcaster br;

	static tf::TransformListener listener;
	tf::StampedTransform target;
	try
	{
		listener.lookupTransform("/kuka_base", "/cookie_crisp", ros::Time(0), target);
	}
	catch (tf::TransformException ex)
	{
		ROS_ERROR("%s", ex.what());
		ros::Duration(.001).sleep();
	}
	tf::pointTFToMsg(target.getOrigin(), base_pose_);
	base_pose_.z += 0.1;
}

int main(int argc, char** argv)
{
	ros::init(argc, argv, "welding_goal");
	ros::NodeHandle nh;
	ros::Publisher pub = nh.advertise<geometry_msgs::PoseArray>("/welding_goal/goal", 1);
	ros::Publisher marker_pub1 =
			nh.advertise<visualization_msgs::Marker>("/welding_goal/marker1", 1);
	ros::Publisher marker_pub2 =
			nh.advertise<visualization_msgs::Marker>("/welding_goal/marker2", 1);
	ros::AsyncSpinner sp(1);
	ros::Timer frame_timer = nh.createTimer(ros::Duration(0.01), timeCallback);

	visualization_msgs::Marker base_marker;
	base_marker.header.frame_id = "/base";
	base_marker.type = visualization_msgs::Marker::CUBE;
	base_marker.scale.x = 0.1914;
	base_marker.scale.y = 0.2723;
	base_marker.scale.z = 0.06;
	base_marker.color.r = 0.6;
	base_marker.color.g = 0.6;
	base_marker.color.b = 0;
	base_marker.color.a = 0.6;

	visualization_msgs::Marker top_marker;
	top_marker.header.frame_id = "/base";
	top_marker.type = visualization_msgs::Marker::LINE_STRIP;
	top_marker.scale.x = 0.01;
	top_marker.color.r = 1;
	top_marker.color.g = 0.6;
	top_marker.color.b = 0;
	top_marker.color.a = 1;
	top_marker.pose.orientation.z = 1;
	top_marker.points.resize(3);
	std::vector<geometry_msgs::Point> welding_offset(3);

	welding_offset[0].x = -0.0957;
	welding_offset[0].y = -0.1361;
	welding_offset[0].z = 0.032;
	welding_offset[1].x = 0.0957;
	welding_offset[1].y = 0.0;
	welding_offset[1].z = 0.032;
	welding_offset[2].x = -0.0957;
	welding_offset[2].y = 0.1361;
	welding_offset[2].z = 0.032;

	goal_pose_.header.frame_id = "/base";
	goal_pose_.poses.resize(1);

	std::vector<double> goal_y, goal_x;
	int steps = 5000;
	goal_y.resize(steps);
	goal_x.resize(steps);
	double delta_x = 0.1914 / (steps / 2), delta_y = 0.1361 / (steps / 2);
	for (int i = 0; i < steps / 2; ++i)
	{
		goal_x[i] = 0.0957 - i * delta_x;
		goal_y[i] = i * delta_y - 0.1361;
	}
	for (int i = 0; i < steps / 2; ++i)
	{
		goal_x[i + steps / 2] = -0.0957 + i * delta_x;
		goal_y[i + steps / 2] = i * delta_y;
	}

	ros::Duration d(0.001);
	sp.start();
	std::cout << "Press to start goal publishing" << std::endl;
	getchar();
	bool isOK = true;
	bool first = true;
	while (ros::ok())
	{
		nh.getParam("/WeldingGoal/isOK", isOK);

		base_marker.pose.position = base_pose_;
		base_marker.pose.position.z -= 0.033;
		for (int i = 0; i < 3; i++)
		{
			top_marker.points[i].x = base_pose_.x + welding_offset[i].x;
			top_marker.points[i].y = base_pose_.y + welding_offset[i].y;
			top_marker.points[i].z = base_pose_.z + welding_offset[i].z;
		}

		goal_pose_.poses[0].position = base_pose_;
		if (increase)
		{
			goal_pose_.poses[0].position.x += goal_x[cnt];
			goal_pose_.poses[0].position.y += goal_y[cnt];
			if (isOK)
			{
				cnt++;
				if (cnt > steps - 1)
				{
					increase = false;
					cnt = steps - 1;
				}
			}
		}
		else
		{
			goal_pose_.poses[0].position.x += goal_x[cnt];
			goal_pose_.poses[0].position.y += goal_y[cnt];
			if (isOK)
			{
				cnt--;
				if (cnt < 0)
				{
					increase = true;
					cnt = 0;
				}
			}
		}
		goal_pose_.poses[0].position = base_pose_;
		goal_pose_.poses[0].position.x += goal_x[0];
		goal_pose_.poses[0].position.y += goal_y[0];
		goal_pose_.poses[0].position.z += 0.02;
		//std::cout << "Goal x=" << goal_pose_.poses[0].position.x << " y=" << goal_pose_.poses[0].position.y << " z=" << goal_pose_.poses[0].position.z << std::endl;
		marker_pub1.publish(base_marker);
		marker_pub2.publish(top_marker);
		pub.publish(goal_pose_);
		ros::spinOnce();
		d.sleep();
	}

	std::cout << "Goal publishing wait for shutdown" << std::endl;
	ros::waitForShutdown();
	return 0;
}

