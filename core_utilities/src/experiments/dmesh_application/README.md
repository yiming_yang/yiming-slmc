## dmesh_application
-----------------

## This package contains two applications using relative distance space representation:
1. LWR robot mock-up welding.
2. Baxter water pouring.

#################################################################
############### To run the LWR welding experiment ###############

1. Configure you ROS network, ROS_IP=your local IP, and ROS_MASTER_URI=http://192.168.0.2:11311
2. Launch the driver on Wendy, and vision tracking system on Rachel.

## Launch MoveIt with the laser pen model
3. roslaunch lwr_pen_moveit demo.launch

## Launch the dmesh adaptation planner
4. roslaunch dmesh_application lwr_welding.launch

## Launch the vision goal tracking node
5. roslaunch dmesh_application vision_goal.launch

## If you want the robot to interact with human with XSENS suit
a. Configure the netwrok
   - In XSENS software, enable streaming to this local host.
   - In ros package xsens_mvn_driver/src/xsens_node.py, change the UDP_IP = [(s.connect(('XXX.XXX.XXX.XXX', 80)) to the XSENS Windows machine IP.
   - rosrun xsens_mvn_driver xsens_node.
   - Before you do step 4, in lwr_welding launch file, set "/MeshGraphManager/ExternalTopic" to "/xsens_obs/xsens_mesh_obs".
   - Do step 4.
   - rosrun dmesh_application xsens_obs


#################################################################
########### To run the Baxter water pouring experiment ##########

1. Configure you ROS network, ROS_IP=your local IP, and ROS_MASTER_URI=http://192.168.106.5:11311
2. Launch the controller on Baxter machine.

## Launch MoveIt with the bottle-cup model
3. roslaunch baxter demo.launch

## Launch the dmesh adaptation planner
4. roslaunch dmesh_application baxter_fill.launch

## If you want the robot to interact with human with XSENS suit
Same as in LWR experiment

#################################################################

## Contact
## Yiming Yang
## yiming.yang@ed.ac.uk
