#ifndef EXOTICA_HPP_GENERAL
#define EXOTICA_HPP_GENERAL

//!< Core Library
#include <exotica/Tools.h>
#include <exotica/Object.h>
#include <exotica/Factory.h>
#include <exotica/Test.h>
#include <exotica/TaskDefinition.h>
#include <exotica/TaskMap.h>
#include <exotica/PlanningProblem.h>
#include <exotica/MotionSolver.h>
#include <exotica/Server.h>
//!< The XML Parser
#include <tinyxml2/tinyxml2.h>


#endif
