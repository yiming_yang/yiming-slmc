\contentsline {section}{\numberline {1}Introduction}{3}{section.1}
\contentsline {subsection}{\numberline {1.1}Document Structure}{3}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}The EXOTica Library}{3}{subsection.1.2}
\contentsline {section}{\numberline {2}Setup}{5}{section.2}
\contentsline {subsection}{\numberline {2.1}Package Organisation}{5}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Pre-requisites}{5}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Installation}{5}{subsection.2.3}
\contentsline {section}{\numberline {3}Introductory Optimisation Theory}{6}{section.3}
\contentsline {subsection}{\numberline {3.1}Notation}{6}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Components}{7}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Cost Functions}{8}{subsection.3.3}
\contentsline {section}{\numberline {4}Solving Optimisation Problems}{10}{section.4}
\contentsline {subsection}{\numberline {4.1}Architectural Overview}{10}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Boiler-Plate}{11}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Initialisation}{11}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}Solving the Optimisation Problem}{14}{subsection.4.4}
\contentsline {section}{\numberline {5}Library Implementations}{15}{section.5}
\contentsline {subsection}{\numberline {5.1}Task Definitions}{15}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Velocity Solvers}{16}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}Position Solvers}{17}{subsection.5.3}
\contentsline {section}{\numberline {6}Extending the Library}{18}{section.6}
\contentsline {subsection}{\numberline {6.1}Creating Task-Definition Classes}{18}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}Creating Position Solvers}{19}{subsection.6.2}
\contentsline {subsection}{\numberline {6.3}Creating Velocity Solvers}{20}{subsection.6.3}
\contentsline {section}{\numberline {7}Detailed API}{23}{section.7}
\contentsline {section}{\numberline {A}Limitations}{24}{appendix.A}
\contentsline {section}{\numberline {B}Version History}{24}{appendix.B}
\contentsline {section}{\numberline {C}Listings}{27}{appendix.C}
