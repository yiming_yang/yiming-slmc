#!/usr/bin/python
import socket
import struct
import numpy as np

import rospy
from geometry_msgs.msg import Pose, PoseArray, Quaternion, Point

# Debug mode -- prints information onto the console
DEBUG = False
# Subtract Pelvis -- i.e. make relative w.r.t. pelvis
SUB_PELVIS = False
# Parse advanced information, currently not broadcast
PARSE_ALL = False

# Init Socket connection to nikita's MVN studio
UDP_IP = [(s.connect(('192.168.106.199', 80)), s.getsockname()[0], s.close()) for s in [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1]
UDP_PORT = 9763 # standard port; config in MVN Studio > Options > Preferences > Network Stream

def initUDPStream():
  global sock
  global pubPoseArray
  global posArr
  global r
  sock = socket.socket(socket.AF_INET,
  										 socket.SOCK_DGRAM)
  sock.bind((UDP_IP, UDP_PORT))

  # Init ROS node
  pubPoseArray = rospy.Publisher('mvn_pose', PoseArray)
  rospy.init_node('xsens_mvn')

  r = rospy.Rate(100) # or 120 -- need to find that out TODO
  posArr = PoseArray()

def UDPParser():
  global sock
  while True and not rospy.is_shutdown():
    data, addr = sock.recvfrom(1500)
    
    messagetype = data[4:6] # we want 02 for Quaternions, 01 for Euler
    timecode = data[12:16]

    if PARSE_ALL:
      idstring = data[:6]
      samplecounter = data[6:10]
      datagramcounter = data[10]
      numberofitems = data[11]
      avatarId = data[16]
      reservedHeaderWoContent = data[17:24]

    if messagetype == '01':
      if DEBUG:
        print "Euler mode not supported"

    elif messagetype == '02':
      if DEBUG:
        print "Quaternion received"
      
      # 23 segments with each 32bytes 
      for segment in range(0, 22):
        i = segment*32 + 24
        j = segment*32 + 28
              
        # 4 bytes segmentId
        if PARSE_ALL:
          segmentId = reduce(lambda rst, d: rst * 10 + d, struct.unpack('4B', data[i:j]))
        
        # 4 bytes x-coordinate of segment position
        posX = np.fromstring(data[(i+4):(j+4)], dtype=">f") # float32 in meters
        # 4 bytes y-coordinate of segment position
        posY = np.fromstring(data[(i+8):(j+8)], dtype=">f")
        # 4 bytes z-coordinate of segment position
        posZ = np.fromstring(data[(i+12):(j+12)], dtype=">f")
        
        # 4 bytes q1 rotation - segment rotation quaternion component 1 (re)
        q1 = np.fromstring(data[(i+16):(j+16)], dtype=">f")
        # 4 bytes q2 rotation - segment rotation quaternion component 1 (i)
        q2 = np.fromstring(data[(i+20):(j+20)], dtype=">f")
        # 4 bytes q3 rotation - segment rotation quaternion component 1 (j)
        q3 = np.fromstring(data[(i+24):(j+24)], dtype=">f")
        # 4 bytes q4 rotation - segment rotation quaternion component 1 (k)
        q4 = np.fromstring(data[(i+28):(j+28)], dtype=">f")

        if DEBUG:
          print "S#", segmentId, "X", posX, "Y", posY, "Z", posZ, "q1", q1, "q2", q2, "q3", q3, "q4", q4
        
        # Substract pelvis from everything...
        if SUB_PELVIS:
          pelvisX = np.fromstring(data[(28):(32)], dtype=">f")
          pelvisY = np.fromstring(data[(32):(36)], dtype=">f")
          pelvisZ = np.fromstring(data[(36):(40)], dtype=">f")
          posX = posX - pelvisX
          posY = posY - pelvisY
          posZ = posZ - pelvisZ
        
        if segment == 0: # Re-init posArr
          posArr = PoseArray()
        
        point = Point(posX, posY, posZ)
        quat = Quaternion(q2, q3, q4, q1)
        posArr.poses.append(Pose(point, quat))
      
      
      posArr.header.frame_id = "base"
      posArr.header.stamp = rospy.Time.now() # TODO: try timecode ...
      pubPoseArray.publish(posArr)
      r.sleep()   
        
    else:
      if DEBUG:
        print "OTHER -- please implement", messagetype # 12 is avatar information


if __name__ == '__main__':
  initUDPStream()
  UDPParser()
  
