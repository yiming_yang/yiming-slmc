#uoe_xsens_mvn_publisher
The ROS node parses data sent by the Xsens MVN Studio and publishes it as a `PoseArray[]` to a ROS topic.

###Get Started
This version of the `xsens_mvn_driver` ROS node auto-configures the IP address itself with the port defaulting to `9763`.

If the IP address used is _not_ on the first ethernet device and/or the port configured in MVN Studio does not correspond to 9763, please adjust the corresponding variables at the top of `scripts/xsens_node.py`.

The node is started by executing `rosrun xsens_mvn_driver xsens_node.py`. The node will publish a `PoseArray[]` to the topic `/mvn_pose`. 

###Debug
Open rviz with `rosrun rviz rviz &` and add the topic `/mvn_pose` to the view to see the data in real-time.
Alternatively, the Debug flag can be set to True to write information to the console.

###Coordinate Systems
The module supports sending the raw, unmodified data (in the world coordinate system) as streamed by MVN Studio when SUB_PELVIS is deactivated. With `SUB_PELVIS = True`, the sent data is rooted.

###Correspondence
The following table gives an overview of the correspondence on the IDs of the returned PoseArray[] to the trackers in Xsens:

| Limb/Tracker    | Corresponding Array Index |
|-----------------|---------------------------|
| Pelvis          | 0                         |
| L5              | 1                         |
| L3              | 2                         |
| T12             | 3                         |
| T8              | 4                         |
| Neck            | 5                         |
| Head            | 6                         |
| Right Shoulder  | 7                         |
| Right Upper Arm | 8                         |
| Right Forearm   | 9                         |
| Right Hand      | 10                        |
| Left Shoulder   | 11                        |
| Left Upper Arm  | 12                        |
| Left Forearm    | 13                        |
| Left Hand       | 14                        |
| Right Upper Leg | 15                        |
| Right Lower Leg | 16                        |
| Right Foot      | 17                        |
| Right Toe       | 18                        |
| Left Upper Leg  | 19                        |
| Left Lower Leg  | 20                        |
| Left Foot       | 21                        |
| Left Toe        | 22                        |
| Prop1           | 23                        |
| Prop2           | 24                        |
| Prop3           | 25                        |
| Prop4           | 26                        |


###Dependencies
The package needs `numpy` to be installed.