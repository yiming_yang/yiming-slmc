#ifndef NODE_H
#define NODE_H

#include "aico/AICOsolver.h"
#include <ros/ros.h>
#include <ros/package.h>
#include <kinematic_maps/EffPosition.h>
#include <exotica_collision.h>
#include <exotica/Initialiser.h>
#include <sensor_msgs/JointState.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/PoseArray.h>
#include <eigen_conversions/eigen_msg.h>
#include <data_replay/GenerateGoals.h>
#include <tf/transform_broadcaster.h>
#include <tf_conversions/tf_kdl.h>
#include <geometric_shapes/shapes.h>
#include <geometric_shapes/mesh_operations.h>
#include <geometric_shapes/shape_messages.h>
#include <geometric_shapes/shape_operations.h>

class Hrp2WallNode
{
public:
    Hrp2WallNode();
    bool reinitProblem(std::vector<Eigen::VectorXd> & q_init);
private:
    ros::NodeHandle nh_;
    ros::NodeHandle nhg_;
    std::string resource_path_;
    ros::Publisher jointStatePublisher_;
    ros::Publisher posePublisher_;
    ros::ServiceClient goalGenerator_;
    tf::TransformBroadcaster broadcaster_;
    exotica::AICOProblem_ptr prob;
    data_replay::GenerateGoals srv;
    void collCallback(exotica::CollisionAvoidance* ptr, int t);
    void setupCollisionScene(planning_scene::PlanningScenePtr pscene);
};

#endif // NODE_H
