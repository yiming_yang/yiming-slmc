#include "wall_stepping/node.h"

using namespace exotica;

Hrp2WallNode::Hrp2WallNode() : nh_("~"), nhg_()
{
    jointStatePublisher_ = nhg_.advertise<sensor_msgs::JointState>("/joint_states", 1);
    posePublisher_ = nhg_.advertise<geometry_msgs::PoseArray>("/goals", 1);
    goalGenerator_ = nhg_.serviceClient<data_replay::GenerateGoals>("goal_generator");
    srv.request.timesteps_com = 15;
    srv.request.timesteps_footlift = 5;
    srv.request.timesteps_mid = 5;
    srv.request.starting_foot = 1;
    srv.request.initialpose.resize(40,0.0);
    srv.request.foot_lift = 0.01;
    srv.request.foot_midarch = 0.03;
    srv.request.com_height = 0.35;
    srv.request.dir_up = 0.2;
    srv.request.dir_front = 0.2;
    geometry_msgs::PoseArray pmsg;
    pmsg.poses.resize(5);
    if (!goalGenerator_.call(srv))
    {
      ROS_ERROR("Failed to call goal generator service.");
    }
    else
    {

        // TODO Auto-generated constructor stub
        resource_path_ = ros::package::getPath("wall_stepping").append("/../resources/");
        ROS_INFO_STREAM("Loaded path: " << resource_path_);

        {
            // Declarations
            Initialiser ini;
            MotionSolver_ptr tmp_sol;
            Server_ptr ser;
            PlanningProblem_ptr tmp_prob;

            // List problems and
            std::vector<std::string> impl;
            MotionSolver_fac::Instance().listImplementations(impl);
            for(int i=0;i<impl.size();i++) ROS_INFO_STREAM("Solver: '"<<impl[i]<<"'");

            std::string problem_name, solver_name;
            nh_.getParam("problem", problem_name);
            nh_.getParam("solver", solver_name);

            // Initialise and solve
            if(ok(ini.initialise(resource_path_ + std::string("hrp2.xml"), ser, tmp_sol, tmp_prob,problem_name,solver_name)))
            {
                exotica::AICOsolver_ptr sol=boost::static_pointer_cast<exotica::AICOsolver>(tmp_sol);
                prob = boost::static_pointer_cast<exotica::AICOProblem>(tmp_prob);
                std::vector<Eigen::VectorXd> q_init;
                reinitProblem(q_init);

                kinematica::KinematicScene_ptr collision_scene = prob->k_scenes_["CollisionScene"];
                planning_scene::PlanningScenePtr pscene=collision_scene->getPlanningScene();
                boost::shared_ptr<exotica::CollisionAvoidance> collmap = boost::static_pointer_cast<exotica::CollisionAvoidance>(prob->getTaskMaps()["Collision"]);
                collmap->setPreUpdateClaaback(boost::bind( &Hrp2WallNode::collCallback, this, _1 , _2));
                setupCollisionScene(pscene);

                if(!ok(sol->specifyProblem(tmp_prob))) {INDICATE_FAILURE; return;}
                Eigen::MatrixXd solution;
                ROS_INFO_STREAM("Calling solve()");
                {
                    ros::WallTime start_time = ros::WallTime::now();
                    if(ok(sol->Solve(q_init,solution)))
                    {
                        double time=ros::Duration((ros::WallTime::now() - start_time).toSec()).toSec();
                        //ROS_INFO_STREAM("Finished solving\nSolution:\n"<<solution);
                        ROS_INFO_STREAM("Finished solving ("<<time<<"s)");
                        sol->saveCosts(resource_path_ + std::string("costs.txt"));

                        sensor_msgs::JointState jnt;
                        jnt.name={"CHEST_JOINT0",
                                  "CHEST_JOINT1",
                                  "RLEG_JOINT5",
                                  "LLEG_JOINT4",
                                  "LLEG_JOINT5",
                                  "LLEG_JOINT0",
                                  "LLEG_JOINT1",
                                  "LLEG_JOINT2",
                                  "LLEG_JOINT3",
                                  "LHAND_JOINT1",
                                  "LARM_JOINT6",
                                  "RARM_JOINT5",
                                  "LARM_JOINT4",
                                  "LARM_JOINT5",
                                  "LARM_JOINT2",
                                  "LARM_JOINT3",
                                  "LARM_JOINT0",
                                  "LARM_JOINT1",
                                  "LHAND_JOINT0",
                                  "RARM_JOINT4",
                                  "RLEG_JOINT2",
                                  "RLEG_JOINT3",
                                  "HEAD_JOINT0",
                                  "HEAD_JOINT1",
                                  "RLEG_JOINT4",
                                  "LHAND_JOINT2",
                                  "RHAND_JOINT1",
                                  "RARM_JOINT6",
                                  "LHAND_JOINT3",
                                  "RARM_JOINT0",
                                  "RLEG_JOINT0",
                                  "RARM_JOINT1",
                                  "RHAND_JOINT4",
                                  "LHAND_JOINT4",
                                  "RLEG_JOINT1",
                                  "RHAND_JOINT0",
                                  "RARM_JOINT2",
                                  "RHAND_JOINT2",
                                  "RHAND_JOINT3",
                                  "RARM_JOINT3"};
                        jnt.position.resize(solution.cols());
                        ros::Rate loop_rate(1.0/prob->getTau());
                        int i=0;
                        boost::shared_ptr<exotica::TaskSqrError> ltask = boost::static_pointer_cast<exotica::TaskSqrError>(prob->getTaskDefinitions()["LeftFootPosition"]);
                        boost::shared_ptr<exotica::TaskSqrError> rtask = boost::static_pointer_cast<exotica::TaskSqrError>(prob->getTaskDefinitions()["RightFootPosition"]);
                        double lrho,rrho;
                        Eigen::VectorXd tmp,tmp1;
                        tmp.resize(18);
                        double tt=2.0;
                        tf::Transform transform;
                        kinematica::KinematicScene_ptr lscene = prob->k_scenes_["LeftFootScene"];
                        kinematica::KinematicScene_ptr rscene = prob->k_scenes_["RightFootScene"];
                        std::vector<std::string> poses_names = {"base_link"};
                        std::vector<KDL::Frame> lposes(1);
                        std::vector<KDL::Frame> rposes(1);
                        KDL::Frame foot_frame;
                        while(ros::ok())
                        {
                            jnt.header.stamp=ros::Time::now();
                            jnt.header.seq++;
                            for(int j=0;j<solution.cols();j++)
                                jnt.position[j]=solution(i,j);

                            pmsg.header.stamp=jnt.header.stamp;
                            pmsg.header.seq++;
                            ltask->getRho(lrho,i);
                            rtask->getRho(rrho,i);
                            if(lrho!=0.0)
                            {
                                pmsg.header.frame_id = "l_sole";
                                ltask->getGoal(tmp,i);
                                for(int j=0;j<6;j++)
                                {
                                    pmsg.poses[j].position.x=tmp(j*3+0);
                                    pmsg.poses[j].position.y=tmp(j*3+1);
                                    pmsg.poses[j].position.z=tmp(j*3+2);
                                }
                            }
                            if(rrho!=0.0)
                            {
                                pmsg.header.frame_id = "r_sole";
                                rtask->getGoal(tmp,i);
                                for(int j=0;j<6;j++)
                                {
                                    pmsg.poses[j].position.x=tmp(j*3+0);
                                    pmsg.poses[j].position.y=tmp(j*3+1);
                                    pmsg.poses[j].position.z=tmp(j*3+2);
                                }
                            }
                            tmp1=solution.row(i);
                            prob->update(tmp1,i);
                            lscene->getPoses(poses_names,lposes);
                            rscene->getPoses(poses_names,rposes);
                            foot_frame = KDL::Frame(KDL::Rotation::Quaternion(srv.response.support_foot_transform[i].orientation.x,srv.response.support_foot_transform[i].orientation.y,srv.response.support_foot_transform[i].orientation.z,srv.response.support_foot_transform[i].orientation.w),
                                                    KDL::Vector(srv.response.support_foot_transform[i].position.x, srv.response.support_foot_transform[i].position.y, srv.response.support_foot_transform[i].position.z));
                            lposes[0]=foot_frame*lposes[0];
                            rposes[0]=foot_frame*rposes[0];
                            tf::TransformKDLToTF(lrho!=0.0?lposes[0]:rposes[0],transform);

                            broadcaster_.sendTransform(tf::StampedTransform(transform, jnt.header.stamp, "wall/base_link", "base_link"));
                            jointStatePublisher_.publish(jnt);
                            posePublisher_.publish(pmsg);

                            if(i==0 && tt>0)
                            {
                                tt-=prob->getTau();
                            }
                            else
                            {
                                tt=2.0;
                                i++;
                                if(i>=solution.rows()) i=0;
                            }
                            ros::spinOnce();
                            loop_rate.sleep();

                        }
                    }
                    else
                    {
                        ROS_INFO_STREAM("Failed to find solution");
                    }
                }
            }
        }
    }
}

bool Hrp2WallNode::reinitProblem(std::vector<Eigen::VectorXd> & q_init)
{
  int T=srv.response.timesteps-2;
  ROS_INFO_STREAM("Setting time steps: "<<T);
  prob->setTime(T);
  boost::shared_ptr<exotica::TaskSqrError> ltask = boost::static_pointer_cast<exotica::TaskSqrError>(prob->getTaskDefinitions()["LeftFootPosition"]);
  boost::shared_ptr<exotica::TaskSqrError> rtask = boost::static_pointer_cast<exotica::TaskSqrError>(prob->getTaskDefinitions()["RightFootPosition"]);
  Eigen::VectorXd tmp;
  tmp.resize(18);
  int N=prob->getW().rows();
  q_init.assign(T+2,Eigen::VectorXd::Zero(N));
  double lrho,rrho;
  ltask->getRho(lrho);
  rtask->getRho(rrho);

  int t;
  for(int tt=0;tt<T+2;tt++)
  {
    t=tt;
    if(t>=T) t=T-1;
    ltask->setRho(srv.response.support_foot[t]==0?lrho:0.0,t);
    tmp << srv.response.lcom_goal[t].x,
           srv.response.lcom_goal[t].y,
           srv.response.lcom_goal[t].z,
           srv.response.lcom_goal[t].x,
           srv.response.lcom_goal[t].y,
           srv.response.lcom_goal[t].z+1.0,
           srv.response.lcom_goal[t].x+0.2,
           srv.response.lcom_goal[t].y,
           srv.response.lcom_goal[t].z,
           srv.response.rfoot_goal[t].x,
           srv.response.rfoot_goal[t].y,
           srv.response.rfoot_goal[t].z,
           srv.response.rfoot_goal_up[t].x,
           srv.response.rfoot_goal_up[t].y,
           srv.response.rfoot_goal_up[t].z,
           srv.response.rfoot_goal_direction[t].x,
           srv.response.rfoot_goal_direction[t].y,
           srv.response.rfoot_goal_direction[t].z;
    ltask->setGoal(tmp,t);

    rtask->setRho(srv.response.support_foot[t]==0?0.0:rrho,t);
    tmp << srv.response.rcom_goal[t].x,
           srv.response.rcom_goal[t].y,
           srv.response.rcom_goal[t].z,
           srv.response.rcom_goal[t].x,
           srv.response.rcom_goal[t].y,
           srv.response.rcom_goal[t].z+1.0,
           srv.response.rcom_goal[t].x+0.2,
           srv.response.rcom_goal[t].y,
           srv.response.rcom_goal[t].z,
           srv.response.lfoot_goal[t].x,
           srv.response.lfoot_goal[t].y,
           srv.response.lfoot_goal[t].z,
           srv.response.lfoot_goal_up[t].x,
           srv.response.lfoot_goal_up[t].y,
           srv.response.lfoot_goal_up[t].z,
           srv.response.lfoot_goal_direction[t].x,
           srv.response.lfoot_goal_direction[t].y,
           srv.response.lfoot_goal_direction[t].z;
    rtask->setGoal(tmp,t);

    for (int i=0;i<N;i++)
    {
        q_init[tt](i)=srv.response.pose[t*N+i];
    }

  }
  return true;
}

void Hrp2WallNode::collCallback(exotica::CollisionAvoidance* ptr, int t)
{
    static std::vector<KDL::Frame> poses;
    static std::vector<std::string> poses_names = {"base_link"};
    kinematica::KinematicScene_ptr scene;

    KDL::Frame foot_frame = KDL::Frame(KDL::Rotation::Quaternion(srv.response.support_foot_transform[t].orientation.x,srv.response.support_foot_transform[t].orientation.y,srv.response.support_foot_transform[t].orientation.z,srv.response.support_foot_transform[t].orientation.w),
                            KDL::Vector(srv.response.support_foot_transform[t].position.x, srv.response.support_foot_transform[t].position.y, srv.response.support_foot_transform[t].position.z));
    if(srv.response.support_foot[t]==0)
    {
        scene = prob->k_scenes_["LeftFootScene"];
    }
    else
    {
        scene = prob->k_scenes_["RightFootScene"];
    }
    scene->getPoses(poses_names,poses);
    ptr->setObsFrame((foot_frame*poses[0]).Inverse());
}

void Hrp2WallNode::setupCollisionScene(planning_scene::PlanningScenePtr pscene)
{
    moveit_msgs::AttachedCollisionObject attached_object;
    attached_object.link_name = "wall";
    /* The header must contain a valid TF frame*/
    attached_object.object.header.frame_id = "base_link";
    /* The id of the object */
    attached_object.object.id = "wall";

    /* A default pose */
    geometry_msgs::Pose pose;
    pose.orientation.w = 1.0;

    /* Define a box to be attached */
    shapes::ShapeConstPtr m((shapes::Shape*)shapes::createMeshFromResource("package://hrp2_14_description/urdf/wall.obj"));

    shape_msgs::Mesh co_mesh;
    shapes::ShapeMsg co_mesh_msg;
    shapes::constructMsgFromShape(m.get(),co_mesh_msg);
    co_mesh = boost::get<shape_msgs::Mesh>(co_mesh_msg);
    attached_object.object.meshes.push_back(co_mesh);
    attached_object.object.mesh_poses.push_back(pose);
    attached_object.object.operation = attached_object.object.ADD;
    moveit_msgs::PlanningScene planning_scene;
    planning_scene.world.collision_objects.push_back(attached_object.object);
    planning_scene.is_diff = true;
    pscene->usePlanningSceneMsg(planning_scene);

    //EigenSTL::vector_Affine3d poses;
    //poses.push_back(Eigen::Affine3d());
    //std::vector< shapes::ShapeConstPtr > shapes;
    //shapes.push_back(m);
    //pscene->getWorldNonConst()->addToObject("wall",shapes,poses);
    pscene->printKnownObjects(std::cerr);
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "HRP2AICOwallStepping");
    ROS_INFO_STREAM("Started");
    Hrp2WallNode ex;
    ros::spin();
}
