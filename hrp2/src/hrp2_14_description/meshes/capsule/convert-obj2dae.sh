#!/bin/sh
obj_files=`find . -name '*.obj'`

for f in $obj_files; do
    dae_f=`echo $f | sed 's/.obj$/.dae/g'`
    meshlabserver -i $f -o $dae_f
    echo $dae_f
done

