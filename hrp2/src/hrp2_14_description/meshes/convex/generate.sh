#!/bin/sh
#
# This script relies on convex_decomposition ROS package to generate
# a simplified version of the HRP-2 model to be used when checking
# for collisions.
#
# It requires the following software:
# - hrp2_14_description (this package),
# - meshlab (3d viewer providing easy import/export into various 3d formats)
#   apt-get install meshlab,
# - convex_decomposition package provided by the robot_model stack.

pkg_dir=`rospack find hrp2_14_description`

convex_decomposition_dir=`rospack find convex_decomposition`
convex_decomposition_bin=\
"$convex_decomposition_dir/convex_decomposition/bin/convex_decomposition"

meshes_dir=$pkg_dir/meshes
convex_meshes_dir=$pkg_dir/meshes/convex

for body in `find $meshes_dir -name '*.dae'`; do
 body_obj=$convex_meshes_dir/`basename "$body" | sed 's|.dae|.obj|'`
 meshlabserver -i $body -o $body_obj
 $convex_decomposition_bin $body_obj
done

# Warning: the DAE files generated by convex_decomposition do not seem
# to be correct. Re-export them using meshlab for safety.

# "Remove" unwanted files.
find $convex_meshes_dir -name '*.xml' -exec rm {} \;
find $convex_meshes_dir -name '*.dae' -exec rm {} \;

# Re-export.
for body in `find $convex_meshes_dir -name '*_convex.obj'`; do
 body_dae=`echo $body | sed 's|.obj|.dae|'`
 meshlabserver -i $body -o $body_dae
done
