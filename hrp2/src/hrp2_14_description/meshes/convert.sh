#!/bin/sh
for i in `find ~pis/OpenHRP/Controller/IOserver/robot/HRP2/model -name '*.wrl'`; do
    meshlabserver -i $i -o `pwd`/$(echo `basename $i` | sed 's|.wrl$|.dae|')
done

for i in `find ~pis/OpenHRP/Controller/IOserver/robot/HRP2JRL/model -name '*.wrl'`; do
    meshlabserver -i $i -o `pwd`/$(echo `basename $i` | sed 's|.wrl$|.dae|')
done
