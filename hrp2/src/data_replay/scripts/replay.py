#!/usr/bin/env python
import rospy
import pickle
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Pose
from geometry_msgs.msg import PoseArray
import tf
from tf_conversions import posemath

if __name__ == "__main__":
  rospy.init_node('data_replay', anonymous=True)
  rospack = rospy.core.rospkg.RosPack()
  pth=rospack.get_path("data_replay")
  q = pickle.load(open(pth+"/scripts/cspace-path-values.dat","rb"))
  footl = pickle.load(open(pth+"/scripts/cspace-path-footL.dat","rb"))
  footr = pickle.load(open(pth+"/scripts/cspace-path-footR.dat","rb"))
  jmap = pickle.load(open(pth+"/scripts/cspace-path-jmap.dat","rb"))
  keys = pickle.load(open(pth+"/scripts/cspace-path-keys.dat","rb"))
  pub = rospy.Publisher('/joint_states',JointState,queue_size=10)
  pubf = rospy.Publisher('/feet',PoseArray,queue_size=10)
  br = tf.TransformBroadcaster()
  r = rospy.Rate(1)
  msg=JointState()
  i=0
  p=PoseArray()
  p.poses.append(Pose())
  p.poses.append(Pose())
  while not rospy.is_shutdown():
    msg.header.stamp = rospy.get_rostime()
    msg.header.seq=msg.header.seq+1
    msg.name=keys[i]
    msg.position=q[i]
    pub.publish(msg)
    p.header=msg.header
    p.header.frame_id='wall/base_link'
    p.poses[0].position.x=footl[i][0]
    p.poses[0].position.y=footl[i][1]
    p.poses[0].position.z=footl[i][2]
    qt = tf.transformations.quaternion_from_euler(0,0,footl[i][3])
    p.poses[0].orientation.x=qt[0]
    p.poses[0].orientation.y=qt[1]
    p.poses[0].orientation.z=qt[2]
    p.poses[0].orientation.w=qt[3]
    pp=posemath.fromMsg(p.poses[0]).Inverse()
    br.sendTransform((pp.p[0], pp.p[1], pp.p[2]), pp.M.GetQuaternion(), msg.header.stamp, "wall/base_link", "l_sole")
    p.poses[1].position.x=footr[i][0]
    p.poses[1].position.y=footr[i][1]
    p.poses[1].position.z=footr[i][2]
    qt = tf.transformations.quaternion_from_euler(0,0,footr[i][3])
    p.poses[1].orientation.x=qt[0]
    p.poses[1].orientation.y=qt[1]
    p.poses[1].orientation.z=qt[2]
    p.poses[1].orientation.w=qt[3]
    pp=posemath.fromMsg(p.poses[1]).Inverse()
    br.sendTransform((pp.p[0], pp.p[1], pp.p[2]), pp.M.GetQuaternion(), msg.header.stamp, "wall/base_link", "r_sole")

    pubf.publish(p)
    i=i+1
    if i>=len(q):
      i=0
    r.sleep()
