#!/usr/bin/env python
# -*- coding: utf-8 -*-

import rospy
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Pose
from geometry_msgs.msg import Point
from geometry_msgs.msg import PoseArray
import tf
from tf_conversions import posemath
from data_replay.srv import *

def transform(trans, point):
  ret=Pose();
  tmp=posemath.Frame()
  tmp.p.x(point.x)
  tmp.p.y(point.y)
  tmp.p.z(point.z)
  tmp=trans*tmp
  q=tmp.M.GetQuaternion()
  ret.position.x=tmp.p.x()
  ret.position.y=tmp.p.y()
  ret.position.z=tmp.p.z()
  ret.orientation.x=q[0]
  ret.orientation.y=q[1]
  ret.orientation.z=q[2]
  ret.orientation.w=q[3]
  return ret

def generate_trajectory():
  rospy.init_node('data_replay_goals', anonymous=True)
  rospy.wait_for_service('goal_generator')
  print 'Computing goals'
  try:
    gengoals = rospy.ServiceProxy('goal_generator', GenerateGoals)
    req=GenerateGoalsRequest()
    req.timesteps_com = 50
    req.timesteps_footlift = 10
    req.timesteps_mid=25
    req.starting_foot = 1
    req.initialpose=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    req.foot_lift=0.02
    req.foot_midarch=0.05
    req.com_height = 0.5
    req.dir_up=0.2
    req.dir_front=0.2
    res = gengoals.call(req)
    r = rospy.Rate(60)
    pubf = rospy.Publisher('/feet',PoseArray,queue_size=1)
    p=PoseArray()
    p.poses.append(Pose())
    p.poses.append(Pose())
    p.poses.append(Pose())
    p.poses.append(Pose())
    p.poses.append(Pose())
    p.poses.append(Pose())
    p.poses.append(Pose())
    n=res.timesteps
    print 'Playing back goals'
    i=0
    while not rospy.is_shutdown():
      tr=posemath.fromMsg(res.support_foot_transform[i])
      p.poses[0]=transform(tr,res.lfoot_goal[i])
      p.poses[1]=transform(tr,res.lfoot_goal_direction[i])
      p.poses[2]=transform(tr,res.lfoot_goal_up[i])
      p.poses[3]=transform(tr,res.rfoot_goal[i])
      p.poses[4]=transform(tr,res.rfoot_goal_direction[i])
      p.poses[5]=transform(tr,res.rfoot_goal_up[i])
      if res.support_foot[i]==0:
        p.poses[6]=transform(tr,res.lcom_goal[i])
      else:
        p.poses[6]=transform(tr,res.rcom_goal[i])
      p.header.stamp = rospy.get_rostime()
      p.header.seq=p.header.seq+1
      p.header.frame_id='map'
      pubf.publish(p)
      i=i+1
      if i>n:
        i=0
      r.sleep()
  except rospy.ServiceException, e:
    print "Service call failed: %s"%e

if __name__ == "__main__":
  generate_trajectory()
