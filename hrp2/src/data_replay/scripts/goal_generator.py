#!/usr/bin/env python
# -*- coding: utf-8 -*-

import rospy
from data_replay.srv import *
import pickle
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Pose
from geometry_msgs.msg import Point
from geometry_msgs.msg import PoseArray
import tf
from tf_conversions import posemath

def get_support(foot,t):
    ret=Pose()
    if foot==0:
      ret.position.x=footl[t][0]
      ret.position.y=footl[t][1]
      ret.position.z=footl[t][2]
      qt = tf.transformations.quaternion_from_euler(0,0,footl[t][3])
      ret.orientation.x=qt[0]
      ret.orientation.y=qt[1]
      ret.orientation.z=qt[2]
      ret.orientation.w=qt[3]
    else:
      ret.position.x=footr[t][0]
      ret.position.y=footr[t][1]
      ret.position.z=footr[t][2]
      qt = tf.transformations.quaternion_from_euler(0,0,footr[t][3])
      ret.orientation.x=qt[0]
      ret.orientation.y=qt[1]
      ret.orientation.z=qt[2]
      ret.orientation.w=qt[3]
    return ret

def Frame2Point(frm):
    ret=Point()
    ret.x=frm.p.x()
    ret.y=frm.p.y()
    ret.z=frm.p.z()
    return ret

def get_foot_goal1(frame,support,foot,t):
    ret=Pose()
    if foot==support:
        return posemath.Frame()
    else:
      if foot==0:
        ret.position.x=footl[t][0]
        ret.position.y=footl[t][1]
        ret.position.z=footl[t][2]
        qt = tf.transformations.quaternion_from_euler(0,0,footl[t][3])
        ret.orientation.x=qt[0]
        ret.orientation.y=qt[1]
        ret.orientation.z=qt[2]
        ret.orientation.w=qt[3]
      else:
        ret.position.x=footr[t][0]
        ret.position.y=footr[t][1]
        ret.position.z=footr[t][2]
        qt = tf.transformations.quaternion_from_euler(0,0,footr[t][3])
        ret.orientation.x=qt[0]
        ret.orientation.y=qt[1]
        ret.orientation.z=qt[2]
        ret.orientation.w=qt[3]
      return posemath.fromMsg(frame).Inverse()*posemath.fromMsg(ret)

def get_foot_goal(frame,support,foot,t,i,n1,n2,foot_lift,foot_midarch):
    ret=Pose()
    if foot==support:
      return posemath.Frame()
    else:
      z=0
      w=0
      if i<n1:
        ww=float(i)/float(n1-1)
        w=0
        z=ww*foot_lift
      else:
        if i<n1+n2:
          ww=float(i-n1)/float(n2-1)
          w=ww*0.5
          z=foot_lift*(1.0-ww)+foot_midarch*ww
        else:
          if i<n1+2*n2:
            ww=float(i-n1-n2)/float(n2-1)
            w=0.5+ww*0.5
            z=foot_lift*(ww)+foot_midarch*(1.0-ww)
          else:
            ww=float(i-n1-n2-n2)/float(n2-1)
            w=1.0
            z=(1.0-ww)*foot_lift

      if foot==0:
        ret.position.x=footl[max(t-1,0)][0]*(1.0-w)+footl[t][0]*w
        ret.position.y=footl[max(t-1,0)][1]*(1.0-w)+footl[t][1]*w
        ret.position.z=footl[max(t-1,0)][2]*(1.0-w)+footl[t][2]*w+z
        qt = tf.transformations.quaternion_from_euler(0,0,footl[max(t-1,0)][3]*(1.0-w)+footl[t][3]*w)
        ret.orientation.x=qt[0]
        ret.orientation.y=qt[1]
        ret.orientation.z=qt[2]
        ret.orientation.w=qt[3]
      else:
        ret.position.x=footr[max(t-1,0)][0]*(1.0-w)+footr[t][0]*w
        ret.position.y=footr[max(t-1,0)][1]*(1.0-w)+footr[t][1]*w
        ret.position.z=footr[max(t-1,0)][2]*(1.0-w)+footr[t][2]*w+z
        qt = tf.transformations.quaternion_from_euler(0,0,footr[max(t-1,0)][3]*(1.0-w)+footr[t][3]*w)
        ret.orientation.x=qt[0]
        ret.orientation.y=qt[1]
        ret.orientation.z=qt[2]
        ret.orientation.w=qt[3]
      return posemath.fromMsg(frame).Inverse()*posemath.fromMsg(ret)

def get_com_goal(support,i,n,lpos,rpos,rel):
  ret=Pose()
  ret.orientation.w=1.0
  w=float(i)/float(n-1)
  if support==0:
    ret.position.x=lpos.p.x()*(1.0-w)+rpos.p.x()*w
    ret.position.y=lpos.p.y()*(1.0-w)+rpos.p.y()*w
    ret.position.z=lpos.p.z()*(1.0-w)+rpos.p.z()*w
  else:
    ret.position.x=rpos.p.x()*(1.0-w)+lpos.p.x()*w
    ret.position.y=rpos.p.y()*(1.0-w)+lpos.p.y()*w
    ret.position.z=rpos.p.z()*(1.0-w)+lpos.p.z()*w
  return posemath.fromMsg(ret)*rel

def get_com_goal_static(support,lpos,rpos,rel):
  if support==0:
    return lpos*rel
  else:
    return rpos*rel



def handle_generate_goals(req):
    res=GenerateGoalsResponse()
    N=len(q)
    #N=2
    NN=len(q[0])
    T=(2*N-1)*(req.timesteps_com+2*(req.timesteps_footlift+req.timesteps_mid))
    res.timesteps=T
    id=0
    support=req.starting_foot
    relup=posemath.Frame()
    relup.p.z(req.dir_up)
    relfwd=posemath.Frame()
    relfwd.p.x(req.dir_front)
    relcom=posemath.Frame()
    relcom.p.z(req.com_height)
    res.pose=[0]*NN*T
    for t in range(0,2*N-1):
      stp=t/2
      side=(t%2)
      support=(req.starting_foot+side)%2
      support_frame=get_support(support,max(stp-1,0))
      for i in range(0,req.timesteps_com):
        if req.starting_foot==0:
          lfoot=get_foot_goal1(support_frame,support,0,max(stp+side-1,0))
          rfoot=get_foot_goal1(support_frame,support,1,max(stp-1,0))
        else:
          lfoot=get_foot_goal1(support_frame,support,0,max(stp-1,0))
          rfoot=get_foot_goal1(support_frame,support,1,max(stp+side-1,0))
        lcom=get_com_goal(support,i,req.timesteps_com,lfoot,rfoot,relcom)
        rcom=get_com_goal(support,i,req.timesteps_com,lfoot,rfoot,relcom)
        res.support_foot.append(support)
        res.support_foot_transform.append(support_frame)
        res.lfoot_goal.append(Frame2Point(lfoot));
        res.lfoot_goal_direction.append(Frame2Point(lfoot*relfwd));
        res.lfoot_goal_up.append(Frame2Point(lfoot*relup));        
        res.rfoot_goal.append(Frame2Point(rfoot));
        res.rfoot_goal_direction.append(Frame2Point(rfoot*relfwd));
        res.rfoot_goal_up.append(Frame2Point(rfoot*relup));
        res.lcom_goal.append(Frame2Point(lcom))
        res.rcom_goal.append(Frame2Point(rcom))
        for j in range(0,NN):
            res.pose[id*NN+j]=q[0][j]
        id+=1

      support=(req.starting_foot+side+1)%2
      support_frame=get_support(support,max(stp+side-1,0))
      for i in range(0,2*(req.timesteps_footlift+req.timesteps_mid)):
        lfoot=get_foot_goal(support_frame,support,0,stp,i,req.timesteps_footlift,req.timesteps_mid,req.foot_lift,req.foot_midarch)
        rfoot=get_foot_goal(support_frame,support,1,stp,i,req.timesteps_footlift,req.timesteps_mid,req.foot_lift,req.foot_midarch)
        lcom=get_com_goal_static(support,lfoot,rfoot,relcom)
        rcom=get_com_goal_static(support,lfoot,rfoot,relcom)
        res.support_foot.append(support)
        res.support_foot_transform.append(support_frame)
        res.lfoot_goal.append(Frame2Point(lfoot));
        res.lfoot_goal_direction.append(Frame2Point(lfoot*relfwd));
        res.lfoot_goal_up.append(Frame2Point(lfoot*relup));
        res.rfoot_goal.append(Frame2Point(rfoot));
        res.rfoot_goal_direction.append(Frame2Point(rfoot*relfwd));
        res.rfoot_goal_up.append(Frame2Point(rfoot*relup));
        res.lcom_goal.append(Frame2Point(lcom))
        res.rcom_goal.append(Frame2Point(rcom))
        for j in range(0,NN):
            res.pose[id*NN+j]=q[stp][j]
        id+=1
      support=1-support
    return res

def goal_generator_server():
    global q
    global footl
    global footr
    global jmap
    global keys
    rospy.init_node('goal_generator_server')

    rospack = rospy.core.rospkg.RosPack()
    pth=rospack.get_path("data_replay")
    q_ = pickle.load(open(pth+"/scripts/cspace-path-values.dat","rb"))
    footl_ = pickle.load(open(pth+"/scripts/cspace-path-footL.dat","rb"))
    footr_ = pickle.load(open(pth+"/scripts/cspace-path-footR.dat","rb"))
    jmap_ = pickle.load(open(pth+"/scripts/cspace-path-jmap.dat","rb"))
    keys_ = pickle.load(open(pth+"/scripts/cspace-path-keys.dat","rb"))
    istart=0
    iend=len(q_)
    istart=15
    iend=25
    print istart
    print iend
    q=q_[istart:iend];
    footl=footl_[istart:iend];
    footr=footr_[istart:iend];
    jmap=jmap_[istart:iend];
    keys=keys_[istart:iend];

    s = rospy.Service('goal_generator', GenerateGoals, handle_generate_goals)
    print "Running goal generator service."
    rospy.spin()

if __name__ == "__main__":
    goal_generator_server()
