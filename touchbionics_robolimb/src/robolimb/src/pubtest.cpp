#include "ros/ros.h"
#include "std_msgs/String.h"
#include <sstream>

#include <robolimb/group.h>
#include <robolimb/robotcommand_gr_t.h>

///Reads CAN Messages from the robolimb and publishes it.

int main(int argc, char **argv)
{

  ros::init(argc, argv, "pubtest");

  ros::NodeHandle n;

  ros::Publisher pub = n.advertise<robolimb::robotcommand_gr_t>("command_group_timed", 1000);

  robolimb::group data;
  robolimb::robotcommand_gr_t msg;

  data.iDigit=1; data.iState=1; data.iVelocity=250; data.iStartTime=0; data.iSendTime=500;

  msg.timedcommands.push_back(data);

  data.iDigit=2; data.iState=1; data.iVelocity=250; data.iStartTime=1000; data.iSendTime=500;

   msg.timedcommands.push_back(data);

 data.iDigit=3; data.iState=1; data.iVelocity=250; data.iStartTime=1500; data.iSendTime=500;

   msg.timedcommands.push_back(data);

ros::Rate loop_rate(20);

int i=6;

  while(i>0)
  {

if(ros::ok())
{
 ROS_INFO("%s","message sent");

  pub.publish(msg);

 ros::spinOnce();

 loop_rate.sleep();

i--;
}

  }
 
  return 0;
}
