#include "ros/ros.h"
#include "std_msgs/String.h"
#include <sstream>

#include <robolimb/robotfeedback.h>
#include "robolimb/PCANBasic.h"

///Reads CAN Messages from the robolimb and publishes it.

int main(int argc, char **argv)
{

  TPCANMsg ReadMessage;

  ros::init(argc, argv, "robotstatus");

  ros::NodeHandle n;

  robolimb::robotfeedback currentstatus;

  ros::Publisher status_pub = n.advertise<robolimb::robotfeedback>("status", 1000);

  ros::Rate loop_rate(20);

  int8_t Status = CAN_Initialize(PCAN_USBBUS1 , PCAN_BAUD_1M, 0, 0, 0);

  if(Status!=0)
	ROS_INFO("%s","CAN Initialization Failure");

  while (ros::ok())
  {

		while ((Status=CAN_Read(PCAN_USBBUS1,&ReadMessage,NULL)) != PCAN_ERROR_QRCVEMPTY)
		{
			if (Status != PCAN_ERROR_OK) 
			{
				ROS_INFO("%s","CAN Message Read Error");
				break;
			}

			if(ReadMessage.ID==0x201)	
			{
				currentstatus.iDigitstatus1=(int)ReadMessage.DATA[1];
				currentstatus.iDigitCurrent1=((int)ReadMessage.DATA[2]*256 + (int)ReadMessage.DATA[3])/218.25;
			}
			else if(ReadMessage.ID==0x202)	
			{
				currentstatus.iDigitstatus2=(int)ReadMessage.DATA[1];
				currentstatus.iDigitCurrent2=((int)ReadMessage.DATA[2]*256 + (int)ReadMessage.DATA[3])/218.25;
			}
			else if(ReadMessage.ID==0x203)	
			{
				currentstatus.iDigitstatus3=(int)ReadMessage.DATA[1];
				currentstatus.iDigitCurrent3=((int)ReadMessage.DATA[2]*256 + (int)ReadMessage.DATA[3])/218.25;
			}
			else if(ReadMessage.ID==0x204)	
			{
				currentstatus.iDigitstatus4=(int)ReadMessage.DATA[1];
				currentstatus.iDigitCurrent4=((int)ReadMessage.DATA[2]*256 + (int)ReadMessage.DATA[3])/218.25;
			}
			else if(ReadMessage.ID==0x205)	
			{
				currentstatus.iDigitstatus5=(int)ReadMessage.DATA[1];
				currentstatus.iDigitCurrent5=((int)ReadMessage.DATA[2]*256 + (int)ReadMessage.DATA[3])/218.25;
			}
			else if(ReadMessage.ID==0x206)	
			{
				currentstatus.iDigitstatus6=(int)ReadMessage.DATA[1];
				currentstatus.iDigitCurrent6=((int)ReadMessage.DATA[2]*256 + (int)ReadMessage.DATA[3])/218.25;
			}
			//status_pub.publish(currentstatus);
		}
		status_pub.publish(currentstatus);

    
    ros::spinOnce();

    loop_rate.sleep();
  }


  return 0;
}
