#include "ros/ros.h"
#include "std_msgs/String.h"
#include <robolimb/robotcommand_in.h>
#include <robolimb/robotcommand_gr.h>
#include <robolimb/group.h>
#include <robolimb/robotcommand_gr_t.h>
#include "robolimb/PCANBasic.h"


void commandCallback_in(const robolimb::robotcommand_in msg)
{
  TPCANMsg Message;

  Message.LEN = 4;
  Message.MSGTYPE = PCAN_MESSAGE_STANDARD;
  Message.ID = 0x100+msg.iDigit;
  Message.DATA[0]=0x00;
  Message.DATA[1]=0x00+msg.iState;
  Message.DATA[2]=(msg.iVelocity/256);
  Message.DATA[3]=(msg.iVelocity%256);

  ROS_INFO("%d",CAN_Write(PCAN_USBBUS1,&Message));
}

void sendcommand(int iDigit,int iState,int iVelocity)
{
  TPCANMsg Message;

  Message.LEN = 4;
  Message.MSGTYPE = PCAN_MESSAGE_STANDARD;
  Message.ID = 0x100+iDigit;
  Message.DATA[0]=0x00;
  Message.DATA[1]=0x00+iState;
  Message.DATA[2]=(iVelocity/256);
  Message.DATA[3]=(iVelocity%256);

  ROS_INFO("%d",CAN_Write(PCAN_USBBUS1,&Message));
}


int CommandDigit_Timed(int iDigit,int iState,int iVelocity,float fTime)
{
TPCANMsg Message;

Message.LEN = 4;
Message.MSGTYPE = PCAN_MESSAGE_STANDARD;
Message.ID = 0x100+iDigit;
Message.DATA[0]=0x00;
Message.DATA[1]=0x00+iState;
Message.DATA[2]=(iVelocity/256);
Message.DATA[3]=(iVelocity%256);

if(CAN_Write(PCAN_USBBUS1,&Message)==PCAN_ERROR_OK)
{
ROS_INFO("sleepinf for %f",fTime);
ros::Duration(fTime).sleep();
}


Message.ID = 0x100+iDigit;
Message.DATA[0]=0x00;
Message.DATA[1]=0x00;
Message.DATA[2]=(iVelocity/256);
Message.DATA[3]=(iVelocity%256);

return (CAN_Write(PCAN_USBBUS1,&Message));

}


bool sortByStartTime(const robolimb::group &lhs, const robolimb::group &rhs)
 { 
	return lhs.iStartTime < rhs.iStartTime;
 }


void commandCallback_group_timed(const robolimb::robotcommand_gr_t::ConstPtr&  msg)
{

 ROS_INFO("%s","message received");
 
int iSize=msg->timedcommands.size();

  std::vector<robolimb::group> cmd(iSize);

   for (int i=0; i< iSize; ++i)
    {
      const robolimb::group &data = msg->timedcommands[i];
      cmd[i]=msg->timedcommands[i];
      ROS_INFO_STREAM("ivalue" << i << "iDigit: " << data.iDigit << "iState: " << data.iState <<
                      "iStartTime: " << data.iStartTime << "iSendTime: " << data.iSendTime);
    }

   std::sort(cmd.begin(), cmd.end(), sortByStartTime);

    int idelaycum=0;

     for(int i=0;i<iSize;i++)
    {
			ros::Duration((cmd[i].iStartTime-idelaycum)/1000).sleep(); 

			idelaycum=idelaycum+cmd[i].iStartTime;
			
			if(CommandDigit_Timed(cmd[i].iDigit,cmd[i].iState,cmd[i].iVelocity,(cmd[i].iSendTime/1000.0))==PCAN_ERROR_OK)
			{
				ROS_INFO("Send Command digit=%d state=%d velocity=%d sendtime=%d",cmd[i].iDigit,cmd[i].iState,cmd[i].iVelocity,cmd[i].iSendTime);
			}
			else
			{
				printf("%s","Command Failed\n");
			}
   }

}


void commandCallback_group(const robolimb::robotcommand_gr msg)
{
sendcommand(1,msg.iState1,msg.iVelocity1);
sendcommand(2,msg.iState2,msg.iVelocity2);
sendcommand(3,msg.iState3,msg.iVelocity3);
sendcommand(4,msg.iState4,msg.iVelocity4);
sendcommand(5,msg.iState5,msg.iVelocity5);
sendcommand(6,msg.iState6,msg.iVelocity6);
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "robotcommands");

    int8_t Status = CAN_Initialize(PCAN_USBBUS1 , PCAN_BAUD_1M, 0, 0, 0);

  if(Status!=0)
	ROS_INFO("%s","CAN Initialization Failure");

  ros::NodeHandle n;
  ros::Subscriber sub1 = n.subscribe("command_in", 1000, commandCallback_in);
  ros::Subscriber sub2 = n.subscribe("command_group", 1000, commandCallback_group);
  ros::Subscriber sub3 = n.subscribe("command_group_timed", 1000, commandCallback_group_timed);
  ros::spin();

  return 0;
}
