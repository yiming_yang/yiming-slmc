/*
 * lwr_driver.h
 *
 *  Created on: 4 Nov 2014
 *      Author: Vladimir Ivan
 */

#ifndef LWR_DRIVER_H_
#define LWR_DRIVER_H_

#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <Eigen/Dense>
#include <eigen_conversions/eigen_msg.h>
#include <string>
#include <boost/shared_ptr.hpp>
#include <boost/thread/thread.hpp>
#include <boost/thread/mutex.hpp>
#include "fri_remote.h"
#include <lwr_driver/FriCommand.h>
#include <lwr_driver/FriCommandJointPosition.h>
#include <lwr_driver/FriCommandJointStiffness.h>
#include <lwr_driver/FriCommandCartesianStiffness.h>
#include <lwr_driver/FriCommandMode.h>
#include <lwr_driver/FriState.h>
#include <dynamic_reconfigure/server.h>
#include <lwr_driver/lwr_driverConfig.h>

#define CONST_FRI_DOF LBR_MNJ
#define CONST_CART FRI_CART_VEC
#define CONST_CART_POSE FRI_CART_FRM_DIM
#define CONST_MAX_ANGULAR_VELOCITY {100.0/180.0*M_PI,110.0/180.0*M_PI,100.0/180.0*M_PI,130.0/180.0*M_PI,130.0/180.0*M_PI,180.0/180.0*M_PI,180.0/180.0*M_PI}

enum ENUM_FRI_CONTROL_MODE {
    FRI_OFF = 0,
    FRI_POSITION_CONTROL = 10,
    FRI_CARTESIAN_STIFFNESS = 20,
    FRI_JOINT_STIFNESS = 30
};

/**
 * \brief This structure describes the commands for the robot.
 */
struct FRICommand
{
		std::vector<float> jointPosition; //!< \brief Commanded joint angles (used in FRI_JOINT_STIFNESS and FRI_POSITION_CONTROL control modes)
		std::vector<float> jointStiffness; //!< \brief Joint stiffness properties (FRI_JOINT_STIFNESS control mode)
		std::vector<float> jointDamping; //!< \brief Joint damping properties (FRI_JOINT_STIFNESS control mode)
		std::vector<float> jointTorque; //!< \brief Commanded joint torques (used in FRI_JOINT_STIFNESS control mode)
		std::vector<float> cartPose; //!< \brief Desired Cartesian 6D pose (FRI_CARTESIAN_STIFFNESS control mode)
		std::vector<float> cartStiffness; //!< \brief Desired Cartesian 6D stiffness (FRI_CARTESIAN_STIFFNESS control mode)
		std::vector<float> cartDamping; //!< \brief Desired Cartesian 6D damping (FRI_CARTESIAN_STIFFNESS control mode)
		std::vector<float> cartForce; //!< \brief Desired Cartesian 6D external force (used in FRI_CARTESIAN_STIFFNESS control mode)
		std::vector<float> cartNull; //!< \brief Joint configuration for  Null space resolution (used in FRI_CARTESIAN_STIFFNESS control mode)
		ENUM_FRI_CONTROL_MODE controlMode; //!< \brief Control mode commanded by user program (Used when changing control modes from user program).
		ros::Time commandTime; //!< Time when the command was update (not the time when it was actually sent to the robot though)
		FRICommand();
};

/**
 * \brief This structure describes the state of the robot.
 */
struct FRIState
{
	std::vector<float> jointPosition; //!< \brief Measured joint angles (as received from KRC)
	std::vector<float> jointVelocity; //!< \brief Estimated joint velocity (estimated from measured joint angles)
	std::vector<float> jointCmdPosition; //!< \brief Commanded joint angles (as received from KRC)
	std::vector<float> jointTorque; //!< \brief Measured joint torques (as received from KRC)
	std::vector<float> jointTorqueExt; //!< \brief Measured external joint torques (as received from KRC)
	std::vector<float> cartPose; //!< \brief Measured Cartesian 6D pose (FRI_CARTESIAN_STIFFNESS control mode)
	std::vector<float> cartCmdPose; //!< \brief Commanded Cartesian 6D pose (FRI_CARTESIAN_STIFFNESS control mode)
	std::vector<float> cartCmdPoseOffset; //!< \brief Commanded Cartesian 6D pose FRI offset (FRI_CARTESIAN_STIFFNESS control mode)
	float dT; //!< \brief Communication loop time (also used for velocity and acceleration calculation)
	ros::Time stateTime; //!< \brief Time when the state measurement was received
	int controlScheme; //!< \brief Control scheme (0 - other, 10 - position, 20 - cart stiffness, 30 - joint stiffness)
	int mode; //!< \brief Control mode (0 - off, 1 - monitor, 2 - command)
	int quality; //!< \brief Communication quality (0 - unacceptable, 1 - bad, 2 - OK, 3 - perfect)
	FRICommand command;

	FRIState();
};

struct FRIStateInternal
{
		std::vector<float> jointPosition;
		std::vector<float> jointVelocity;
		std::vector<float> jointAcceleration;
		std::vector<float> jointPositionPrev;
		std::vector<float> jointPositionPrevPrev;
		std::vector<float> jointVelocityPrev;
		ros::Time stateTime;
		float dT;
		float dTPrev;
		int cnt;

		FRIStateInternal();
		void update(const std::vector<float>& state, const ros::Time& t);
};

struct FRIJointLimits
{
		float positionMin;
		float positionMax;
		float velocity;
		float acceleration;
};

geometry_msgs::Wrench vectorToWrench(std::vector<float>& vec);
void wrenchToVector(const geometry_msgs::Wrench & wr, std::vector<float>& vec);

class LWRDriver
{
	public:
		LWRDriver ();
		virtual
		~LWRDriver ();

		/**
		 * \brief Starts the communication thread and connects to the remote host
		 * @param port UDP port number
		 * @param hintToHost Hint to KRC network address
		 */
		bool Start(int port, std::string hintToHost);

		/**
		 * \brief Stops the communication thread.
		 */
		void Stop();

	private:
		/**
		 * \brief Communication thread function
		 * @param port UDP port number
		 * @param hintToHost Hint to KRC network address
		 */
		void CommunicationThreadFunction(int port, std::string hintToHost);

		/**
		 * \brief State publishing thread function
		 */
		void StatePublisherThreadFunction();

		/**
		 * \brief Copies state from the friRemote class
		 * @param ret Returned state
		 * @param friInst Refrence to the friRemote class
		 */
		void CopyState(FRIState& ret, friRemote& friInst);

		/**
		 * \brief Joint position command callback
		 * @param Command message.
		 */
		void jointPositionCommandCallback(const lwr_driver::FriCommandJointPosition& msg);

		/**
		 * \brief Joint stiffness command callback
		 * @param Command message.
		 */
		void jointStiffnessCommandCallback(const lwr_driver::FriCommandJointStiffness& msg);

		/**
		 * \brief Cartesian stiffness command callback
		 * @param Command message.
		 */
		void cartesianStiffnessCommandCallback(const lwr_driver::FriCommandCartesianStiffness& msg);

		/**
		 * \brief Control mode command callback
		 * @param Command message.
		 */
		void modeCommandCallback(const lwr_driver::FriCommandMode& msg);

		/**
		 * \brief Loads joint limits from parameter server
		 * @param joint_name Joint name
		 * @param limits Returned joint limits struct
		 * @return True on success
		 */
		bool getJointLimits(const std::string& joint_name, FRIJointLimits& limits);

		/**
		 * \brief Applies joint position, velocity and acceleration limits to the command
		 * @param stateI Internal state for keeping track of previously commanded positions
		 * @param state Current state of the robot
		 * @param command Command to be modified
		 */
		void limitCommand(FRIStateInternal& stateI, const FRIState& state, FRICommand& command);

		void dynamicCallback(lwr_driver::lwr_driverConfig &config, uint32_t level);

	  int wantsClose_; //!< \brief Indicates whether the FRI server wants to terminate.
	  FRIState state_; //!< \brief Current state.
	  FRICommand command_;  //!< \brief Current command.
	  bool threadRunning_; //!< \brief Communication thread running flag.
	  boost::thread communicationThread_; //!< \brief Communication thread.
	  boost::thread statePublisherThread_; //!< \brief State publisher thread.
	  boost::mutex stateLock_; //!< \brief State update thread mutex.
	  boost::condition_variable stateCond_; //!< \brief State update thread condition.
	  bool readyToPublish_; //!< \brief Flag for signalling that the state data is ready for publishing.
          bool command_soft_; //!< \brief Toggles sending the commands to the robot using rqt_reconfigure. Use this as software emegency stop.
	  boost::mutex commandLock_; //!< \brief Command update thread mutex.
	  ros::NodeHandle nh_; //!< \brief ROS node handle.
	  ros::Publisher statePublisher_; //!< \brief State topic publisher.
	  ros::Publisher jointStatePublisher_; //!< \brief Joint state topic publisher.
	  ros::Subscriber subPos_; //!< \brief Joint position command topic subscriber.
	  ros::Subscriber subJnt_; //!< \brief Joint stiffness command topic subscriber.
	  ros::Subscriber subCart_; //!< \brief Cartesian stiffness command topic subscriber.
	  ros::Subscriber subMode_; //!< \brief Control mode command topic subscriber.
	  ros::Time startTime_; //!< \brief Time when first data packet arrived.
	  std::vector<FRIJointLimits> limits_; //!< \brief Joint limits
	  std::vector<std::string> jointNames_; //!< \brief Joint names
	  float accelerationSafe_; //!< \brief Velocity safety limit multiplier
	  float velocitySafe_; //!< \brief Acceleration safety limit multiplier
	  boost::mutex limitsLock_; //!< \brief Safety limit thread lock
	  dynamic_reconfigure::Server<lwr_driver::lwr_driverConfig> dyn_server_;
};

#endif /* LWR_DRIVER_H_ */
