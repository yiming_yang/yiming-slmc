/*
 * lwr_driver.cpp
 *
 *  Created on: 4 Nov 2014
 *      Author: Vladimir Ivan
 */

#include "lwr_driver/lwr_driver.h"
#include <math.h>

inline float sign(float val)
{
    if(val<0)
    {
        return -1;
    }
    else
        return 1;
};

inline double sign(double val)
{
    if(val<0)
    {
        return -1;
    }
    else
        return 1;
};

FRIState::FRIState(): dT(0.0), controlScheme(0), mode(0), quality(0)
{
	jointPosition.assign(CONST_FRI_DOF,0.0f);
	jointTorque.assign(CONST_FRI_DOF,0.0f);
	jointTorqueExt.assign(CONST_FRI_DOF,0.0f);
	cartPose.assign(CONST_CART,0.0f);
	cartCmdPose.assign(CONST_CART,0.0f);
	cartCmdPoseOffset.assign(CONST_CART,0.0f);
	jointVelocity.assign(CONST_FRI_DOF,0.0f);
}

FRICommand::FRICommand(): controlMode(FRI_OFF)
{
	jointPosition.assign(CONST_FRI_DOF,0.0f);
	jointStiffness.assign(CONST_FRI_DOF,500.0f);
	jointDamping.assign(CONST_FRI_DOF,0.2f);
	jointTorque.assign(CONST_FRI_DOF,0.0f);
	cartPose.assign(CONST_CART_POSE,0.0f);
	cartStiffness.assign(CONST_CART,500.0f);
	cartDamping.assign(CONST_CART,0.2f);
	cartForce.assign(CONST_CART,0.0f);
	cartNull.assign(CONST_FRI_DOF,0.0f);
}

FRIStateInternal::FRIStateInternal() : cnt(0), dT(0.0f), dTPrev(0.0f)
{
	jointPosition.assign(CONST_FRI_DOF,0.0f);
	jointVelocity.assign(CONST_FRI_DOF,0.0f);
	jointAcceleration.assign(CONST_FRI_DOF,0.0f);
	jointPositionPrev.assign(CONST_FRI_DOF,0.0f);
	jointPositionPrevPrev.assign(CONST_FRI_DOF,0.0f);
	jointVelocityPrev.assign(CONST_FRI_DOF,0.0f);
}

void FRIStateInternal::update(const std::vector<float>& state, const ros::Time& t)
{
	if(cnt>0 && (float)(t-stateTime).toSec()>dT*3.0f)
	{
		cnt=0;
		ROS_WARN_STREAM_THROTTLE(0.5,"State update timing inconsistent. Velocities reset to zero.");
	}
	switch(cnt)
	{
		case 0:
			jointPosition=state;
			jointPositionPrev=state;
			jointPositionPrevPrev=state;
			dTPrev=dT=1.0;
			cnt++;
			break;
		case 1:
			jointPositionPrevPrev=jointPosition;
			jointPositionPrev=jointPosition;
			dTPrev=dT=(float)(t-stateTime).toSec();
			jointPosition=state;
			cnt++;
			break;
		default:
			jointPositionPrevPrev=jointPositionPrev;
			jointPositionPrev=jointPosition;
			jointPosition=state;
			dTPrev=dT;
			dT=(float)(t-stateTime).toSec();
			break;
	}
	stateTime=t;
	jointVelocityPrev=jointVelocity;
	float tmpv;
	for(int i=0;i<CONST_FRI_DOF;i++)
	{
		jointVelocity[i]=(jointPosition[i]-jointPositionPrev[i])/dT;
		jointVelocityPrev[i]=jointVelocity[i]+(jointPositionPrev[i]-jointPositionPrevPrev[i])/dTPrev;
		jointAcceleration[i]=(jointVelocity[i]-jointVelocityPrev[i])/dT;
	}
}

LWRDriver::LWRDriver () : threadRunning_(false), wantsClose_(0), nh_(), readyToPublish_(false), accelerationSafe_(0.2), velocitySafe_(0.2)
{
	ROS_INFO_STREAM("Starting KUKA LWR4 FRI driver.");
	statePublisher_ = nh_.advertise<lwr_driver::FriState>("kuka_lwr_state",10);
	jointStatePublisher_ = nh_.advertise<sensor_msgs::JointState>("/joint_states", 1);
	subPos_ = nh_.subscribe("/lwr/commandJointPosition",10, &LWRDriver::jointPositionCommandCallback, this);
	subJnt_ = nh_.subscribe("/lwr/commandJointStiffness",10, &LWRDriver::jointStiffnessCommandCallback, this);
	subCart_ = nh_.subscribe("/lwr/commandCartesianStiffness",10, &LWRDriver::cartesianStiffnessCommandCallback, this);
	subMode_ = nh_.subscribe("/lwr/commandMode",10, &LWRDriver::modeCommandCallback, this);
	limits_.resize(CONST_FRI_DOF);
	jointNames_ = {	"lwr_arm_0_joint","lwr_arm_1_joint","lwr_arm_2_joint","lwr_arm_3_joint","lwr_arm_4_joint","lwr_arm_5_joint","lwr_arm_6_joint"};
}

LWRDriver::~LWRDriver ()
{
	Stop();
}

bool LWRDriver::Start(int port, std::string hintToHost)
{
	bool gotLimits=true;
	for(int i=0;i<CONST_FRI_DOF;i++)
	{
		gotLimits=gotLimits && getJointLimits(jointNames_[i],limits_[i]);
	}
	if(!gotLimits)
	{
		return false;
	}
	if(!threadRunning_)
	{
		dynamic_reconfigure::Server<lwr_driver::lwr_driverConfig>::CallbackType f;
		f = boost::bind(&LWRDriver::dynamicCallback, this, _1, _2);
		dyn_server_.setCallback(f);
		threadRunning_ = true;
		wantsClose_ = false;
		readyToPublish_=false;
		statePublisherThread_=boost::thread(&LWRDriver::StatePublisherThreadFunction, this);
		communicationThread_=boost::thread(&LWRDriver::CommunicationThreadFunction,this,port,hintToHost);
#if defined(BOOST_THREAD_PLATFORM_PTHREAD)
		{
			// ... pthread version
			ROS_WARN_STREAM("FRI: Setting communication thread scheduling attributes.");
			int retcode;
			int policy;
			struct sched_param param;
			pthread_t threadID = (pthread_t) communicationThread_.native_handle();
			policy = SCHED_FIFO;
			param.sched_priority = 90;
			if ((retcode = pthread_setschedparam(threadID, policy, &param)) != 0)
			{
				switch(retcode)
				{
					case ESRCH:
						ROS_WARN_STREAM("Error setting thread parameter! (" << retcode <<")\nNo thread with the ID "<<threadID<<" could be found.");
						break;
					case EINVAL:
						ROS_WARN_STREAM("Error setting thread parameter! (" << retcode <<")\nPolicy "<<policy<<" is not a recognized policy, or param does not make sense for the policy.");
						break;
					case EPERM:
						ROS_WARN_STREAM("Error setting thread parameter! (" << retcode <<")\nThe caller does not have appropriate privileges to set the specified scheduling policy and parameters.");
						break;
					default:
						ROS_WARN_STREAM("Error setting thread parameter! (" << retcode <<")");
						break;
				}

			}
		}
#endif
	}
	else
	{
		ROS_WARN_STREAM("FRI: Communication thread is already running.");
	}
	return true;
}

void LWRDriver::Stop()
{
  if(threadRunning_)
  {
  	wantsClose_=true;
  	statePublisherThread_.join();
  	communicationThread_.join();
  	threadRunning_=false;
  }
}

void LWRDriver::CopyState(FRIState& ret, friRemote& friInst)
{
  ret.jointPosition.assign(friInst.getMsrMsrJntPosition(),friInst.getMsrMsrJntPosition()+CONST_FRI_DOF);
  ret.jointCmdPosition.assign(friInst.getMsrCmdJntPosition(),friInst.getMsrCmdJntPosition()+CONST_FRI_DOF);
  ret.jointTorque.assign(friInst.getMsrJntTrq(),friInst.getMsrJntTrq()+CONST_FRI_DOF);
  ret.jointTorqueExt.assign(friInst.getMsrEstExtJntTrq(),friInst.getMsrEstExtJntTrq()+CONST_FRI_DOF);
  ret.cartPose.assign(friInst.getMsrCartPosition(),friInst.getMsrCartPosition()+CONST_CART_POSE);
  ret.dT = friInst.getSampleTime();
  ret.controlScheme = (int)friInst.getCurrentControlScheme();
  ret.mode = (int)friInst.getState();
  ret.quality = (int)friInst.getQuality();
  ret.cartCmdPose.assign(friInst.getMsrCmdCartPosition(),friInst.getMsrCmdCartPosition()+CONST_CART_POSE);
  ret.cartCmdPoseOffset.assign(friInst.getMsrCmdCartPosFriOffset(),friInst.getMsrCmdCartPosFriOffset()+CONST_CART_POSE);

  ret.stateTime = startTime_ + ros::Duration((double)friInst.getSequenceCount()*(double)ret.dT);
}

inline std::ostream& operator << (std::ostream& os, const std::vector<float>& v)
{
    os << "[";
    for (std::vector<float>::const_iterator ii = v.begin(); ii != v.end(); ++ii)
    {
        os << " " << *ii;
    }
    os << " ]";
    return os;
}

void LWRDriver::dynamicCallback(lwr_driver::lwr_driverConfig &config, uint32_t level)
{
	boost::mutex::scoped_lock lock(limitsLock_);
	velocitySafe_=std::min(1.0f,std::max(0.0f,(float)config.v_multiplier));
	accelerationSafe_=std::min(1.0f,std::max(0.0f,(float)config.a_multiplier));
        command_soft_=config.command;
}

void LWRDriver::limitCommand(FRIStateInternal& stateI, const FRIState& state, FRICommand& command)
{

	float v,a,bb,bt,ss,p1,p2,p3,velocitySafe,accelerationSafe;
	{
		boost::mutex::scoped_lock lock(limitsLock_);
		velocitySafe=velocitySafe_;
		accelerationSafe=accelerationSafe_;
	}
	stateI.update(command.jointPosition,state.stateTime);
	for(int i=0;i<CONST_FRI_DOF;i++)
	{
		// Previous time step
		p1=stateI.jointPositionPrevPrev[i];
		// Current time step
		p2=stateI.jointPositionPrev[i];
		// Next desired time step
		p3=std::min(std::max(stateI.jointPosition[i],limits_[i].positionMin),limits_[i].positionMax);
		// Velocity
		v=limits_[i].velocity*velocitySafe*state.dT;
		// Accelereation
		a=limits_[i].acceleration*accelerationSafe*state.dT*state.dT;

		// Bottom position bounded by velocity and acceleration
		bb=std::max(p2-v,2.0f*p2-p1-a);
		// Top position bounded by velocity and acceleration
		bt=std::min(p2+v,2.0f*p2-p1+a);

		if (p2>p1)
		{
			// Stopping point
			ss=p3-(p2-p1)*0.5f*((p2-p1)/a+1.0f);
			if (ss<=bb)
			{
				// If at stopping point or past it, slow down
				stateI.jointPosition[i]=bb;
			}
			else if (ss>=bt)
			{
				// If far away from stopping point, accelerate towards it
				stateI.jointPosition[i]=bt;
			}
			else
			{
				// If next step needs to start slowing down but is not at stopping point yet, slow down to the stopping point
				stateI.jointPosition[i]=ss;
			}
		}
		else if (p2<p1)
		{
			// Stopping point
			ss=p3+(p2-p1)*0.5f*((p2-p1)/a+1.0f);
			if (ss<=bb)
			{
				// If far away from stopping point, accelerate towards it
				stateI.jointPosition[i]=bb;
			}
			else if (ss>=bt)
			{
				// If at stopping point or past it, slow down
				stateI.jointPosition[i]=bt;
			}
			else
			{
				// If next step needs to start slowing down but is not at stopping point yet, slow down to the stopping point
				stateI.jointPosition[i]=ss;
			}
		}
		else
		{
			stateI.jointPosition[i]=std::max(bb,std::min(bt,p3));
		}
	}



	command.jointPosition=stateI.jointPosition;
	return;

}

void LWRDriver::CommunicationThreadFunction(int port, std::string hintToHost)
{
	char * hint = new char[hintToHost.size() + 1];
	std::copy(hintToHost.begin(), hintToHost.end(), hint);
	friRemote friInst(port,hint);

	FRIState state;
	FRICommand command;

	FRIStateInternal sint;
	FRIStateInternal cint;

	friInst.doDataExchange();
	startTime_ = ros::Time::now();

	CopyState(state,friInst);
	sint.update(state.jointPosition,state.stateTime);
	cint.update(state.jointCmdPosition,state.stateTime);
	command.controlMode =  FRI_POSITION_CONTROL;
	command.jointPosition = state.jointCmdPosition;
	command.cartPose = state.cartCmdPose;
  command.controlMode = FRI_POSITION_CONTROL;
	{
		boost::mutex::scoped_lock lock(commandLock_);
		command.commandTime = ros::Time::now();
		command_=command;
	}

	bool start = true;

	do
	{
		if(command_soft_)
		{
			boost::mutex::scoped_lock lock(commandLock_);
			command=command_;
		}
		limitCommand(cint,state,command);
		switch ( friInst.getCurrentControlScheme() )
		{
			case FRI_CTRL_POSITION:
				if ( friInst.getState() == FRI_STATE_CMD )
				{
						if(start)
						{
								start=false;
								ROS_INFO_STREAM("Switching to position control");
								boost::mutex::scoped_lock lock(commandLock_);
								command.jointPosition = state.jointCmdPosition;
								command.commandTime = ros::Time::now();
								command_=command;
								cint.cnt=0;
						}
						friInst.doPositionControl(command.jointPosition.data(),false);
				}
				else
				{
						friInst.doPositionControl(state.jointCmdPosition.data(),false);
				}
				break;
			case FRI_CTRL_JNT_IMP:
				if ( friInst.getState() == FRI_STATE_CMD )
				{
					if(start)
					{
						ROS_INFO_STREAM("Switching to joint impedance control");
						start=false;
						boost::mutex::scoped_lock lock(commandLock_);
						command.jointPosition = state.jointCmdPosition;
						command.commandTime = ros::Time::now();
						command_=command;
						cint.cnt=0;
					}
					friInst.doJntImpedanceControl(command.jointPosition.data(),command.jointStiffness.data(),command.jointDamping.data(),command.jointTorque.data(),false);
				}
				else
				{
					friInst.doJntImpedanceControl(state.jointCmdPosition.data(),NULL,NULL,NULL,false);
				}
				break;
			case FRI_CTRL_CART_IMP:
				// TODO: Implement cartesian impedance mode
				if ( friInst.getState() == FRI_STATE_CMD )
				{
						if(start)
						{
							ROS_INFO_STREAM("Switching to Cartesian impedance control");
							start=false;
							boost::mutex::scoped_lock lock(commandLock_);
							command.cartPose = state.cartPose;
							command.commandTime = ros::Time::now();
							command.cartNull=state.jointCmdPosition;
							command_=command;
							cint.cnt=0;
						}
						friInst.doCartesianImpedanceControl(NULL,command.cartStiffness.data(),command.cartDamping.data(),command.cartForce.data(), command.jointPosition.data(),false);
				}
				else
				{
					friInst.doCartesianImpedanceControl(NULL,command.cartStiffness.data(),command.cartDamping.data(),command.cartForce.data(), state.jointCmdPosition.data(),false);
				}
				break;
			default:
				ROS_WARN_STREAM("Other control scheme!");
				break;
		}

		if ( friInst.getState() == FRI_STATE_CMD )
		{
			if(friInst.getFrmKRLInt(0)==0)
			{
				friInst.setToKRLInt(0, (int)command.controlMode);
			}
			else
			{
				friInst.setToKRLInt(0, 0);
			}
		}
		else
		{
			switch(command.controlMode)
			{
				case 10:
					friInst.doPositionControl(command.jointPosition.data(),false);
					break;
				case 20:
					friInst.doCartesianImpedanceControl(NULL,command.cartStiffness.data(),command.cartDamping.data(),command.cartForce.data(), state.jointCmdPosition.data(),false);
					break;
				case 30:
					friInst.doJntImpedanceControl(command.jointPosition.data(),command.jointStiffness.data(),command.jointDamping.data(),command.jointTorque.data(),false);
					break;
			}
			start=true;
		}
		friInst.doDataExchange();
		CopyState(state,friInst);
		sint.update(state.jointPosition,state.stateTime);
		state.jointVelocity = sint.jointVelocity;
		state.command=command;
		{
			boost::lock_guard<boost::mutex> lock(stateLock_);
			state_=state;
			readyToPublish_=true;
		}
		stateCond_.notify_all();
	} while (!wantsClose_);

	while (friInst.getFrmKRLInt(0)!=5)
	{
		friInst.setToKRLInt(0, (int)FRI_STATE_OFF);
		friInst.doDataExchange();
	}
	boost::this_thread::sleep(boost::posix_time::milliseconds(500));
}

void LWRDriver::StatePublisherThreadFunction()
{
	lwr_driver::FriState msg;
	lwr_driver::FriCommand cmd;
	sensor_msgs::JointState jnt;
	msg.joint_names = jointNames_;
	cmd.joint_names = jnt.name = msg.joint_names;
	jnt.position.resize(CONST_FRI_DOF);
	jnt.velocity.resize(CONST_FRI_DOF);
	jnt.effort.resize(CONST_FRI_DOF);
	Eigen::Affine3d aff;
	Eigen::Matrix4d tmp = Eigen::Matrix4d::Identity();
	while(!wantsClose_)
	{
		boost::unique_lock<boost::mutex> lock(stateLock_);
		while(!readyToPublish_)
		{
			stateCond_.wait(lock);
		}
		readyToPublish_=false;

		if(!wantsClose_)
		{
			msg.jointPosition = state_.jointPosition;
			msg.jointCmdPosition = state_.jointCmdPosition;
			msg.jointTorque = state_.jointTorque;
			msg.jointTorqueExt = state_.jointTorqueExt;
			msg.controlScheme = state_.controlScheme;
			msg.mode = state_.mode;
			msg.quality = state_.quality;
			msg.dT = state_.dT;
			tmp.block<3,4>(0,0)=Eigen::Map<Eigen::Matrix< float,3,4,Eigen::RowMajor > >(state_.cartPose.data()).cast<double>();
			aff=Eigen::Affine3d(tmp);
			tf::poseEigenToMsg(aff, msg.cartPose);

			tmp.block<3,4>(0,0)=Eigen::Map<Eigen::Matrix< float,3,4,Eigen::RowMajor > >(state_.cartCmdPose.data()).cast<double>();
			aff=Eigen::Affine3d(tmp);
			tf::poseEigenToMsg(aff, msg.cartCmdPose);

			tmp.block<3,4>(0,0)=Eigen::Map<Eigen::Matrix< float,3,4,Eigen::RowMajor > >(state_.cartCmdPoseOffset.data()).cast<double>();
			aff=Eigen::Affine3d(tmp);
			tf::poseEigenToMsg(aff, msg.cartCmdPoseOffset);

			msg.header.stamp = state_.stateTime;
			cmd.header.stamp = state_.command.commandTime;
			cmd.jointPosition = state_.command.jointPosition;
			cmd.jointStiffness = state_.command.jointStiffness;
			cmd.jointDamping = state_.command.jointDamping;
			cmd.jointTorque = state_.command.jointTorque;
			tmp.block<3,4>(0,0)=Eigen::Map<Eigen::Matrix< float,3,4,Eigen::RowMajor > >(state_.command.cartPose.data()).cast<double>();
			aff=Eigen::Affine3d(tmp);
			tf::poseEigenToMsg(aff, cmd.cartPose);
			cmd.cartStiffness=vectorToWrench(state_.command.cartStiffness);
			cmd.cartStiffness = vectorToWrench(state_.command.cartStiffness);
			cmd.cartDamping = vectorToWrench(state_.command.cartDamping);
			cmd.cartForce = vectorToWrench(state_.command.cartForce);
			cmd.cartNull = state_.command.cartNull;
			cmd.controlMode = state_.command.controlMode;
			msg.command=cmd;
			statePublisher_.publish(msg);

			jnt.header.stamp = state_.stateTime;
			jnt.position=std::vector<double>(state_.jointPosition.begin(),state_.jointPosition.end());
			jnt.velocity=std::vector<double>(state_.jointVelocity.begin(),state_.jointVelocity.end());
			jnt.effort=std::vector<double>(state_.jointTorque.begin(),state_.jointTorque.end());
			jointStatePublisher_.publish(jnt);
		}
	}
}

geometry_msgs::Wrench vectorToWrench(std::vector<float>& vec)
{
	geometry_msgs::Wrench ret;
	if(vec.size()!=6)
	{
		ROS_WARN_STREAM_THROTTLE(1,"Wrench vector has incorrect size!");
		return ret;
	}
	ret.force.x=vec[0];
	ret.force.y=vec[1];
	ret.force.z=vec[2];
	ret.torque.x=vec[3];
	ret.torque.y=vec[4];
	ret.torque.z=vec[5];
	return ret;
}

void wrenchToVector(const geometry_msgs::Wrench & wr, std::vector<float>& vec)
{
	vec.resize(6);
	vec[0]=wr.force.x;
	vec[1]=wr.force.y;
	vec[2]=wr.force.z;
	vec[3]=wr.torque.x;
	vec[4]=wr.torque.y;
	vec[5]=wr.torque.z;
}


void LWRDriver::jointPositionCommandCallback(const lwr_driver::FriCommandJointPosition& msg)
{
	boost::mutex::scoped_lock lock(commandLock_);
	command_.commandTime = msg.header.stamp;
	command_.jointPosition = msg.jointPosition;
}

void LWRDriver::jointStiffnessCommandCallback(const lwr_driver::FriCommandJointStiffness& msg)
{
	boost::mutex::scoped_lock lock(commandLock_);
	command_.commandTime = msg.header.stamp;
	command_.jointPosition = msg.jointPosition;
	command_.jointStiffness = msg.jointStiffness;
	command_.jointDamping = msg.jointDamping;
	command_.jointTorque = msg.jointTorque;
}

void LWRDriver::cartesianStiffnessCommandCallback(const lwr_driver::FriCommandCartesianStiffness& msg)
{
	boost::mutex::scoped_lock lock(commandLock_);
	command_.commandTime = msg.header.stamp;
	command_.cartNull = msg.jointPosition;
	command_.jointPosition = msg.jointPosition;
	wrenchToVector(msg.cartStiffness,command_.cartStiffness);
	wrenchToVector(msg.cartDamping,command_.cartDamping);
	wrenchToVector(msg.cartForce,command_.cartForce);
}

void LWRDriver::modeCommandCallback(const lwr_driver::FriCommandMode& msg)
{
	boost::mutex::scoped_lock lock(commandLock_);
	switch(msg.controlMode)
	{
		case 0:
			command_.commandTime = msg.header.stamp;
			command_.controlMode = FRI_OFF;
			break;
		case 10:
			command_.commandTime = msg.header.stamp;
			command_.controlMode = FRI_POSITION_CONTROL;
			break;
		case 20:
			command_.commandTime = msg.header.stamp;
			command_.controlMode = FRI_CARTESIAN_STIFFNESS;
			break;
		case 30:
			command_.commandTime = msg.header.stamp;
			command_.controlMode = FRI_JOINT_STIFNESS;
			break;
		default:
			ROS_WARN_STREAM_THROTTLE(1,"Invalid control mode commanded: "<<msg.controlMode);
			break;
	}
}

bool LWRDriver::getJointLimits(const std::string& joint_name, FRIJointLimits& limits)
{
  // Node handle scoped where the joint limits are defined
  ros::NodeHandle limits_nh;
  try
  {
    const std::string limits_namespace = "LWR_FRI/joint_limits/" + joint_name;
    if (!nh_.hasParam(limits_namespace))
    {
    	ROS_ERROR_STREAM("No joint limits specification found for joint '" << joint_name <<
                       "' in the parameter server (namespace " << nh_.getNamespace() + "/" + limits_namespace << ").");
      return false;
    }
    limits_nh = ros::NodeHandle(nh_, limits_namespace);
  }
  catch(const ros::InvalidNameException& ex)
  {
    ROS_ERROR_STREAM(ex.what());
    return false;
  }

  // Position limits
	double min_pos, max_pos;
	if (limits_nh.getParam("min_position", min_pos) && limits_nh.getParam("max_position", max_pos))
	{
		limits.positionMin = min_pos;
		limits.positionMax = max_pos;
	}
	else
	{
		ROS_ERROR_STREAM("Position limits for joint '" << joint_name << "' are not specified!");
		return false;
	}

  // Velocity limits
	double max_vel;
	if (limits_nh.getParam("max_velocity", max_vel))
	{
		limits.velocity = max_vel;
	}
	else
	{
		ROS_ERROR_STREAM("Velocity limits for joint '" << joint_name << "' are not specified!");
		return false;
	}

  // Acceleration limits
	double max_acc;
	if (limits_nh.getParam("max_acceleration", max_acc))
	{
		limits.acceleration = max_acc;
	}
	else
	{
		ROS_ERROR_STREAM("Acceleration limits for joint '" << joint_name << "' are not specified!");
		return false;
	}

  return true;
}

int main(int argc, char** argv)
{
	ros::init(argc, argv, "LWR_FRI");

	if (!ros::param::has("~port"))
	{
		ROS_ERROR_STREAM("FRI: Communication port was not specified.");
		return 1;
	}
	if (!ros::param::has("~host_hint"))
	{
		ROS_ERROR_STREAM("FRI: Hint to host was not specified.");
		return 1;
	}

  boost::shared_ptr<LWRDriver> drv(new LWRDriver());
  int port;
  std::string hint;
  ros::param::get("~port",port);
  ros::param::get("~host_hint",hint);
  if(drv->Start(port, hint))
	  ros::spin();
	drv->Stop();
	return 0;
};

