#!/usr/bin/env python
# license removed for brevity
import rospy
import math
from lwr_driver.msg import FriCommandJointPosition


def talker():
    rospy.init_node('go_zero_instant', anonymous=True)
    pub = rospy.Publisher('/lwr/commandJointPosition', FriCommandJointPosition, queue_size=10)
    rospy.loginfo("Commanding zero joint angles")
    r = rospy.Rate(100) # 100hz
    msg=FriCommandJointPosition()
    while not rospy.is_shutdown():
      msg.header.stamp=rospy.get_rostime()
      msg.jointPosition=tuple([0,0,0,0,0,0,0])
      pub.publish(msg)      
      r.sleep()

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException: pass