#!/usr/bin/env python
# license removed for brevity
import rospy
import math
from lwr_driver.msg import FriState
from lwr_driver.msg import FriCommandCartesianStiffness

def talker():
    rospy.init_node('test_cartesian', anonymous=True)
    pub = rospy.Publisher('/lwr/commandCartesianStiffness', FriCommandCartesianStiffness, queue_size=10)
    rospy.loginfo("Starting LWR Cartesian Stiffness mode test node")
    r = rospy.Rate(100) # 100hz
    msg=FriCommandCartesianStiffness()
    t=0.0
    msg.jointPosition=[1.256637006008532e-06, -0.052356038242578506, 2.5481806460447842e-06, 1.518426775932312, 4.066617293574382e-06, -0.9599856734275818, -7.157595246098936e-05]
    msg.cartStiffness.force.x=3500
    msg.cartStiffness.force.y=3500
    msg.cartStiffness.force.z=140
    msg.cartStiffness.torque.x=3500
    msg.cartStiffness.torque.y=3500
    msg.cartStiffness.torque.z=3500
    msg.cartDamping.force.x=0.7
    msg.cartDamping.force.y=0.7
    msg.cartDamping.force.z=0.2
    msg.cartDamping.torque.x=0.7
    msg.cartDamping.torque.y=0.7
    msg.cartDamping.torque.z=0.7
    while not rospy.is_shutdown():
      msg.header.stamp=rospy.get_rostime()
      pub.publish(msg)
      t=t+0.01
      if t>.05:
        break
      r.sleep()
    t=0.0  
    while not rospy.is_shutdown():
        msg.header.stamp=rospy.get_rostime()
        pub.publish(msg)
        t=t+0.01
        r.sleep()

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException: pass
