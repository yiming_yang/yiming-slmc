#!/usr/bin/env python
# license removed for brevity
import rospy
from geometry_msgs.msg import Pose
from geometry_msgs.msg import PoseStamped
from lwr_driver.msg import FriState

def callback(data):
    global pub
    p=PoseStamped()
    p.pose=data.cartPose
    p.header.stamp=data.header.stamp
    p.header.frame_id='base'
    pub.publish(p)

if __name__ == '__main__':
    global pub
    try:
        rospy.init_node('lwr_republisher', anonymous=True)
        rospy.loginfo("Starting LWR republisher")
        pub = rospy.Publisher('/lwr/cartPose', PoseStamped, queue_size=1)
        rospy.Subscriber("/kuka_lwr_state", FriState, callback)
        rospy.spin()
    except rospy.ROSInterruptException: pass