#!/usr/bin/env python
# license removed for brevity
import rospy
import math
from lwr_driver.msg import FriCommandJointPosition
from sensor_msgs.msg import JointState

def callback(data):
    global pub
    msg=FriCommandJointPosition()
    msg.header.stamp=rospy.get_rostime()
    msg.jointPosition=data.position
    pub.publish(msg)

def talker():
    global pub
    rospy.init_node('go_home', anonymous=True)
    pub = rospy.Publisher('/lwr/commandJointPosition', FriCommandJointPosition, queue_size=10)
    rospy.Subscriber("/kuka_lwr_joint_commands", JointState, callback)
    rospy.loginfo("Republishing LWR joint position commands")
    r = rospy.Rate(100)
    while not rospy.is_shutdown():      
      r.sleep()

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException: pass