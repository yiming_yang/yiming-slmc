#!/usr/bin/env python
# license removed for brevity
import rospy
import math
from lwr_driver.msg import FriCommandJointPosition

def talker():
    rospy.init_node('go_home', anonymous=True)
    pub = rospy.Publisher('/lwr/commandJointPosition', FriCommandJointPosition, queue_size=10)
    rospy.loginfo("Starting LWR homing node")
    r = rospy.Rate(100) # 100hz
    msg=FriCommandJointPosition()
    q=[1.256637006008532e-06, -0.052356038242578506, 2.5481806460447842e-06, 1.518426775932312, -0.2, -0.9599856734275818, -7.157595246098936e-05]
    while not rospy.is_shutdown():
      msg.header.stamp=rospy.get_rostime()
      msg.jointPosition=tuple(q)
      pub.publish(msg)      
      r.sleep()

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException: pass