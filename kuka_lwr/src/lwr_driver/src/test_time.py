#!/usr/bin/env python
# license removed for brevity
import rospy
import math
from lwr_driver.msg import FriState
from std_msgs.msg import Float64MultiArray

def callback(data):
    global pub
    global msg
    msg.data[0]=data.header.stamp.to_sec()
    msg.data[1]=data.command.header.stamp.to_sec()
    msg.data[2]=msg.data[0]-msg.data[1]
    msg.data[3]=rospy.get_time()-starttime
    msg.data[4]=rospy.get_time()-msg.data[0]
    pub.publish(msg)  
    
def talker():
    global pub
    global msg
    global starttime
    rospy.init_node('go_home', anonymous=True)
    state=FriState()
    msg=Float64MultiArray()
    msg.data=[0.0,0.0,0.0,0.0,0.0]
    starttime=rospy.get_time()
    pub = rospy.Publisher('/lwr/timing', Float64MultiArray, queue_size=10)
    rospy.Subscriber("/kuka_lwr_state", FriState, callback)
    rospy.loginfo("Starting LWR timing test node")

    rospy.spin()
    
if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException: pass