#include <std_msgs/Float64MultiArray.h>
#include <baxter_core_msgs/JointCommand.h>
#include <ros/ros.h>
class baxter_teleop
{
	public:
		baxter_teleop()
		{
			pub_left_=nh_.advertise<baxter_core_msgs::JointCommand>("/robot/limb/left/joint_command",1);
			pub_right_=nh_.advertise<baxter_core_msgs::JointCommand>("/robot/limb/right/joint_command",1);
			sub_=nh_.subscribe<std_msgs::Float64MultiArray>("imesh_joint_cmd", 1,boost::bind(&baxter_teleop::poseCallback, this, _1));
			names_={"left_s0", "left_s1", "left_e0", "left_e1", "left_w0", "left_w1", "left_w2",
		"right_s0", "right_s1", "right_e0", "right_e1", "right_w0", "right_w1", "right_w2"};
		}
	private:
		void poseCallback(const std_msgs::Float64MultiArray::ConstPtr & pose)
		{
			cmd.mode=1;
			cmd.names.resize(7);
			cmd.command.resize(7);			
			uint i;			
			for(i=0;i<7;i++)
			{
				cmd.command[i]=pose->data[i];
				cmd.names[i]=names_[i];
			}		
			pub_left_.publish(cmd);	
			
			for(i=0;i<7;i++)
			{
				cmd.command[i]=pose->data[i+7];
				cmd.names[i]=names_[i+7];
			}
			pub_right_.publish(cmd);
		}
		std::vector<std::string> names_;
		baxter_core_msgs::JointCommand cmd;
		ros::NodeHandle nh_;
		ros::Publisher pub_left_;
		ros::Publisher pub_right_;
		ros::Subscriber sub_;
		
		
};
int main(int argc, char **argv)
{
	ros::init(argc, argv, "baxter_teleop");
	baxter_teleop teleop;
	ros::spin();
	return 0;
}
