#include <baxter_core_msgs/JointCommand.h>
#include <ros/ros.h>
#include <vector>

/// This class spawns a node that reads a trajecotry from a text file then publishes this trajectory as commands to the robot. The playback will loop.

class baxter_rlsc
{
public:
  /// Constructor
  baxter_rlsc(): looprate(10),T(0),loaded(false)
  {
    pub_left_=nh_.advertise<baxter_core_msgs::JointCommand>("/robot/limb/left/joint_command",1);
    pub_right_=nh_.advertise<baxter_core_msgs::JointCommand>("/robot/limb/right/joint_command",1);
    names_={"right_s0", "right_s1", "right_e0", "right_e1", "right_w0", "right_w1", "right_w2","left_s0", "left_s1", "left_e0", "left_e1", "left_w0", "left_w1", "left_w2"};
    trajectory=std::vector<double>();
  }

  /*! \brief Loads the data from a text file
    \param filename Path to the file
  */
  void load(const char* filename)
  {
    loaded=false;
    trajectory.clear();
    FILE *fp=NULL;
    fp=fopen(filename, "r");
    if(!fp) { std::cerr << "Can not open file '" << filename << "'!\n"; return;}
    double rate;
    if(fscanf(fp,"%le",&rate)!=1) {fclose(fp);std::cerr << "Can not load update rate!\n";return;}
    
    double data[18];
    while(fscanf(fp,"%le %le %le %le %le %le %le %le %le %le %le %le %le %le %le %le %le %le\n",&data[0],&data[1],&data[2],&data[3],&data[4],&data[5],&data[6],&data[7],&data[8],&data[9],&data[10],&data[11],&data[12],&data[13],&data[14],&data[15],&data[16],&data[17])==18)
    {
      for(int i=0;i<7;i++) trajectory.push_back(data[i]);
      for(int i=0;i<7;i++) trajectory.push_back(data[i+9]);
    }
    fclose(fp);
    std::cerr << "Trajectory length is " << trajectory.size()/14 << "\n";
    if(trajectory.size()==0) return;
    std::cerr << "Setting update rate " << rate << "Hz!\n";
    looprate=ros::Rate(rate);
    T=trajectory.size()/14;
    loaded=true;
  }

  /// \brief Runs the main loop and plays the trajectory commanding the robot.
  void run()
  {
    if(loaded)
    {
      
      cmdl.mode=1;
      cmdl.names.resize(7);
      cmdl.command.resize(7);			
      cmdr.mode=1;
      cmdr.names.resize(7);
      cmdr.command.resize(7);	
      int j=0;
      while(ros::ok())
      {
        for(int i=0;i<7;i++)
        {
          cmdr.command[i]=trajectory[j*14+i];
          cmdr.names[i]=names_[i];
        }		
        pub_right_.publish(cmdr);	

        for(int i=0;i<7;i++)
        {
          cmdl.command[i]=trajectory[j*14+i+7];
          cmdl.names[i]=names_[i+7];
        }
        pub_left_.publish(cmdl);
        std::cerr << "Commanding time step " << j << "\n"; 
        if(j==0)
        {
          std::cerr << "Waiting 8 senconds to start!\n";
          for(int i=0;i<8.0/(double)looprate.expectedCycleTime().toSec() && ros::ok();i++){pub_left_.publish(cmdl);pub_right_.publish(cmdr);ros::spinOnce();looprate.sleep();}
        }
        j++;
        if(j>=T) j=0;
        ros::spinOnce();
        looprate.sleep();
      }
    }
    else
    {
       std::cerr << "Trajectory was not loaded!\n";
    }
  }
  
private:
  std::vector<std::string> names_; //!< \brief Names of joints that are being controlled
  baxter_core_msgs::JointCommand cmdl; //!< \brief Command to the left arm
  baxter_core_msgs::JointCommand cmdr; //!< \brief Command to the right arm
  ros::NodeHandle nh_; //!< \brief Ros node handle
  ros::Publisher pub_left_; //!< \brief Command publisher for the left arm
  ros::Publisher pub_right_; //!< \brief Command publisher for the right arm
  std::vector<double> trajectory; //!< \brief Trajectory of joint angles ordered as: 7 joints of the right and left arm respectively, repeated $T$ times
  int T; //!< \brief Number of time steps stored in the trajectory
  bool loaded; //!< \brief Flag to indicate if the trajectory has been loaded
  ros::Rate looprate; //!< \brief Update rate
};


int main(int argc, char **argv)
{
  ros::init(argc, argv, "baxter_rlsc");
  std::string filename("trajectory.txt");
  ros::NodeHandle priv_nh("~");
  priv_nh.getParam("data", filename);
  baxter_rlsc bax;
  bax.load(filename.c_str());
  bax.run();
  return 0;
}
