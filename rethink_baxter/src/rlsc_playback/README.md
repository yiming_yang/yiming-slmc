## RLSC course trajectory playback utility
-----------------

This package spawns a node that reads a trajecotry from a text file then publishes this trajectory as commands to the robot. The playback will loop.

To run:
    1. rosrun rlsc_playback rlsc _data:="path_to_trajectory_file"
