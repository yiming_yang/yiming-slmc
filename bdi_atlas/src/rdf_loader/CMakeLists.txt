cmake_minimum_required(VERSION 2.8.3)
project(rdf_loader)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++0x")

find_package(catkin REQUIRED COMPONENTS
  moveit_core
  #moveit_ros_perception
  #pluginlib
  actionlib
  roscpp
  rosconsole
  #dynamic_reconfigure
  #message_filters
  srdfdom
  urdf
  tf
  tf_conversions
)


find_package(Qt4 COMPONENTS QtCore QtGui REQUIRED)

include(${QT_USE_FILE})


## System dependencies are found with CMake's conventions
find_package(Boost REQUIRED COMPONENTS signals)

## System dependencies are found with CMake's conventions
find_package(EIGEN REQUIRED)

## catkin specific configuration ##
catkin_package(
  INCLUDE_DIRS include
  CATKIN_DEPENDS moveit_core
  LIBRARIES ${PROJECT_NAME}
  DEPENDS eigen system_lib boost Qt4
)

## Build ##
include_directories(
  rdf_loader/include
  include
  SYSTEM
  ${catkin_INCLUDE_DIRS}
  ${EIGEN_INCLUDE_DIRS}
  ${Boost_INCLUDE_DIRS}
)

set(MOVEIT_LIB_NAME moveit_rdf_loader)

add_library(${MOVEIT_LIB_NAME} src/rdf_loader.cpp)
target_link_libraries(${MOVEIT_LIB_NAME} ${catkin_LIBRARIES})

install(TARGETS ${MOVEIT_LIB_NAME} LIBRARY DESTINATION lib)
install(DIRECTORY include/ DESTINATION include)
