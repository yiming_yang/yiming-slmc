#include <gtest/gtest.h>
#include <ros/ros.h>

#include <atlas_conversions/joint_names.h>


TEST(ShouldPass, expectJointNamesSizeEqual)
{
  EXPECT_EQ(AtlasJointNames.size(), atlas::numJoints());
  EXPECT_EQ(AtlasJointNamesStr.size(), atlas::numJoints());
}

int main(int argc, char** argv)
{
	::testing::InitGoogleTest(&argc, argv);

	ros::init(argc, argv, "test_default_init");
  
	return RUN_ALL_TESTS();
}
