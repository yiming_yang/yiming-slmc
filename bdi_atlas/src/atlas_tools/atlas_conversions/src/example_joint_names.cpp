/*
 * example_joint_names.cpp
 *
 *  Created on: 11 Oct 2013
 *      Author: Vladimir Ivan
 */

#include <iostream>
#include <atlas_conversions/joint_names.h>
#include <iomanip>

int main(int argc, char **argv)
{
  // Using the latest joints does not require changing anything
  std::cout << "Number of joints:" << NJoints << "\n";
  std::cout << "Joint names (latest):\n";
  for (int i = 0; i < NJoints; i++) {
    std::cout << std::setw(2) << i << " '" << atlas::jointNameFromIdx(i) << "'\n";
  }

  // Run code dependent on joint names here

  // Request v2 joints
  atlas::initJointNames(2);
  std::cout << "Number of joints: " << NJoints << "\n";
  std::cout << "Joint names (v2):\n";
  for (int i = 0; i < NJoints; i++) {
    std::cout << std::setw(2) << i << " '" << atlas::jointNameFromIdx(i) << "'\n";
  }

  // Run code dependent on v2 joint names here

  // Request v3 joints
  atlas::initJointNames(3);
  std::cout << "Number of joints: " << NJoints << "\n";
  std::cout << "Joint names (v3):\n";
  for (int i = 0; i < NJoints; i++) {
    std::cout << std::setw(2) << i << " '" << atlas::jointNameFromIdx(i) << "'\n";
  }

  // Run code dependent on v3 joint names here

  // Request v3 joints and print everything, including Sandia hand and Hokuyo
  atlas::initJointNames(3);
  std::cout << "Number of joints: " << atlas::numJoints() << "\n";
  std::cout << "Joint names (latest with Sandia hands and Hokuyo):\n";
  for (int i = 0; i < atlas::numJoints(); i++) {
    std::cout << std::setw(2) << i << " '" << atlas::jointNameFromIdx(i) << "'\n";
  }

  // Run code dependent on joint names here

  return 0;
}
