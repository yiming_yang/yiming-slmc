/*
 * kdl_joint_mapper.cpp
 *
 *  Created on: 11 Oct 2013
 *      Author: Vladimir Ivan
 */

#include <atlas_conversions/joint_names.h>
#include <atlas_conversions/kdl_joint_mapper.h>
#include <ros/ros.h>

JointMapper* JointMapper::instance = NULL;

JointMapper::JointMapper()
{

}

JointMapper::~JointMapper()
{

}

JointMapper* JointMapper::getInstance()
{
  if(JointMapper::instance==NULL)
  {
    JointMapper::instance = new JointMapper();
  }
  return JointMapper::instance;
}

int JointMapper::GetNJoints()
{
  return NJoints;
}

int JointMapper::GetJointMap(std::vector< int >& jointMap_, KDL::Chain& chain)
{
  jointMap_.resize(chain.getNrOfJoints(),-1);
  int k=0;
  for (int j=0;j<(int)chain.segments.size();j++)
  {
    if(chain.segments[j].getJoint().getType()!=KDL::Joint::None)
    {
      int i;
      bool found=false;
      for(i=0;i<NJoints;i++)
      {
        if(chain.segments[j].getJoint().getName().compare(atlas::jointNameFromIdx(i))==0)
        {
          found=true;
          jointMap_[k]=i;
          k++;
          break;
        }
      }
      if(!found)
      {
        ROS_ERROR("KDL tree: Could not find joint '%s' in the KDL tree. Possible robot model mismatch?",chain.segments[j].getJoint().getName().c_str());
        return k+1;
      }
    }
  }
  return 0;
}

int JointMapper::GetJointMap(std::vector< int >& jointMap_, KDL::Tree& tree)
{
  jointMap_.resize(atlas::numJoints(), -1);
  KDL::SegmentMap segments = tree.getSegments();
  for(int i=0;i<NJoints;i++)
  {
    bool found=false;
    for (KDL::SegmentMap::const_iterator iter = segments.begin(); iter!=segments.end(); iter++)
    {
      if(iter->second.segment.getJoint().getName().compare(atlas::jointNameFromIdx(i))==0)
      {
        found=true;
        jointMap_[i]=iter->second.q_nr;
        break;
      }
    }
    if(!found)
    {
      ROS_ERROR("KDL tree: Could not find joint '%s' in the KDL tree. Possible robot model mismatch?",atlas::jointNameFromIdx(i).c_str());
      return i;
    }
  }
  return 0;
}
