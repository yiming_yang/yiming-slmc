#include <atlas_conversions/joint_names.h>

#include <vector>
#include <map>

namespace {

/// \brief This class initializes the joint names to default values on runtime.
class DummyJointNameInitializer { public: DummyJointNameInitializer(); };

/// \brief Internal array of all joint names (including all versions)
const char* g_atlas_joint_names[] = {
/// @cond
#define NAME_ENTRY(NAME, ID, VER, GROUP) #NAME,
  ATLAS_JOINT_NAMES_MAP(NAME_ENTRY)
#undef NAME_ENTRY
/// @endcond
};

/// \brief Internal array of joint indices for all joints (including all versions)
int g_atlas_joint_indices[] = {
/// @cond
#define NAME_ENTRY(NAME, ID, VER, GROUP) ID,
  ATLAS_JOINT_NAMES_MAP(NAME_ENTRY)
#undef NAME_ENTRY
/// @endcond
};

/// \brief Internal array of model versions for all joints (including all versions)
double g_atlas_joint_versions[] = {
/// @cond
#define NAME_ENTRY(NAME, ID, VER, GROUP) VER,
  ATLAS_JOINT_NAMES_MAP(NAME_ENTRY)
#undef NAME_ENTRY
/// @endcond
};

#if 0 // TODO should this be used anywhere?
/// \brief Internal array of joint groups for all joints (including all versions)
RobotJointGroup g_atlas_joint_groups[] = {
/// @cond
#define NAME_ENTRY(NAME, ID, VER_MAJ, VER_MIN, GROUP) GROUP,
  ATLAS_JOINT_NAMES_MAP(NAME_ENTRY)
#undef NAME_ENTRY
/// @endcond
};
#endif

/**
 * A vector of the Atlas joint names (as std::string) for joint indices.
 */
std::vector<std::string> g_atlas_joint_names_str;

/**
 * A map from the Atlas joint names (as std::string) to joint indices.
 */
std::map<std::string, int> g_atlas_joint_idx_from_name;

DummyJointNameInitializer::DummyJointNameInitializer()
{
  atlas::initJointNames();
  
  // TODO remove
  InitJointNames(-1.0);
}

} // namespace

void atlas::initJointNames(int version)
{
  int const nJointsInternal = sizeof(g_atlas_joint_indices)/sizeof(int);
  
  int tmpNJoints = 0;
  for (int i = 0; i < nJointsInternal; i++)
  {
    // Joints in internal representation are listed in version order.
    // Keep accepting joints as long as the version is one we accept.
    if (g_atlas_joint_indices[i] > tmpNJoints
     && (g_atlas_joint_versions[i] <= version || version == -1))
    {
      tmpNJoints = g_atlas_joint_indices[i];
    }
  }
  
  tmpNJoints++;
  g_atlas_joint_names_str.resize(tmpNJoints);
  std::vector<int> ver(tmpNJoints, -1);
  
  for (int i = 0; i < nJointsInternal; i++)
  {
    int vcur = ver[g_atlas_joint_indices[i]];
    int vnew = g_atlas_joint_versions[i];
    if ((vnew == version || (vnew > vcur && vnew < version) || (vnew > vcur && version == -1)) )
    {
      int idx = g_atlas_joint_indices[i];
      std::string name = g_atlas_joint_names[i];
      
      g_atlas_joint_names_str[idx] = name;
      g_atlas_joint_idx_from_name[name] = idx;
      ver[g_atlas_joint_indices[i]] = vnew;
    }
  }
}

std::string atlas::jointNameFromIdx(int idx)
{
  if (idx < 0 || idx >= numJoints())
  {
    return "ERROR";
  }
  return g_atlas_joint_names_str[idx];
}

int atlas::jointIdxFromName(std::string const& name)
{
  if (g_atlas_joint_idx_from_name.find(name) == g_atlas_joint_idx_from_name.end())
  {
    return -1;
  }
  
  return g_atlas_joint_idx_from_name[name];
}

int atlas::numJoints()
{
  return (int)g_atlas_joint_names_str.size();
}


////////////////////////////////////////////////////////////////////////////////
// DEPRECATED
////////////////////////////////////////////////////////////////////////////////

const char* AtlasJointNamesInternal[] = {
/// @cond
#define NAME_ENTRY(NAME, ID, VER, GROUP) #NAME,
  ATLAS_JOINT_NAMES_MAP(NAME_ENTRY)
#undef NAME_ENTRY
/// @endcond
};

int AtlasJointIndicesInternal[] = {
/// @cond
#define NAME_ENTRY(NAME, ID, VER, GROUP) ID,
  ATLAS_JOINT_NAMES_MAP(NAME_ENTRY)
#undef NAME_ENTRY
/// @endcond
};

double AtlasJointVersionsInternal[] = {
/// @cond
#define NAME_ENTRY(NAME, ID, VER, GROUP) VER,
  ATLAS_JOINT_NAMES_MAP(NAME_ENTRY)
#undef NAME_ENTRY
/// @endcond
};

RobotJointGroup AtlasJointGroupsInternal[] = {
/// @cond
#define NAME_ENTRY(NAME, ID, VER, GROUP) GROUP,
  ATLAS_JOINT_NAMES_MAP(NAME_ENTRY)
#undef NAME_ENTRY
/// @endcond
};

std::vector<const char*> AtlasJointNames;
std::vector<std::string> AtlasJointNamesStr;

void InitJointNames(double Version)
{
  int NJointsInternal = sizeof(AtlasJointIndicesInternal)/sizeof(int);
  int tmpNJoints=0;
  double vcur, vnew;
  for(int i=0;i<(int)NJointsInternal;i++)
  {
    if(AtlasJointIndicesInternal[i]>tmpNJoints && (AtlasJointVersionsInternal[i]<=Version || Version==-1.0))
      tmpNJoints=AtlasJointIndicesInternal[i];
  }
  tmpNJoints++;
  AtlasJointNamesStr.resize(tmpNJoints);
  AtlasJointNames.resize(tmpNJoints);
  std::vector<double> ver(tmpNJoints,-1.0);
  for(int i=0;i<(int)NJointsInternal;i++)
  {
    vcur=ver[AtlasJointIndicesInternal[i]];
    vnew=AtlasJointVersionsInternal[i];
    if((vnew==Version || (vnew>vcur && vnew<Version) || (vnew>vcur && Version==-1.0)) )
    {
      AtlasJointNamesStr[AtlasJointIndicesInternal[i]]=AtlasJointNamesInternal[i];
      ver[AtlasJointIndicesInternal[i]]=vnew;
    }
  }
  for(int i=0;i<(int)tmpNJoints;i++)
  {
    AtlasJointNames[i]=AtlasJointNamesStr[i].c_str();
  }
}

////////////////////////////////////////////////////////////////////////////////
// END DEPRECATED
////////////////////////////////////////////////////////////////////////////////

// HACK Goes at the end to ensure the Init call gets invoked after std::vectors
// have run their own static initialization
namespace { DummyJointNameInitializer dummyDummDummyVonDummerhauser; }