#ifndef LLJC_TESTING_CLASS_H_
#define LLJC_TESTING_CLASS_H_

#include <ros/ros.h>
#include <atlas_msgs/AtlasState.h>
#include <atlas_msgs/AtlasCommand.h>
#include <atlas_conversions/joint_names.h>
#include <hku_msgs/lowLevelJointController.h>
#include <hku_msgs/BehaviorRegData.h>
#include <hku_msgs/AvailableBehaviors.h>
#include <hku_msgs/CurrentBehavior.h>
#include <math.h>
#include <hku_msgs/RegisterBehavior.h>
#include <lljc/SwitchBehavior.h>
#include <lljc/register.h>
#include <yaml-cpp/yaml.h>
#include <ros/package.h>
#include <boost/filesystem.hpp>

class LLJCTests
{
public:
    LLJCTests();
    LLJCTests(ros::NodeHandle* nodehandle,
              ros::NodeHandle* priv_nodehandle,
              const hku_msgs::BehaviorRegData* reg_data):
        nh_(*nodehandle),
        priv_nh_(*priv_nodehandle),
        looprate_hz_(100),
        looprate_(looprate_hz_),
        node_reg_data_(*reg_data)
    {
        switch_client_ = nh_.serviceClient<lljc::SwitchBehavior>("lljc/switch_behavior");
        initializeSubscribers();
    }

    virtual ~LLJCTests(void) {}
    int run()
    {
        if(!initializePublishers())
        {
            ROS_ERROR("Failed to initialize Publishers");
            return 1;
        }

        if(!initializeTestParams())
        {
            ROS_ERROR("Failed to initialize Test Parameters");
            return 1;
        }

        while(ros::ok())
        {
            switchBehaviors();
            moveJointsToPrepPose();
            runJointSineTest();
            releaseBehavior();
            for(int i = 0; i < 5; i++)
            {
                looprate_.sleep();
                ros::spinOnce();
            }
        }
        return 0;
    }

private:
    struct TestParams
    {
        double home_pose[NJoints];
        double prep_pose[NJoints];
        double amplitude;
        double frequency;
        int cycles;
        int test_joint;
    };
    ros::NodeHandle nh_;
    ros::NodeHandle priv_nh_;
    int looprate_hz_;
    ros::Rate looprate_;
    hku_msgs::BehaviorRegData node_reg_data_;
    std::string current_behavior_;
    int node_behavior_idx_;

    hku_msgs::lowLevelJointController last_command_;
    hku_msgs::lowLevelJointController test_cmd_;

    double reported_joint_angles_[NJoints];

    TestParams test_params_;

    lljc::SwitchBehavior switch_srv_;

    ros::Publisher joint_cmd_pub_;
    ros::Subscriber last_cmd_sub_;
    ros::Subscriber current_behavior_sub_;
    ros::Subscriber atlas_state_sub_;
    ros::ServiceClient switch_client_;


    bool initializePublishers()
    {
        lljc::BehaviorData behavior_data = lljc::register_behavior(&node_reg_data_);

        if(!behavior_data.is_registered)
        {
            ROS_ERROR("Failed to initialize Publishers");
            return false;
        }

        node_behavior_idx_ = behavior_data.behavior_id;
        joint_cmd_pub_ = nh_.advertise<hku_msgs::lowLevelJointController>(behavior_data.topic, 1, true);
        return true;
    }

    void atlasStateCB(const atlas_msgs::AtlasState& atlas_state_msg_holder)
    {
        for(int i = 0; i < NJoints; i++)
        {
            reported_joint_angles_[i] = atlas_state_msg_holder.position[i];
        }
    }

    void currentBehaviorCB(const hku_msgs::CurrentBehavior& behavior_msg_holder)
    {
        current_behavior_ = behavior_msg_holder.behavior_name;
    }

    void lastCommandCB(const hku_msgs::lowLevelJointController& cmd_msg_holder)
    {
        last_command_ = cmd_msg_holder;
    }

    void initializeSubscribers()
    {
        ROS_INFO("Initializing Subscriber to joint states");
        atlas_state_sub_ = nh_.subscribe("atlas/atlas_state", 1, &LLJCTests::atlasStateCB, this);

        ROS_INFO("Initializing Subscriber to current behavior");
        current_behavior_sub_ = nh_.subscribe("lljc/current_behavior", 1, &LLJCTests::currentBehaviorCB, this);

        ROS_INFO("Initializing Subscriber to last command");
        last_cmd_sub_ = nh_.subscribe("lljc/last_cmd", 1, &LLJCTests::lastCommandCB, this);
    }

    void releaseBehavior()
    {
        if(current_behavior_ == node_reg_data_.behavior_name)
        {
            callSwitch(-1); //Return to halt behavior
        }
    }

    void switchBehaviors()
    {
        reported_joint_angles_[0] = -1000.0;
        last_command_.jntAngles[0] = -1000.0;
        //wait for something at least semi-plausible to be published for joint-angle 0.
        while (ros::ok() && ((reported_joint_angles_[0]< -10.0)||(reported_joint_angles_[0]> 10.0)) && ((last_command_.jntAngles[0] < -10) || (last_command_.jntAngles[0] > 10)))
        {
            looprate_.sleep();
            ros::spinOnce();
        }


        while (ros::ok() && (current_behavior_ != "halt"))
        {
            looprate_.sleep();
            ros::spinOnce();
        }
        callSwitch(node_behavior_idx_);
    }

    void callSwitch(int idx)
    {
        switch_srv_.request.behavior_idx = idx;
        bool is_switched = false;

        switch_client_.call(switch_srv_);
        is_switched = switch_srv_.response.is_switched;

        if(!is_switched)
        {
            ROS_ERROR("Didn't switch");
        }
    }

    void moveJointsToPrepPose()
    {
        for (int i = 0; i < NJoints; i++)
        {
            test_cmd_.jntAngles[i] = last_command_.jntAngles[i];
            test_cmd_.jntVelocities[i] = last_command_.jntVelocities[i];
            test_cmd_.jntEfforts[i] = last_command_.jntEfforts[i];
            test_cmd_.k_effort[i] = last_command_.k_effort[i];
        }
        joint_cmd_pub_.publish(test_cmd_);

        double joint_increment[NJoints];
        for (int i = 0; i < NJoints; i++)
        {
            joint_increment[i] = (test_params_.prep_pose[i] - test_cmd_.jntAngles[i]) / 700.0;
            test_cmd_.jntVelocities[i] = 0;
            test_cmd_.jntEfforts[i] = 0;
            test_cmd_.k_effort[i] = 255;
        }

        for (int i = 0; i < 700; i++)
        {
            for (int i = 0; i < NJoints; i++)
            {
                test_cmd_.jntAngles[i] += joint_increment[i];
            }
            joint_cmd_pub_.publish(test_cmd_);
            looprate_.sleep();
            ros::spinOnce();
        }
    }

    void runJointSineTest()
    {
        double amplitude = test_params_.amplitude;
        int idx = test_params_.test_joint;
        double offset = test_params_.prep_pose[idx];
        double freq = test_params_.frequency;
        int nCycles = test_params_.cycles;


        double phase = 0.0;
        double final_phase = 2.0 * M_PI * nCycles;
        double omega = 2.0 * M_PI * freq;
        double dt = 1.0 / looprate_hz_;
        double dphase = omega * dt;

        double qdes = 0.0;
        double qdot_des = 0.0;


        // here to run the prescribed test:
        if (nCycles > 0)
        {
            //SPECIALIZE THE FOLLOWING TRAJECTORY GENERATION FOR MIRRORED-SYMMETRIC ELBOW MOTIONS
            while (phase < final_phase)
            {
                qdes = offset + amplitude * sin(phase); // here is a sinusoid, offset by "offset"
                qdot_des = amplitude * omega * cos(phase); // here is the corresponding velocity

                test_cmd_.jntAngles[idx] = qdes;
                test_cmd_.jntAngles[idx + 6] = -qdes;
                test_cmd_.jntVelocities[idx] = qdot_des;
                test_cmd_.jntVelocities[idx + 6] = -qdot_des;

                // send out command and sleep:
                joint_cmd_pub_.publish(test_cmd_); // send out joint commands to Atlas
                phase += dphase;
                looprate_.sleep();
                ros::spinOnce();
            }

        }

        double joint_increment[NJoints];
        for (int i = 0; i < NJoints; i++)
        {
            test_cmd_.jntAngles[i] = last_command_.jntAngles[i];
            joint_increment[i] = (test_params_.home_pose[i] - test_cmd_.jntAngles[i]) / 700.0;
            test_cmd_.jntVelocities[i] = 0;
            test_cmd_.jntEfforts[i] = 0;
            test_cmd_.k_effort[i] = 255;
        }

        for (int i = 0; i < 700; i++)
        {
            for (int i = 0; i < NJoints; i++)
            {
                test_cmd_.jntAngles[i] += joint_increment[i];
            }
            joint_cmd_pub_.publish(test_cmd_);
            looprate_.sleep();
            ros::spinOnce();
        }
    }

    bool initializeTestParams()
    {
        std::string test_file("elx_sin.yaml");
        priv_nh_.getParam("test_file", test_file);
        std::string file_path = test_file;

        if(!boost::filesystem::exists(test_file))
        {
            std::string package_path = ros::package::getPath("low_level_joint_controller");
            file_path = package_path + "/tests/config/" + test_file;
        }

        YAML::Node test_node;

        try
        {
            test_node = YAML::LoadFile(file_path);
        }
        catch (YAML::BadFile& e)
        {
            ROS_ERROR("ERROR: Bad YAML file. Couldn't read test parameters from YAML file. Check to make sure the YAML file exists and is in /low_level_joint_controller/tests/config/ ");
            return false;
        }

        try
        {
            test_params_.cycles = test_node["cycles"].as<int>();
            test_params_.test_joint = test_node["joint_idx"].as<int>();
        }
        catch (YAML::TypedBadConversion<int>& e)
        {
            ROS_ERROR("Malformed YAML file: Please make sure params cycles and joint_idx test file conforms to example file");
            return false;
        }

        try
        {
            test_params_.amplitude = test_node["amplitude"].as<double>();
            test_params_.frequency = test_node["frequency"].as<double>();

            for (int i = 0; i < NJoints; i++)
            {
                test_params_.home_pose[i] = test_node["home_pose"][i].as<double>();
                test_params_.prep_pose[i] = test_node["test_pose"][i].as<double>();
            }
        }
        catch (YAML::TypedBadConversion<double>& e)
        {
            ROS_ERROR("Malformed YAML file: Please make sure test file conforms to example file");
            return false;
        }

        return true;
    }
};


#endif
