#include <ros/ros.h>
#include <gtest/gtest.h>
#include <low_level_joint_controller.h>
#include <ros_gtest/test_macros.h>

TEST(TestSuite, testCase)
{
    EXPECT_TRUE(true);
    
    EXPECT_EQ(1, 1);
    
    EXPECT_FALSE(true);
    
    EXPECT_GT(2, 4);
    
    FAIL();
}

TEST(RosSuite, MsgPublished)
{
    EXPECT_ROS_PUBLISH(atlas_msgs::AtlasCommand, "atlas/atlas_command", 2);
}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "low_level_joint_controller_test");
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
