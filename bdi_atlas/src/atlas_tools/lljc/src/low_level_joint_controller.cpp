/**  low_level_joint_controller implementation file*/
#include <hku_msgs/lowLevelJointController.h>

#include <lljc/low_level_joint_controller.h>

bool LowLevelJointController::reportCommandCB(lljc::low_level_controller_last_command::Request& req, lljc::low_level_controller_last_command::Response& res)
{
    ROS_WARN("Report Command Service is deprecated. Its replacement is the lljc_last_cmd topic");
    res.cmd = last_command_;
    return true;
}

bool LowLevelJointController::registerBehaviorCB(hku_msgs::RegisterBehavior::Request& req,
                                                 hku_msgs::RegisterBehavior::Response& res)
{
    for(unsigned int i = 0; i < behavior_data_.size(); i++)
    {
        if(behavior_data_[i].behavior_reg_data.behavior_name == req.reg_data.behavior_name)
        {
                ROS_INFO("Node %s already exists.", req.reg_data.behavior_name.c_str());
                res.topic = behavior_data_[i].topic;
                res.id = behavior_data_[i].id;
                return true;
        }
    }

    ROS_INFO("Received a request to register node %s", req.reg_data.behavior_name.c_str());
    RegData new_data;
    new_data.behavior_reg_data = req.reg_data;
    int temp_id(behavior_data_.size());
    new_data.id = temp_id;
    res.id = temp_id;
    res.topic = "lljc/" + req.reg_data.behavior_name + "_cmd";
    new_data.topic = res.topic;

    if(req.reg_data.type == hku_msgs::BehaviorRegData::ATLAS_COMMAND)
    {
        new_data.reg_topic_sub = nh_.subscribe(res.topic,
                                                    1,
                                                    boost::function<void(const boost::shared_ptr<const atlas_msgs::AtlasCommand>&)>(boost::bind(&LowLevelJointController::atlasJointCmdVecCB, this, _1, temp_id)),
                                                    ros::VoidConstPtr(),
                                                    ros::TransportHints().reliable().tcpNoDelay());
    }
    else
    {
        new_data.reg_topic_sub = nh_.subscribe(res.topic,
                                                    1,
                                                    boost::function<void(const boost::shared_ptr<const hku_msgs::lowLevelJointController>&)>(boost::bind(&LowLevelJointController::jointCmdVecCB, this, _1, temp_id)),
                                                    ros::VoidConstPtr(),
                                                    ros::TransportHints().reliable().tcpNoDelay());
    }

    behavior_data_.push_back(new_data);
    available_behaviors_.behaviors.push_back(new_data.behavior_reg_data);
    behavior_list_pub_.publish(available_behaviors_);

    return true;
}

std::string LowLevelJointController::getBehaviorName(int idx) const
{
    std::string behavior_name = "ERROR";
    if (idx == HALT)
    {
        behavior_name = "halt";
    }
    if (idx >= 0 && idx < (int)behavior_data_.size())
    {
        behavior_name = behavior_data_[idx].behavior_reg_data.behavior_name;
    }
    return behavior_name;
}

bool LowLevelJointController::switchBehaviorCB(lljc::SwitchBehavior::Request& req, lljc::SwitchBehavior::Response& res)
{
    is_switching_ = true;
    if (req.behavior_idx == current_behavior_idx_)
    {
        ROS_INFO("LLJC is already executing behavior: %s", getBehaviorName(current_behavior_idx_).c_str());
        res.is_switched = true;
        return true;
    }

    if(req.behavior_idx < HALT || req.behavior_idx >= (int)behavior_data_.size())
    {
        ROS_WARN("Received request to switch to behavior that doesn't exist");
        res.is_switched = false;
        return false;
    }

    current_behavior_idx_ = req.behavior_idx;
    ROS_INFO("Requested behavior is registered. Service request to switch to behavior %s granted", getBehaviorName(current_behavior_idx_).c_str());
    res.is_switched = true;

    current_behavior_.behavior_name = getBehaviorName(current_behavior_idx_);
    //If we have a halt state, what is the purpose of halting a behavior? Should we remove the is_halted field?
    if(req.behavior_idx == HALT)
    {
        current_behavior_.is_halted = true;
    }
    else
    {
        current_behavior_.is_halted = false;
    }
    current_behavior_pub_.publish(current_behavior_);
    return true;
}

void LowLevelJointController::gravityTorquesCB(const hku_msgs::vectorOfDoublesWHeaderConstPtr& message_holder)
{
    gravity_torques_ = message_holder;
}

void LowLevelJointController::atlasStateCB(const atlas_msgs::AtlasState& js)
{
    jointStatesMsgToAPI(js, reported_state_);
    received_joints_angles_ = true;
    lljc_heartbeat_pub_.publish(num_iter_); //Neal - legacy feature, will be deprecated (heartbeat--shows low-level ctlr is alive)
}

void LowLevelJointController::setBDIInfoCB(const hku_msgs::BDIBehaviorInfo& bdi_info)
{
    bool user_control = true;
    for(int i = 0; i < NJoints; i++)
    {
        user_control = bdi_info.is_joint_user_controlled[i];
        if(!user_control)
        {
            valid_cmds_.pos[i] = reported_state_.pos[i];
            valid_cmds_.vel[i] = 0;
            valid_cmds_.effort[i] = 0;
            valid_cmds_.k_effort[i] = 0;
        }
        is_joint_user_controlled_[i] = user_control;
    }
    received_BD_info_ = true;
}

void LowLevelJointController::atlasJointCmdVecCB(boost::shared_ptr<const atlas_msgs::AtlasCommand> const& atlas_cmd_msg, int id)
{
    ROS_ASSERT(id >= 0);

    if(current_behavior_idx_ != id)
        return;

    if(!jointCommandsMsgToAPI(atlas_cmd_msg, received_cmds_))
    {
        return;
    }

    if(validateJointCommand(received_cmds_))
    {
        valid_cmds_ = received_cmds_;
    }
}


void LowLevelJointController::jointCmdVecCB(boost::shared_ptr<const hku_msgs::lowLevelJointController> const& lljc_cmd_msg, int id)
{
    ROS_ASSERT(id >= 0);

    if(current_behavior_idx_ != id)
    {
        return;
    }

    if(!jointCommandsMsgToAPI(lljc_cmd_msg, received_cmds_))
    {
        return;
    }

    if(validateJointCommand(received_cmds_))
    {
        valid_cmds_ = received_cmds_;
    }
}

LowLevelJointController::LowLevelJointController(ros::NodeHandle* nodehandle,
                                                 //ros::NodeHandle* lljc_nodehandle,
                                                 ros::NodeHandle* priv_nodehandle) :
        current_behavior_idx_(HALT),
        lljc_looprate_(1000),
        nh_(*nodehandle),
        priv_nh_(*priv_nodehandle),
        is_switching_(false),
        failed_switches_(0),
        switch_attempts_(3),
        MAX_BACK_MBY_ANGLE_(0.2),
        BACK_MBY_IDX_(1),
        received_BD_info_(false),
        received_joints_angles_(false)
{
    initializeTimers();
    initializePublishers();
    initializeSubscribers();
    initializeServices();
    initializeBehaviors();
    current_behavior_.behavior_name = "halt";
    current_behavior_.is_halted = true;
    current_behavior_pub_.publish(current_behavior_);
}

//ok for now
void LowLevelJointController::initializeTimers()
{
    ROS_INFO("Initializing Timers");
    start_time_=ros::Time::now();
    ROS_INFO("Started at ros::Time of %.3f", ros::Time::now().toSec());
}

bool LowLevelJointController::initializeSafetyParameters()
{
    ROS_INFO("Initializing Safety Parameters.");
    std::string package_path = ros::package::getPath("hku_configs");
    std::string file_path = package_path + "/config/joint_limits.yaml";

    bool use_simulator = false;
    priv_nh_.getParam("use_sim", use_simulator);

    if(use_simulator)
    {
        file_path = package_path + "/config/sim_joint_limits.yaml";
        ROS_WARN("Simulator Joint Limits do not reflect the real robot's range of motion. Look over %s to see the differences.", file_path.c_str());
    }

    YAML::Node joint_limits_node;

    try
    {
        joint_limits_node = YAML::LoadFile(file_path);
    }
    catch (YAML::BadFile& e)
    {
        ROS_ERROR("ERROR:\n%s\nBad YAML file. Couldn't read joint limits from YAML file. Check to make sure the YAML file \"%s\" exists and is in /hku_configs/config/.", e.what(), file_path.c_str());
        return false;
    }

    try
    {
        if(!joint_limits_node["joint_limits"])
        {
            ROS_ERROR("Failed to load joint limits from file: %s", file_path.c_str());
            return false;
        }
        for(int i =0; i < NJoints; i++)
        {
            //Shamelessly stolen from Vlad. Thanks Vlad!
            min_joint_limit_[i] = joint_limits_node["joint_limits"][atlas::jointNameFromIdx(i).c_str()]["min_position"].as<double>() - 0.001;
            max_joint_limit_[i] = joint_limits_node["joint_limits"][atlas::jointNameFromIdx(i).c_str()]["max_position"].as<double>() + 0.001;
        }
    }
    catch(YAML::TypedBadConversion<double>& e)
    {
        ROS_ERROR("Couldn't parse YAML file for Joint Limits.");
        return 1;
    }

    ROS_INFO("Loaded Joint Limits File: %s", file_path.c_str());

    file_path = package_path + "/config/servo_error_limits.yaml";

    try
    {
        joint_limits_node = YAML::LoadFile(file_path);
    }
    catch (YAML::BadFile& e)
    {
        ROS_ERROR("ERROR:\n%s\nBad YAML file. Couldn't read joint limits from YAML file. Check to make sure the YAML file \"%s\" exists and is in /hku_configs/config/.", e.what(), file_path.c_str());
        return false;
    }

    try
    {
        if(!joint_limits_node["error_limits"])
        {
            ROS_ERROR("Failed to load joint limits from file: %s", file_path.c_str());
            return false;
        }
        for(int i = 0; i < NJoints; i++)
        {
            max_angle_error_[i] = joint_limits_node["error_limits"]["max_angle_error"][i].as<double>();
        }
    }
    catch(YAML::TypedBadConversion<double>& e)
    {
        ROS_ERROR("Couldn't parse YAML file for Servo Error Limits.");
        return 1;
    }

    ROS_INFO("Loaded Servo Errors File: %s", file_path.c_str());

    return true;

}

void LowLevelJointController::initializeBehaviors()
{
    ROS_INFO("Initializing Behaviors.");
    RegData reg_behaviors;
    for(int i = 0; i < NJoints; i++)
    {
        reg_behaviors.behavior_reg_data.pos_select[i] = true;
        reg_behaviors.behavior_reg_data.vel_select[i] = true;
        reg_behaviors.behavior_reg_data.effort_select[i] = true;
    }

    reg_behaviors.behavior_reg_data.behavior_name = "legacy_behavior";
    reg_behaviors.topic = "lowLevelJntCmd";
    reg_behaviors.behavior_reg_data.type = hku_msgs::BehaviorRegData::LEGACY;
    reg_behaviors.id = 0;
    reg_behaviors.behavior_reg_data.use_grav_compensation = true;
    reg_behaviors.reg_topic_sub = nh_.subscribe("lowLevelJntCmd",
                                                1,
                                                boost::function<void(const boost::shared_ptr<const hku_msgs::lowLevelJointController>&)>(boost::bind(&LowLevelJointController::jointCmdVecCB, this, _1, 0)),
                                                ros::VoidConstPtr(),
                                                ros::TransportHints().reliable().tcpNoDelay());
    behavior_data_.push_back(reg_behaviors);
    available_behaviors_.behaviors.push_back(reg_behaviors.behavior_reg_data);
    reg_behaviors.behavior_reg_data.behavior_name = "hrpsys";
    reg_behaviors.topic = "atlas/atlas_command_hrpsys";
    reg_behaviors.behavior_reg_data.type = hku_msgs::BehaviorRegData::ATLAS_COMMAND;
    reg_behaviors.id = 1;
    reg_behaviors.reg_topic_sub = nh_.subscribe("atlas/atlas_command_hrpsys",
                                                1,
                                                boost::function<void(const boost::shared_ptr<const atlas_msgs::AtlasCommand>&)>(boost::bind(&LowLevelJointController::atlasJointCmdVecCB, this, _1, 1)),
                                                ros::VoidConstPtr(),
                                                ros::TransportHints().reliable().tcpNoDelay());
    behavior_data_.push_back(reg_behaviors);
    available_behaviors_.behaviors.push_back(reg_behaviors.behavior_reg_data);
    behavior_list_pub_.publish(available_behaviors_);
}

void LowLevelJointController::initializePublishers()
{
    ROS_INFO("Advertising publishers");
    joint_cmd_pub_ = nh_.advertise<atlas_msgs::AtlasCommand>("/atlas/atlas_command", 1, true);
    lljc_heartbeat_pub_ = nh_.advertise<std_msgs::UInt64>("lowLevelControllerIter", 1, true);
    num_iter_.data = 1; // HACK TEMP cswetenham - for the behavior server's sake, make this a non-zero value
    last_cmd_pub_ = nh_.advertise<hku_msgs::LowLevelJointController>("lljc/last_cmd", 1, true);
    current_behavior_pub_ = nh_.advertise<hku_msgs::CurrentBehavior>("lljc/current_behavior", 1, true);
    behavior_list_pub_ = nh_.advertise<hku_msgs::AvailableBehaviors>("lljc/behavior_list", 1, true);
}

void LowLevelJointController::initializeSubscribers()
{
    ROS_INFO("Initializing Subscribers");
    joint_states_sub_ = nh_.subscribe("/atlas/atlas_state",
                                      1,
                                      &LowLevelJointController::atlasStateCB,
                                      this);
    gravity_torques_sub_ = nh_.subscribe("torque_control/stand_both",
                                         1,
                                         &LowLevelJointController::gravityTorquesCB,
                                         this);
    bdi_info_sub_ = nh_.subscribe("atlas/bdi_behavior_info",
                                  1,
                                  &LowLevelJointController::setBDIInfoCB,
                                  this);
}

void LowLevelJointController::initializeServices()
{
    ROS_INFO("Setting up register cmd topic service");
    register_topic_service_ = nh_.advertiseService("lljc/register_behavior",
                                                   &LowLevelJointController::registerBehaviorCB,
                                                   this);

    ROS_INFO("Setting up switch cmd topic service");
    switch_topic_service_ = nh_.advertiseService("lljc/switch_behavior",
                                                 &LowLevelJointController::switchBehaviorCB,
                                                 this);

    ROS_INFO("Setting up Report Command service");
    last_cmd_service_ = nh_.advertiseService("atlas/last_lowLevelJntCmd",
                                             &LowLevelJointController::reportCommandCB,
                                             this);
}


bool LowLevelJointController::initializeGains()
{
    ROS_INFO("Initializing Gains");
    atlas_command_.position.resize(NJoints);
    atlas_command_.velocity.resize(NJoints);
    atlas_command_.effort.resize(NJoints);
    atlas_command_.kp_position.resize(NJoints);
    atlas_command_.ki_position.resize(NJoints);
    atlas_command_.kd_position.resize(NJoints);
    atlas_command_.kp_velocity.resize(NJoints);
    atlas_command_.k_effort.resize(NJoints);
    atlas_command_.i_effort_min.resize(NJoints);
    atlas_command_.i_effort_max.resize(NJoints);

    std::string gains_file("REAL_ROBOT.yaml");
    priv_nh_.getParam("gains_file", gains_file);
    std::string file_path = gains_file;

    if(!boost::filesystem::exists(gains_file))
    {
        std::string package_path = ros::package::getPath("low_level_joint_controller");
        file_path = package_path + "/config/" + gains_file;
    }

    YAML::Node gains_node;

    try
    {
        gains_node = YAML::LoadFile(file_path);
    }
    catch (YAML::BadFile& e)
    {
        ROS_ERROR("ERROR:\n%s\nBad YAML file. Couldn't read gains from YAML file. Check to make sure the YAML file \"%s\" exists and is in /low_level_joint_controller/config/.", e.what(), file_path.c_str());
        return false;
    }

    ROS_INFO("Loaded Gains File: %s", file_path.c_str());

    try
    {
        for(int i = 0; i < NJoints; i++)
        {
            atlas_command_.velocity[i] = 0;
            atlas_command_.effort[i] = 0;
            atlas_command_.ki_position[i] = 0.0; // turn off integral gain
            atlas_command_.k_effort[i] = gains_node["k_effort"][i].as<int>();
            atlas_command_.kp_position[i] = gains_node["kp_position"][i].as<double>();
            atlas_command_.kp_velocity[i] = gains_node["kp_velocity"][i].as<double>();
            atlas_command_.kd_position[i] = gains_node["kd_position"][i].as<double>();
        }
    }
    catch(YAML::TypedBadConversion<double>& e)
    {
        ROS_ERROR("YAML file is malformed. Please make sure values for all %d joints are specified.", NJoints);
        return false;
    }
    catch(YAML::TypedBadConversion<int>& e)
    {
        ROS_ERROR("YAML file is malformed. Please make sure values for all %d joints are specified.", NJoints);
        return false;
    }

    return true;
}

void LowLevelJointController::initializeJoints()
{
    ROS_INFO("Waiting for atlas state data...");
    // let the sensor data settle for a few seconds

    for(int count = 0; !received_joints_angles_ && ros::ok() && !received_BD_info_; ++count)
    {
        if (count % 1000 == 0)
        {
            if(!received_joints_angles_)
            {
                ROS_WARN("low_level_joint_controller waiting to receive an AtlasState message.");
            }
            if(!received_BD_info_)
            {
                ROS_WARN("low_level_joint_controller waiting to receive a BDIBehaviorInfo message.");
            }
        }
        ros::spinOnce();
        lljc_looprate_.sleep();
    }

    //Initialize command messages
    valid_cmds_ = reported_state_;
    validJointCommandsToMsg(valid_cmds_, atlas_command_, last_command_);
}

bool LowLevelJointController::validateJointCommand(LLJCCommand& command_data)
{
    bool is_bumpless = true;
    double angle_error = 0.0;

    for(int i = 0; i < NJoints; i++)
    {
        if(!is_joint_user_controlled_[i])
        {
            command_data.pos[i] = reported_state_.pos[i];
            command_data.vel[i] = 0;
            command_data.effort[i] = 0;
            command_data.k_effort[i] = 0;
            continue;
        }
        if(behavior_data_[current_behavior_idx_].behavior_reg_data.pos_select[i])
        {
            //Implemented 12/6/13 at the request of Dr. Newman
            if(command_data.pos[i] > max_joint_limit_[i])
            {
                ROS_WARN("Current command exceed joint limit: joint %d has range %f to %f and was commanded to %f", i, max_joint_limit_[i], min_joint_limit_[i], command_data.pos[i]);
                command_data.pos[i] = max_joint_limit_[i];
                ROS_WARN("Clamping command to joint limit %f.", command_data.pos[i]);
            }
            else if(command_data.pos[i] < min_joint_limit_[i])
            {
                ROS_WARN("Current command exceed joint limit: joint %d has range %f to %f and was commanded to %f", i, max_joint_limit_[i], min_joint_limit_[i], command_data.pos[i]);
                command_data.pos[i] = min_joint_limit_[i];
                ROS_WARN("Clamping command to joint limit %f.", command_data.pos[i]);
            }

            angle_error = fabs(command_data.pos[i] - reported_state_.pos[i]);
            is_bumpless = angle_error <= max_angle_error_[i];
            if(!is_bumpless)
            {
                ROS_ERROR("Current command exceeded safe operation: error is %f for joint %d", angle_error, i);
            }
        }
        else
        {
            command_data.pos[i] = last_command_.jntAngles[i];
        }
        if(!behavior_data_[current_behavior_idx_].behavior_reg_data.vel_select[i])
        {
            command_data.vel[i] = last_command_.jntVelocities[i];
        }
        if(!behavior_data_[current_behavior_idx_].behavior_reg_data.effort_select[i])
        {
            command_data.effort[i] = last_command_.jntEfforts[i];
        }
        if(!is_bumpless)
        {
            if(is_switching_)
            {
                ROS_ERROR("Behavior %s failed to make transition bumpless", getBehaviorName(current_behavior_idx_).c_str());
                if(++failed_switches_ >= switch_attempts_)
                {
                    ROS_ERROR("Behavior %s exceed number of attempts to switch. Returning to halt state.", getBehaviorName(current_behavior_idx_).c_str());
                    current_behavior_idx_ = HALT;
                    failed_switches_ = 0;
                    is_switching_ = false;
                    current_behavior_.behavior_name = "halt";
                    current_behavior_.is_halted = true;
                    current_behavior_pub_.publish(current_behavior_);
                }
            }
            return false;
        }
    }
    failed_switches_ = 0;
    is_switching_ = false;

    //Check if Atlas's torso will bend over too far
    if(command_data.pos[BACK_MBY_IDX_] > MAX_BACK_MBY_ANGLE_)
    {
        ROS_WARN("Exceeded Torso Soft Joint Limit: Changing back_mby command from %f to %f", command_data.pos[BACK_MBY_IDX_], MAX_BACK_MBY_ANGLE_);
        command_data.pos[BACK_MBY_IDX_] = MAX_BACK_MBY_ANGLE_;
    }
    if(command_data.pos[BACK_MBY_IDX_] < -MAX_BACK_MBY_ANGLE_)
    {
        ROS_WARN("Exceeded Torso Soft Joint Limit: Changing back_mby command from %f to %f", command_data.pos[BACK_MBY_IDX_], -MAX_BACK_MBY_ANGLE_);
        command_data.pos[BACK_MBY_IDX_] = -MAX_BACK_MBY_ANGLE_;
    }

    return true;
}

int LowLevelJointController::run()
{
    if(!initializeSafetyParameters())
    {
        ROS_ERROR("Failed to initialize safety checks.");
        return 1;
    }

    if(!initializeGains())
    {
        ROS_ERROR("Failed to initialize gains.");
        return 1;
    }

    initializeJoints();

    ROS_INFO("Starting main loop...");

    // main loop
    while (ros::ok())
    {
        if(!gravity_torques_)
          ROS_WARN("Gravity Compensation is not running.");
        // if halted, do not send commands to Atlas, but do keep track of what BDI is doing

        bool use_grav_comp =
          gravity_torques_
          && (current_behavior_idx_ == HALT
          || behavior_data_[current_behavior_idx_].behavior_reg_data.use_grav_compensation);

        if (current_behavior_idx_ == HALT)
        {
            haltJointCommandsToMsg(reported_state_, atlas_command_, last_command_);
        }
        else
        {
            validJointCommandsToMsg(valid_cmds_, atlas_command_, last_command_);
        }

        if (use_grav_comp)
        {
          for (int i = 0; i < NJoints; i++)
          {
            atlas_command_.effort[i] += gravity_torques_->dblValues[i];
          }
        }

        atlas_command_.header.stamp = ros::Time::now();
        last_command_.header = atlas_command_.header;
        ++num_iter_.data; // keep count of how many commands sent

        joint_cmd_pub_.publish(atlas_command_); // send out joint commands to Atlas
        last_cmd_pub_.publish(last_command_);

        ros::spinOnce();
        lljc_looprate_.sleep();
    }
    return 0;
}

inline void LowLevelJointController::validJointCommandsToMsg(LLJCCommand& command_data, atlas_msgs::AtlasCommand& atlas_command_msg, hku_msgs::LowLevelJointController& last_command_msg)
{
  for(int i = 0; i < NJoints; i++)
    {
        last_command_msg.jntAngles[i] = command_data.pos[i];
        last_command_msg.jntVelocities[i] = command_data.vel[i];
        last_command_msg.jntEfforts[i] = command_data.effort[i];
        last_command_msg.k_effort[i] = command_data.k_effort[i];
        last_command_msg.jntAccelerations[i] = 0;
        atlas_command_msg.position[i] = command_data.pos[i];
        atlas_command_msg.velocity[i] = command_data.vel[i];
        atlas_command_msg.effort[i] = command_data.effort[i];
        atlas_command_msg.k_effort[i] = command_data.k_effort[i];
    }

}

inline void LowLevelJointController::jointStatesMsgToAPI(atlas_msgs::AtlasState const& state_msg, LLJCCommand& command_data)
{
    //Neal - ros_controller checks to ensure atlas_state msgs are valid. A check here would be redundant.
    for(int i = 0; i < NJoints; i++)
    {
        command_data.pos[i] = state_msg.position[i];
    }
}

inline bool LowLevelJointController::jointCommandsMsgToAPI(boost::shared_ptr<const hku_msgs::lowLevelJointController> const& command_msg, LLJCCommand& command_data)
{
    bool valid_command = true;
    bool ignore_k_effort = command_msg->k_effort.size() == 0;

    //Regardless of registration parameters, any behavior must issue commands for all of the following:
    valid_command &= command_msg->jntAngles.size() == NJoints;
    valid_command &= command_msg->jntVelocities.size() == NJoints;
    valid_command &= command_msg->jntEfforts.size() == NJoints;
    if(!ignore_k_effort)
    {
        valid_command &= command_msg->k_effort.size() == NJoints;
    }

    if(!valid_command)
    {
        ROS_ERROR("Invalid Joint Command.  Must specify position, velocity, effort values for all %d joints.", NJoints);
        return false;
    }

    for(int i = 0; i < NJoints; i++)
    {
       command_data.pos[i] = command_msg->jntAngles[i];
       command_data.vel[i] = command_msg->jntVelocities[i];
       command_data.effort[i] = command_msg->jntEfforts[i];
       command_data.k_effort[i] = (ignore_k_effort) ? 255 : command_msg->k_effort[i];
    }

    return true;
}

inline bool LowLevelJointController::jointCommandsMsgToAPI(boost::shared_ptr<const atlas_msgs::AtlasCommand> const& command_msg, LLJCCommand& command_data)
{
    bool valid_command = true;
    bool ignore_k_effort = command_msg->k_effort.size() == 0;

    //Regardless of registration parameters, any behavior must issue commands for all of the following:
    valid_command &= command_msg->position.size() == NJoints;
    valid_command &= command_msg->velocity.size() == NJoints;
    valid_command &= command_msg->effort.size() == NJoints;
    if(!ignore_k_effort)
    {
        valid_command &= command_msg->k_effort.size() == NJoints;
    }

    if(!valid_command)
    {
        ROS_ERROR("Invalid Joint Command.  Must specify position, velocity, effort values for all %d joints.", NJoints);
        return false;
    }

    for(int i = 0; i < NJoints; i++)
    {
        command_data.pos[i] = command_msg->position[i];
        command_data.vel[i] = command_msg->velocity[i];
        command_data.effort[i] = command_msg->effort[i];
        command_data.k_effort[i] = (ignore_k_effort) ? 255 : command_msg->k_effort[i];
    }

    return true;
}

inline void LowLevelJointController::haltJointCommandsToMsg(LLJCCommand& command_data, atlas_msgs::AtlasCommand&  atlas_command_msg, hku_msgs::LowLevelJointController& lljc_command_msg)
{
    for(int i = 0; i < NJoints; i++)
    {
        if(!is_joint_user_controlled_[i])
        {
            atlas_command_msg.position[i] = command_data.pos[i];
            atlas_command_msg.velocity[i] = 0;
            atlas_command_msg.effort[i] = 0;
            atlas_command_msg.k_effort[i] = 0;

            lljc_command_msg.jntAngles[i] = command_data.pos[i];
            lljc_command_msg.jntEfforts[i] = 0;
            lljc_command_msg.k_effort[i] = 0;
            lljc_command_msg.jntVelocities[i] = 0;
        }
        else
        {
            atlas_command_msg.position[i] = lljc_command_msg.jntAngles[i];
            atlas_command_msg.velocity[i] = lljc_command_msg.jntVelocities[i];
            atlas_command_msg.effort[i] = lljc_command_msg.jntEfforts[i];
            atlas_command_msg.k_effort[i] = lljc_command_msg.k_effort[i];
        }
    }
}

int main(int argc, char** argv)
{
    // ROS set-ups:
    ros::init(argc, argv, "lowLevelJointController");

    ros::NodeHandle nh; //eventually change this to namespace lljc
    ros::NodeHandle priv_nh("~");

    LowLevelJointController lljc(&nh, &priv_nh);

    return lljc.run();
}
