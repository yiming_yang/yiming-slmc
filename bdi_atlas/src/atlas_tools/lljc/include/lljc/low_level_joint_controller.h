/** low_level_joint_controller implementation file **/
#ifndef LOW_LEVEL_JOINT_CONTROLLER_H_
#define LOW_LEVEL_JOINT_CONTROLLER_H_

#include <math.h>
#include <stdlib.h>
#include <ros/ros.h>
#include <atlas_msgs/AtlasState.h>
#include <atlas_msgs/AtlasCommand.h>
#include <hku_msgs/lowLevelJointController.h>
#include <hku_msgs/vectorOfDoublesWHeader.h>
#include <hku_msgs/BehaviorRegData.h>
#include <hku_msgs/AvailableBehaviors.h>
#include <hku_msgs/CurrentBehavior.h>
#include <hku_msgs/BDIBehaviorInfo.h>
#include <std_msgs/String.h>
#include <atlas_conversions/joint_names.h>
#include <boost/thread.hpp>
#include <boost/bind.hpp>
#include <boost/thread/locks.hpp>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
#include <string>
#include <vector>
#include <bits/stl_map.h>
#include <yaml-cpp/yaml.h>
#include <ros/package.h>
#include <std_msgs/UInt64.h>
#include <eigen_conversions/eigen_msg.h>
#include <eigen3/Eigen/StdVector>
#include <eigen3/Eigen/StdList>

#include <lljc/low_level_controller_last_command.h>
#include <hku_msgs/RegisterBehavior.h>
#include <lljc/SwitchBehavior.h>

namespace hku_msgs
{
    typedef lowLevelJointController LowLevelJointController;
}


class LowLevelJointController
{

public:
    LowLevelJointController();
    LowLevelJointController(ros::NodeHandle* nodehandle, ros::NodeHandle* priv_nodehandle);
    virtual ~LowLevelJointController(void) {}
    int run();

private:
    struct RegData
    {
        hku_msgs::BehaviorRegData behavior_reg_data;
        int id;
        std::string topic;
        ros::Subscriber reg_topic_sub;
    };

    enum
    {
        HALT = -1
    };


    typedef Eigen::Matrix<double, NJoints, 1> JointVector;
    typedef Eigen::Matrix<uint8_t, NJoints, 1> KEffortVector;

    struct LLJCCommand
    {
        JointVector pos;
        JointVector vel;
        JointVector effort;
        KEffortVector k_effort;
    };

    int current_behavior_idx_;

    std::vector<RegData> behavior_data_;

    ros::Rate lljc_looprate_;
    ros::NodeHandle nh_; //legacy support
    ros::NodeHandle priv_nh_;

    bool is_switching_;
    int failed_switches_;
    int switch_attempts_;

    JointVector max_angle_error_;
    JointVector max_joint_limit_;
    JointVector min_joint_limit_;

    const double MAX_BACK_MBY_ANGLE_;
    const int BACK_MBY_IDX_;

    bool received_BD_info_;
    bool received_joints_angles_;

    LLJCCommand received_cmds_;  //commands from messages
    LLJCCommand valid_cmds_;  //commands that passed safety checks
    LLJCCommand reported_state_; //Sensor Data from /atlas/atlas_state

    bool is_joint_user_controlled_[NJoints];

    std_msgs::UInt64 num_iter_;
    ros::Time start_time_;

    hku_msgs::lowLevelJointController last_command_;
    hku_msgs::AvailableBehaviors available_behaviors_;
    hku_msgs::CurrentBehavior current_behavior_;
    hku_msgs::vectorOfDoublesWHeaderConstPtr gravity_torques_;
    atlas_msgs::AtlasCommand atlas_command_;

    void initializeJoints();
    void initializeServices();
    void initializePublishers();
    void initializeSubscribers();
    void initializeBehaviors();
    bool initializeGains();
    void initializeTimers();
    bool initializeSafetyParameters();

    void gravityTorquesCB(const hku_msgs::vectorOfDoublesWHeaderConstPtr& message_holder);
    void atlasStateCB(const atlas_msgs::AtlasState& js);
    void setBDIInfoCB(const hku_msgs::BDIBehaviorInfo& bdi_info);
    void jointCmdVecCB(boost::shared_ptr<const hku_msgs::lowLevelJointController> const& lljc_cmd_msg, int id);
    void atlasJointCmdVecCB(boost::shared_ptr<const atlas_msgs::AtlasCommand> const& atlas_cmd_msg, int id);
    bool reportCommandCB(lljc::low_level_controller_last_command::Request& req, lljc::low_level_controller_last_command::Response& res);
    bool registerBehaviorCB(hku_msgs::RegisterBehavior::Request& req, hku_msgs::RegisterBehavior::Response& res);
    bool switchBehaviorCB(lljc::SwitchBehavior::Request& req, lljc::SwitchBehavior::Response& res);

    inline bool jointCommandsMsgToAPI(boost::shared_ptr<const atlas_msgs::AtlasCommand> const& command_msg, LLJCCommand& command_data);
    inline bool jointCommandsMsgToAPI(boost::shared_ptr<const hku_msgs::lowLevelJointController> const& command_msg, LLJCCommand& command_data);
    inline void jointStatesMsgToAPI(atlas_msgs::AtlasState const& state_msg, LLJCCommand& command_data);

    inline void validJointCommandsToMsg(LLJCCommand& command_data, atlas_msgs::AtlasCommand&  atlas_command_msg, hku_msgs::LowLevelJointController& lljc_command_msg);
    inline void haltJointCommandsToMsg(LLJCCommand& command_data, atlas_msgs::AtlasCommand&  atlas_command_msg, hku_msgs::LowLevelJointController& lljc_command_msg);

    std::string getBehaviorName(int idx) const;
    bool validateJointCommand(LLJCCommand& command_data);

    ros::Publisher lljc_heartbeat_pub_;
    ros::Publisher joint_cmd_pub_;
    ros::Publisher last_cmd_pub_;
    ros::Publisher behavior_list_pub_;
    ros::Publisher current_behavior_pub_;
    ros::Subscriber joint_states_sub_;
    ros::Subscriber bdi_info_sub_;
    ros::Subscriber gravity_torques_sub_;
    ros::ServiceServer register_topic_service_;
    ros::ServiceServer switch_topic_service_;
    ros::ServiceServer last_cmd_service_;
};
#endif
