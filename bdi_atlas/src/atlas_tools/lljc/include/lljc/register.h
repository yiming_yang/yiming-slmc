#ifndef REGISTER_H_
#define REGISTER_H_

#include <ros/ros.h>
#include <hku_msgs/BehaviorRegData.h>
#include <hku_msgs/RegisterBehavior.h>
#include <atlas_conversions/joint_names.h>

namespace lljc
{
  struct BehaviorData
  {
    std::string topic;
    int behavior_id;
    bool is_registered;
  };

  inline BehaviorData register_behavior(const hku_msgs::BehaviorRegData* reg_data)
  {
    if(reg_data->type < 0 || reg_data->type > 2)
    {
        ROS_ERROR("Requested Behavior Type is not valid.");
        BehaviorData behavior_data;
        behavior_data.is_registered = false;
        return behavior_data;
    }

    hku_msgs::RegisterBehavior::Response lljc_response;
    hku_msgs::RegisterBehavior::Request lljc_request;
    lljc_request.reg_data = *reg_data;

    if(!ros::service::call("lljc/register_behavior", lljc_request, lljc_response))
    {
        ROS_ERROR("Registration Service failed to complete.");
        BehaviorData behavior_data;
        behavior_data.is_registered = false;
        return behavior_data;
    }
    BehaviorData behavior_data;
    behavior_data.topic = lljc_response.topic;
    behavior_data.behavior_id = lljc_response.id;
    behavior_data.is_registered = true;
    return behavior_data;
  }

  inline BehaviorData register_behavior(std::string behavior_name,
                                        int behavior_type,
                                        bool use_gravity_compensation,
                                        bool position_select[NJoints],
                                        bool velocity_select[NJoints],
                                        bool effort_select[NJoints])
  {
    hku_msgs::BehaviorRegData reg_data;
    reg_data.behavior_name = behavior_name;
    reg_data.type = behavior_type;
    reg_data.use_grav_compensation = use_gravity_compensation;
    for (int i = 0; i < NJoints; i++)
    {
        reg_data.pos_select[i] = position_select[i];
        reg_data.vel_select[i] = velocity_select[i];
        reg_data.effort_select[i] = effort_select[i];
    }

    return register_behavior(&reg_data);
  }

} //end namespace low_level_joint_controller

#endif
