cmake_minimum_required(VERSION 2.8.3)
project(lljc)

## Import certain basic scripts etc...
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "$ENV{SLMC_BUILD_TOOLS}/cmake")

## Find catkin macros and libraries
find_package(catkin REQUIRED COMPONENTS
  roscpp
  roslib
  atlas_msgs
  hku_msgs
  std_msgs
  atlas_conversions
  eigen_conversions
  message_generation
)

## System dependencies are found with CMake's conventions
find_package(Boost REQUIRED COMPONENTS system thread signals)
find_package(yaml-cpp REQUIRED)

# catkin_python_setup()

## Generate services in the 'srv' folder
add_service_files(
  FILES
  low_level_controller_last_command.srv
  SwitchBehavior.srv
)

## Generate added messages and services with any dependencies listed here
generate_messages(
  DEPENDENCIES
  hku_msgs
)

## catkin specific configuration ##
catkin_package(
  INCLUDE_DIRS include
  CATKIN_DEPENDS roscpp roslib atlas_msgs hku_msgs std_msgs atlas_conversions eigen_conversions message_runtime
  DEPENDS eigen yaml-cpp system_lib boost
)

## Build ##
include_directories(
  include
  SYSTEM
  ${catkin_INCLUDE_DIRS}
  ${EIGEN_INCLUDE_DIRS}
  ${Boost_INCLUDE_DIRS}
  ${yaml-cpp_INCLUDE_DIRS}
)
add_executable(low_level_joint_controller src/low_level_joint_controller.cpp)
add_executable(test_generator tests/src/test_generator.cpp)
target_link_libraries(low_level_joint_controller ${catkin_LIBRARIES} ${yaml-cpp_LIBRARY} ${Boost_LIBRARY})
target_link_libraries(test_generator ${catkin_LIBRARIES} ${yaml-cpp_LIBRARY} ${Boost_LIBRARY})

## Testing ##
catkin_add_gtest(low_level_joint_controller_test tests/src/low_level_joint_controller_test.cpp)
target_link_libraries(low_level_joint_controller_test ${yaml-cpp_LIBRARY})
